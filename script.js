(function(){
    var script = {
 "mouseWheelEnabled": true,
 "layout": "absolute",
 "borderRadius": 0,
 "height": "100%",
 "id": "rootPlayer",
 "children": [
  "this.MainViewer",
  "this.IconButton_E3C1AA4E_EDAE_C864_41E4_5820E86940F3",
  "this.IconButton_E3081138_EDA3_D82C_41E8_15DF452055EA",
  "this.veilPopupPanorama",
  "this.zoomImagePopupPanorama",
  "this.closeButtonPopupPanorama"
 ],
 "paddingBottom": 0,
 "backgroundPreloadEnabled": true,
 "scrollBarWidth": 10,
 "start": "this.playAudioList([this.audio_2C1BAB8B_2031_FC83_41B7_BE0E20D0E485]); this.init(); this.visibleComponentsIfPlayerFlagEnabled([this.IconButton_E3081138_EDA3_D82C_41E8_15DF452055EA], 'cardboardAvailable'); if(!this.get('fullscreenAvailable')) { [this.IconButton_E3C1AA4E_EDAE_C864_41E4_5820E86940F3].forEach(function(component) { component.set('visible', false); }) }",
 "propagateClick": false,
 "verticalAlign": "top",
 "width": "100%",
 "overflow": "visible",
 "defaultVRPointer": "laser",
 "borderSize": 0,
 "desktopMipmappingEnabled": false,
 "minHeight": 20,
 "paddingRight": 0,
 "definitions": [{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D31ACCD_202E_F487_4192_32EB345EEAEC",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D31BCCD_202E_F487_41BA_AEAD03F59EB4",
 "initialPosition": {
  "yaw": -55.01,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DA3D83D_202E_FB87_4181_3C0977D6DD98",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DA3E83D_202E_FB87_4163_565A7AB008F3",
 "initialPosition": {
  "yaw": -18.62,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D0C8DA0_202E_F4BD_41AA_C082B8E4E93E",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D0CADA0_202E_F4BD_41A7_C0BD3723ED18",
 "initialPosition": {
  "yaw": -77.05,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D3C2C8E_202E_F485_41B2_E75D2B20E741",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D3CCC8E_202E_F485_419D_EBDDA2B56298",
 "initialPosition": {
  "yaw": 107.3,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-15",
 "hfov": 360,
 "overlays": [
  "this.overlay_4CFEC05B_5E5F_9A15_41D4_AABD7709AD2B",
  "this.overlay_4B8534A4_5E40_BA33_41CC_09B777108D8E",
  "this.overlay_4BF797D2_5E41_A614_4198_B17B659E7505",
  "this.panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_tcap0"
 ],
 "id": "panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 161.31,
   "yaw": 123.58,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41"
  },
  {
   "backwardYaw": 170.59,
   "yaw": -6,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69"
  },
  {
   "class": "AdjacentPanorama",
   "panorama": "this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E1D698D_202E_FC87_41BF_ED22121F60A2",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E1D798D_202E_FC87_41B1_8E021C87D608",
 "initialPosition": {
  "yaw": 78.67,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.41,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D87F1C35_D69A_5516_4171_4F32029468FE",
 "yaw": 165.91,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D87F1C35_D69A_5516_4171_4F32029468FE_0_0.png",
    "width": 830,
    "class": "ImageResourceLevel",
    "height": 735
   },
   {
    "url": "media/popup_D87F1C35_D69A_5516_4171_4F32029468FE_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 453
   }
  ]
 },
 "pitch": 18.94,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-05",
 "hfov": 360,
 "overlays": [
  "this.overlay_5859F4EF_57BE_8B53_41CE_078228F76AAE",
  "this.overlay_479BFAC5_57BA_9F57_41D3_AA6B52386CF6",
  "this.overlay_466D2340_57A6_8D4D_41B5_01AC7C8BD8C2",
  "this.overlay_491075A9_59A2_15AA_41C7_AAD8598F074E",
  "this.overlay_93D4F7E4_8B59_60FA_41D8_6992FAD86FFD",
  "this.overlay_9C181819_8B5F_2F4A_41B3_68E1713731E2",
  "this.overlay_93E3C811_8B5F_2F5D_41DF_FF2BDF1AE246",
  "this.overlay_9C1C2DB9_8B5E_E14A_41B3_510BB5771ECE",
  "this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_tcap0",
  "this.popup_D8030674_D69A_D516_41DE_4A7F21C38C32",
  "this.popup_D855325C_D69A_4D15_41D9_6EB8DDE333F7",
  "this.popup_D87F1C35_D69A_5516_4171_4F32029468FE",
  "this.popup_D86D6F24_D69A_B335_41E9_37CBA15E74B1"
 ],
 "id": "panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -112.3,
   "yaw": -14.48,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236"
  },
  {
   "backwardYaw": -12.72,
   "yaw": 88.52,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123"
  },
  {
   "backwardYaw": -98.47,
   "yaw": 21.61,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5"
  },
  {
   "backwardYaw": 142.78,
   "yaw": 124.93,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F8567126_F198_57BF_41D9_3446ACE07DBB"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D11ED7E_202E_F585_41B1_23D1DC8C606F",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D118D7E_202E_F585_41AB_C18F26075CF8",
 "initialPosition": {
  "yaw": -179.61,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D4EAE79_202E_F78F_41BD_30DD8137F7DF",
 "initialPosition": {
  "yaw": -22.5,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.19,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C1D948AD_D1B0_2C5F_41E7_6F23D711C545",
 "yaw": 28.34,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C1D948AD_D1B0_2C5F_41E7_6F23D711C545_0_0.png",
    "width": 735,
    "class": "ImageResourceLevel",
    "height": 596
   },
   {
    "url": "media/popup_C1D948AD_D1B0_2C5F_41E7_6F23D711C545_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 415
   }
  ]
 },
 "pitch": -6.69,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D597E5C_202E_F785_41B7_C5F8FB7C0BC8",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D590E5C_202E_F785_41B9_CA974A00F91E",
 "initialPosition": {
  "yaw": -57.76,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.23,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9B5EF1C_D69D_D316_41A5_2E8737A8F7BD",
 "yaw": 26.52,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9B5EF1C_D69D_D316_41A5_2E8737A8F7BD_0_0.png",
    "width": 775,
    "class": "ImageResourceLevel",
    "height": 747
   },
   {
    "url": "media/popup_D9B5EF1C_D69D_D316_41A5_2E8737A8F7BD_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 493
   }
  ]
 },
 "pitch": 2.02,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_t.jpg"
  }
 ],
 "label": "HIMATEKKIM-2021_01",
 "hfov": 360,
 "overlays": [
  "this.overlay_FFDE3923_F188_D7B5_41E3_8CC9D718719B",
  "this.overlay_FF3BF211_F188_5595_41CD_15A47E1C9EEF",
  "this.overlay_FFC3F61F_F188_FD8D_41E7_DE8A3CF60B75",
  "this.overlay_7E3C4354_6241_9E1C_41CA_A3E49E2C3020",
  "this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_tcap0"
 ],
 "id": "panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 100.6,
   "yaw": -79.79,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8"
  },
  {
   "backwardYaw": -143.83,
   "yaw": -154.47,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD"
  },
  {
   "backwardYaw": 68.72,
   "yaw": 128.31,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41"
  },
  {
   "backwardYaw": -91.58,
   "yaw": -11.55,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D06EDC2_202E_F4FD_4191_18180326D9B2",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D06FDC2_202E_F4FD_4190_BD88032CC5E2",
 "initialPosition": {
  "yaw": 116.92,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 3.34,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C211C828_D32A_9EF0_41DD_EE8B9AE2E7CA",
 "yaw": -26.76,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C211C828_D32A_9EF0_41DD_EE8B9AE2E7CA_0_0.png",
    "width": 773,
    "class": "ImageResourceLevel",
    "height": 720
   },
   {
    "url": "media/popup_C211C828_D32A_9EF0_41DD_EE8B9AE2E7CA_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 476
   }
  ]
 },
 "pitch": -6.14,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D126723_202E_F583_4183_A440FC141373",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D127723_202E_F583_41B2_5C08A3ED0D88",
 "initialPosition": {
  "yaw": -26.33,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D959F18_202E_F58D_41AE_EBFE0AE31E98",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D95AF17_202E_F582_41A5_32476D415251",
 "initialPosition": {
  "yaw": 10.01,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2EEA632E_202E_8D85_41B3_E3CF1BE264EA",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2EEA732D_202E_8D86_4189_1FF71A1F8C15",
 "initialPosition": {
  "yaw": -61.39,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.39,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D80973A1_D69E_732E_41B4_2CA10CF93671",
 "yaw": -15.31,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D80973A1_D69E_732E_41B4_2CA10CF93671_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 596
   },
   {
    "url": "media/popup_D80973A1_D69E_732E_41B4_2CA10CF93671_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 392
   }
  ]
 },
 "pitch": 1.47,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_t.jpg"
  }
 ],
 "label": "HIMATEKKIM-2021_03",
 "hfov": 360,
 "overlays": [
  "this.overlay_E1BEDE09_F1B8_AD75_41DE_C3FCE23F6C26",
  "this.overlay_FE4992AB_F1B8_5AB5_41BF_FC0288BD3A40",
  "this.overlay_59E84FC8_57BD_B55C_41D1_EAF91840FC22",
  "this.overlay_9CEE4666_8B59_E3C6_41DB_99E53A819B6B",
  "this.overlay_9CEE7666_8B59_E3C6_41AA_9BFD1783E297",
  "this.overlay_9C27997A_8B5B_21CF_41C7_E2437113E264",
  "this.overlay_9C06FA82_8B5B_E33E_41D8_6D391711E126",
  "this.popup_9CD55630_8B59_E35A_41DA_381ACC9AD802",
  "this.popup_9CD40631_8B59_E35A_41D2_6FC05520124A",
  "this.panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_tcap0",
  "this.popup_D818D7DC_D69E_D316_41DE_15AF326B24EA",
  "this.popup_D9B7F444_D69E_5575_41D4_2CF331886357",
  "this.popup_D9B5EF1C_D69D_D316_41A5_2E8737A8F7BD",
  "this.popup_D9C29F8E_D69A_53F5_41E6_1BFB17B3B974"
 ],
 "id": "panorama_F8567126_F198_57BF_41D9_3446ACE07DBB",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -92.39,
   "yaw": -148.63,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123"
  },
  {
   "backwardYaw": 124.93,
   "yaw": 142.78,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B"
  },
  {
   "backwardYaw": 118.82,
   "yaw": -70.62,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2CCCE630_202E_F79D_41B6_94B4D74F5E35",
 "initialPosition": {
  "yaw": 19.72,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.57,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9BC7DE7_D6EA_D733_41E5_D608406EDFEC",
 "yaw": -25.38,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9BC7DE7_D6EA_D733_41E5_D608406EDFEC_0_0.png",
    "width": 784,
    "class": "ImageResourceLevel",
    "height": 599
   },
   {
    "url": "media/popup_D9BC7DE7_D6EA_D733_41E5_D608406EDFEC_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 391
   }
  ]
 },
 "pitch": -5.18,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAAFA24_5E43_AE3C_41A5_053B7BCF6120",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_camera",
 "initialPosition": {
  "yaw": -75.96,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -4.95
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2F0BDC14_202E_FB85_41AC_27D6548B78E7",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F0BEC14_202E_FB85_418F_9E380A86177C",
 "initialPosition": {
  "yaw": -5.32,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.05,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D98D7E0E_D6EA_54F2_41D3_36841F916DCC",
 "yaw": -25.29,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D98D7E0E_D6EA_54F2_41D3_36841F916DCC_0_0.png",
    "width": 876,
    "class": "ImageResourceLevel",
    "height": 648
   },
   {
    "url": "media/popup_D98D7E0E_D6EA_54F2_41D3_36841F916DCC_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 378
   }
  ]
 },
 "pitch": 21.37,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F1BDBF7_202E_FC83_41BD_E264D2FCE079",
 "initialPosition": {
  "yaw": -56.42,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAADA23_5E43_AE34_41D5_4299AB8793BA",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_camera",
 "initialPosition": {
  "yaw": -93.3,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -6.61
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C0140ACB_D090_EDDB_41CD_7B0EFA884291",
 "yaw": -34.81,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C0140ACB_D090_EDDB_41CD_7B0EFA884291_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 635
   },
   {
    "url": "media/popup_C0140ACB_D090_EDDB_41CD_7B0EFA884291_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 417
   }
  ]
 },
 "pitch": -4.17,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAB6A26_5E43_AE3C_41B3_910691F38ECB",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_camera",
 "initialPosition": {
  "yaw": -70.47,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -8.37
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E851B22_202E_FDBD_41B3_FA2403C72956",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E852B21_202E_FDBE_41BA_85DEFE735063",
 "initialPosition": {
  "yaw": -6.28,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2EDB0358_202E_8D8D_41B6_12F04D887ED1",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2EDB2358_202E_8D8D_4180_F850C47CFA0B",
 "initialPosition": {
  "yaw": -179.68,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.1,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9814A4E_D695_DD75_41B1_6706B567BE99",
 "yaw": -73.23,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9814A4E_D695_DD75_41B1_6706B567BE99_0_0.png",
    "width": 773,
    "class": "ImageResourceLevel",
    "height": 607
   },
   {
    "url": "media/popup_D9814A4E_D695_DD75_41B1_6706B567BE99_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 402
   }
  ]
 },
 "pitch": 1.78,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-06",
 "hfov": 360,
 "overlays": [
  "this.overlay_573452C6_59A2_0FE6_41A6_649E548C964F",
  "this.overlay_497CB1EB_59A2_0DAE_41C5_584A2066203F",
  "this.overlay_4C64AC76_5DFE_265B_41D2_0FA4E7D18335",
  "this.panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_tcap0"
 ],
 "id": "panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 54.69,
   "yaw": 38.33,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827"
  },
  {
   "backwardYaw": 21.61,
   "yaw": -98.47,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B"
  },
  {
   "backwardYaw": -171.73,
   "yaw": -21.71,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DFB3F54_202E_F585_4181_2A709B763D2A",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DFBCF54_202E_F585_41A4_FE2FC92AC848",
 "initialPosition": {
  "yaw": 168.45,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D17FD64_202E_F585_41BA_13E13A7D1F11",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D179D64_202E_F585_419F_990ACF192814",
 "initialPosition": {
  "yaw": 83.8,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C08E0602_D0F1_E445_41CD_EE518397B837",
 "yaw": 138.97,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C08E0602_D0F1_E445_41CD_EE518397B837_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 635
   },
   {
    "url": "media/popup_C08E0602_D0F1_E445_41CD_EE518397B837_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 417
   }
  ]
 },
 "pitch": -3.81,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D700E1F_202E_F783_4187_4D18A6B9171B",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D702E1F_202E_F783_41AE_B94F2513E629",
 "initialPosition": {
  "yaw": -82.27,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E98D2A5_202E_8C87_41BF_76267A909538",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E98F2A5_202E_8C87_41B1_03EA6EF3B9F9",
 "initialPosition": {
  "yaw": 114.41,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-07",
 "hfov": 360,
 "overlays": [
  "this.overlay_48AEB2F3_59AE_0FBE_41CE_47A8C4D0D010",
  "this.overlay_48FD6727_59A2_F6A7_41C3_6E4B7181A4E7",
  "this.overlay_4B3FFFD5_59A6_15FB_41BA_FE555D5B70DC",
  "this.overlay_4DE5D56C_59A2_0AA9_41D1_F577ECFE3524",
  "this.overlay_935CCCD4_8B5B_20DB_41D9_07D79E8210E2",
  "this.overlay_9C230519_8B5B_E14D_41D8_B68FEF5FDC50",
  "this.overlay_93CCE893_8B5B_2F5E_41D0_72C74C77A73A",
  "this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_tcap0",
  "this.popup_D841E06C_D69A_4D36_41E8_4A20160FD43A",
  "this.popup_D86A6B0C_D695_BCF6_41AF_4489832BFFCA",
  "this.popup_D9F5424F_D696_4D73_41E1_23EEEFCFBBC3"
 ],
 "id": "panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -92.18,
   "yaw": 94.52,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F"
  },
  {
   "backwardYaw": 122.69,
   "yaw": 143.18,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827"
  },
  {
   "backwardYaw": -14.48,
   "yaw": -112.3,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B"
  },
  {
   "backwardYaw": -21.71,
   "yaw": -171.73,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA7A21_5E43_AE34_41A6_E83039CB0202",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_camera",
 "initialPosition": {
  "yaw": -53.67,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D436E9B_202E_F483_41B9_1AF7EA69FB3C",
 "initialPosition": {
  "yaw": 133.84,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.68,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C08E4886_D090_6C4D_41E3_287AABE86716",
 "yaw": 33.26,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C08E4886_D090_6C4D_41E3_287AABE86716_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 619
   },
   {
    "url": "media/popup_C08E4886_D090_6C4D_41E3_287AABE86716_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 407
   }
  ]
 },
 "pitch": -5.04,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D89CF36_202E_F585_41AA_5B9D7CB3BE73",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D89EF36_202E_F585_41B0_95D8B409BF23",
 "initialPosition": {
  "yaw": 66.05,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D5C7FCE_202E_9485_41BD_59DBEBBEDD20",
 "initialPosition": {
  "yaw": -125.31,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_t.jpg"
  }
 ],
 "label": "HIMATEKKIM-2020_03",
 "hfov": 360,
 "overlays": [
  "this.overlay_FC43B25A_EDE2_D86C_41E0_A2D926013203",
  "this.overlay_FF6AAEFF_EDFD_4824_41C6_2F88D8EDF626",
  "this.overlay_FE412BB3_EDA5_C83C_41ED_1BDEB7D858B2",
  "this.overlay_F4CB4C26_EDA3_C824_41EC_1CE162EA19B3",
  "this.overlay_93CF424D_8BF9_63CA_41D4_C75017D5278A",
  "this.overlay_9C1C6401_8BF9_273D_4192_5BAF90BDBFB1",
  "this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_tcap0",
  "this.popup_C2E8FF66_D325_F170_41D0_9E7FB18C203C",
  "this.popup_C2D349AF_D33A_B1F0_41C4_223F79EA2090"
 ],
 "id": "panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 174.68,
   "yaw": -79.65,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C"
  },
  {
   "backwardYaw": -72.7,
   "yaw": 82.84,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8"
  },
  {
   "backwardYaw": -135.36,
   "yaw": -158.87,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6"
  },
  {
   "backwardYaw": 124.99,
   "yaw": 159.3,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_t.jpg"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_t.jpg"
  }
 ],
 "label": "CEF-2017-10",
 "hfov": 360,
 "overlays": [
  "this.overlay_05358C10_14AA_F10B_41B0_7C9EAD488BA0",
  "this.overlay_0FE1A958_19C4_68A2_41B4_DC984F94F0DE",
  "this.overlay_9EF7E55D_8B4A_E1CA_41C3_0DE6CE32B50E",
  "this.overlay_9D270BC6_8B49_20C6_41E1_3A28C4DD465A",
  "this.overlay_9EC34F13_8B49_215D_41B0_CFA3001D7705",
  "this.popup_9EFE0EC8_8B49_20CB_41D4_DE46E9A63D0F",
  "this.panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_tcap0",
  "this.popup_C083DD05_D090_244F_41D1_26233EF31ADC",
  "this.popup_C1D56887_D090_6C4B_41D3_51AD4D4BD31B",
  "this.popup_C0284C66_D090_24CD_41E1_6F013884E173"
 ],
 "id": "panorama_1E557619_144B_2D23_41A0_05CAAAB2652E",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 98.38,
   "yaw": 104.78,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363"
  },
  {
   "backwardYaw": -83.2,
   "yaw": -99.24,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1E547224_144B_E561_41A3_10C4B18B9EE7"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D722777_202E_F583_41B1_D3A492389710",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D723777_202E_F583_419C_B9A38ABEDA4C",
 "initialPosition": {
  "yaw": 5.93,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.21,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C1983982_D090_6C45_41BF_C4ECEB63CADF",
 "yaw": 4.16,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C1983982_D090_6C45_41BF_C4ECEB63CADF_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 637
   },
   {
    "url": "media/popup_C1983982_D090_6C45_41BF_C4ECEB63CADF_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 419
   }
  ]
 },
 "pitch": -4.71,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-11",
 "hfov": 360,
 "overlays": [
  "this.overlay_FBC468E4_F6E1_3BD2_41BB_6C5E0BB53CB4",
  "this.panorama_FBC7B8E5_F6E1_3BD2_41E8_3E8E17D323D3",
  "this.overlay_F9D7CC49_F6E3_7AD5_4170_769834599FD7"
 ],
 "id": "panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 4.1,
   "yaw": -179.94,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_t.jpg"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_t.jpg"
  }
 ],
 "label": "HIMATEKKIM-2020_01",
 "hfov": 360,
 "overlays": [
  "this.overlay_FD59483E_F198_558F_41ED_A4B5406F8AAB",
  "this.overlay_FC8ACE29_F198_6DB5_41EC_90F6FD0C1041",
  "this.overlay_4B42DF27_5E40_663D_41B1_DBB0B989DD4B",
  "this.panorama_FBF97638_F199_BD93_41D1_05116F346852_tcap0"
 ],
 "id": "panorama_FBF97638_F199_BD93_41D1_05116F346852",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -65.22,
   "yaw": 24.96,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C"
  },
  {
   "backwardYaw": 118.61,
   "yaw": 110.12,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6"
  },
  {
   "backwardYaw": 0.32,
   "yaw": 166.46,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D37FCAC_202E_F485_41B7_B3FBCBC5B764",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D378CAC_202E_F485_41AF_2DFEF11780D3",
 "initialPosition": {
  "yaw": 44.64,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D5C07B1_202E_F49F_41B9_C2309F4E3817",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D5C17B1_202E_F49F_41B7_99932AB08C68",
 "initialPosition": {
  "yaw": -46.04,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4ACAD994_5E43_AA1C_41C0_A9288AF316BE",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_camera",
 "initialPosition": {
  "yaw": 163.49,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -6.61
 }
},
{
 "timeToIdle": 5000,
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2F818521_202E_F5BF_41AE_60DA4B86C052",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F819521_202E_F5BF_41B5_199C2A9CC0D5",
 "idleSequence": "this.sequence_2F818521_202E_F5BF_41AE_60DA4B86C052",
 "initialPosition": {
  "yaw": -179,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_t.jpg"
  }
 ],
 "label": "CEF-2019_01",
 "hfov": 360,
 "overlays": [
  "this.overlay_F3383C22_F8EB_7130_41AB_7A61D5CAE880",
  "this.overlay_F338CC22_F8EB_7130_41E3_85D055426C7F",
  "this.overlay_02F894F3_1A44_B966_419B_F945B01FE2A4",
  "this.overlay_9D5ABC89_8BCB_674A_41C3_20678BEE6D24",
  "this.panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_tcap0",
  "this.popup_C1990537_D1B0_244B_41DA_745F287DC290"
 ],
 "id": "panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -96.2,
   "yaw": 153.67,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1"
  },
  {
   "backwardYaw": 0.39,
   "yaw": 81.38,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79"
  },
  {
   "backwardYaw": 102.95,
   "yaw": -155.3,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.14,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C083DD05_D090_244F_41D1_26233EF31ADC",
 "yaw": -153.4,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C083DD05_D090_244F_41D1_26233EF31ADC_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 635
   },
   {
    "url": "media/popup_C083DD05_D090_244F_41D1_26233EF31ADC_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 417
   }
  ]
 },
 "pitch": -2.87,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 3.67,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D94CDD6E_D6EA_5735_4193_08B6A0C3A054",
 "yaw": 91.98,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D94CDD6E_D6EA_5735_4193_08B6A0C3A054_0_1.png",
    "width": 1024,
    "class": "ImageResourceLevel",
    "height": 785
   }
  ]
 },
 "pitch": 3.1,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "levels": [
  {
   "url": "media/popup_D9B4B83F_D6EB_DD12_41DD_58C38F38A71D_0_0.png",
   "width": 1366,
   "class": "ImageResourceLevel",
   "height": 1048
  },
  {
   "url": "media/popup_D9B4B83F_D6EB_DD12_41DD_58C38F38A71D_0_1.png",
   "width": 1024,
   "class": "ImageResourceLevel",
   "height": 785
  },
  {
   "url": "media/popup_D9B4B83F_D6EB_DD12_41DD_58C38F38A71D_0_2.png",
   "width": 512,
   "class": "ImageResourceLevel",
   "height": 392
  }
 ],
 "class": "ImageResource",
 "id": "ImageResource_C795F9A8_D6EE_7F3D_41AF_F973F2EC667F"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_t.jpg"
  }
 ],
 "label": "CEF-2019_03",
 "hfov": 360,
 "overlays": [
  "this.overlay_F45B765E_F829_1110_41D0_E5E403080B4B",
  "this.overlay_F45B365E_F829_1110_41C3_4BE2B594F3AE",
  "this.overlay_F45B265E_F829_1110_41D7_9C9ADCFC9330",
  "this.overlay_9D7B10D9_8BCF_E0CA_41CA_964BEC8F974E",
  "this.overlay_9C772E2A_8BCF_634E_41BA_B5888B6FA322",
  "this.panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_tcap0",
  "this.popup_C1844106_D190_3C4D_41CD_8AC07C6DAFDA",
  "this.popup_C0A660F5_D190_1DCF_41D9_393B74A28BBE"
 ],
 "id": "panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -92.14,
   "yaw": 87.47,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7"
  },
  {
   "backwardYaw": 153.67,
   "yaw": -96.2,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5"
  },
  {
   "backwardYaw": -161.22,
   "yaw": -153.05,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E2CA95B_202E_FD83_41B2_A28373FDF40E",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E2D595B_202E_FD83_419C_62AAE77CDC9D",
 "initialPosition": {
  "yaw": -75.22,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D9E004B_202E_8B83_41B1_B05C59816D3A",
 "initialPosition": {
  "yaw": -105.59,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA3A22_5E43_AE34_41BA_21FD59F9B59E",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_FBF97638_F199_BD93_41D1_05116F346852_camera",
 "initialPosition": {
  "yaw": 82.98,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -4.13
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F2A3BDA_202E_FC8D_41A9_590740CCFECF",
 "initialPosition": {
  "yaw": -104.78,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_t.jpg"
  }
 ],
 "label": "HIMATEKKIM-2021_02",
 "hfov": 360,
 "overlays": [
  "this.overlay_E186DF6D_F189_AB8D_41D8_BF555EE000C1",
  "this.overlay_FE3A09E5_F188_D6BD_41CD_F3F63F54B663",
  "this.overlay_9C76EE43_8B4F_233E_41D4_9D7941BAD051",
  "this.overlay_9C6FF902_8B4B_E13E_41C1_4220D5719201",
  "this.overlay_9C96ED0F_8B49_6146_4196_AA28599C014E",
  "this.overlay_9C7E6549_8B47_E1CD_41B6_842A37145C06",
  "this.panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_tcap0",
  "this.popup_D80973A1_D69E_732E_41B4_2CA10CF93671",
  "this.popup_D85D2714_D69E_F316_41BE_E8BEAD5AF76C",
  "this.popup_D8F7DE7C_D69F_B516_41D3_A8E99ADF690E",
  "this.popup_D84CABAC_D69F_B336_41E6_A15D94113D39"
 ],
 "id": "panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -11.55,
   "yaw": -91.58,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64"
  },
  {
   "backwardYaw": -70.62,
   "yaw": 118.82,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F8567126_F198_57BF_41D9_3446ACE07DBB"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.26,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_DF32787E_D0F0_2CBD_41C5_82E55182878D",
 "yaw": -159.01,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_DF32787E_D0F0_2CBD_41C5_82E55182878D_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 639
   },
   {
    "url": "media/popup_DF32787E_D0F0_2CBD_41C5_82E55182878D_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 420
   }
  ]
 },
 "pitch": -3.6,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.2,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C00A57C2_D090_E3C5_41DF_67E1A1B3DCA0",
 "yaw": -8.41,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C00A57C2_D090_E3C5_41DF_67E1A1B3DCA0_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 627
   },
   {
    "url": "media/popup_C00A57C2_D090_E3C5_41DF_67E1A1B3DCA0_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 412
   }
  ]
 },
 "pitch": -5.65,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2ED8FB7E_202E_FD85_41BA_575BD6AF8D37",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2ED89B7E_202E_FD85_416E_F10861B43D7A",
 "initialPosition": {
  "yaw": -14.46,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAABA25_5E43_AE3C_41A9_2422193DDF3D",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_camera",
 "initialPosition": {
  "yaw": 98.58,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -1.76
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2F2CC3CB_202E_8C83_41B2_5E15BA9622F8",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F2CD3CB_202E_8C83_41AA_20A0B2F4F9B4",
 "initialPosition": {
  "yaw": 100.21,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D98E85E_202E_FB85_41A8_193DF40BC1ED",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D98F85E_202E_FB85_41A1_CDD03168AE22",
 "initialPosition": {
  "yaw": 123.35,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DAC8ED8_202E_F48D_41BA_DD88DAD8F87B",
 "initialPosition": {
  "yaw": 52.87,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D656E3D_202E_F787_41AE_4E1FDC28BDAF",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D650E3D_202E_F787_41A4_6EBEB4D91438",
 "initialPosition": {
  "yaw": 83.98,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-16",
 "hfov": 360,
 "overlays": [
  "this.overlay_45155A8F_5DC0_EE0C_41D7_955C01DFD58C",
  "this.overlay_468C0E0E_5DC3_A60C_41B3_CDF9643188CD",
  "this.overlay_439E6B92_5DC0_6E17_41AB_59157436BB5E",
  "this.overlay_93E39622_8B4B_237E_41E0_DF458B61011D",
  "this.overlay_93E4A031_8B49_5F5D_41BB_4337338B6E06",
  "this.panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_tcap0",
  "this.popup_D98D3810_D6EA_5CED_41DD_B3A503553ACE",
  "this.popup_D9BEA37E_D6ED_B312_41EA_9E72B449870A"
 ],
 "id": "panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 128.31,
   "yaw": 68.72,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64"
  },
  {
   "backwardYaw": 75.22,
   "yaw": -160.28,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA"
  },
  {
   "backwardYaw": 123.58,
   "yaw": 161.31,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E3AF92C_202E_FD85_41A2_B682FC271CA1",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E3A992C_202E_FD85_41AE_6770856C3922",
 "initialPosition": {
  "yaw": 24.7,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA2A23_5E43_AE34_41CE_B00E3AE1B649",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_camera",
 "initialPosition": {
  "yaw": 75.55,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -15.28
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2EA50AE9_202E_FC8F_41BF_1584192CA2A7",
 "initialPosition": {
  "yaw": 174,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2EFA6302_202E_8D7D_41B3_72BAEE8981E0",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2EFA7302_202E_8D7D_41A0_E2CA4503490B",
 "initialPosition": {
  "yaw": 114.78,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CA59A1F_5E43_AE0C_41D3_1A5B008EC1A2",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_camera",
 "initialPosition": {
  "yaw": 126.33,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E7F79E5_202E_FC87_414F_3284EC3993D8",
 "initialPosition": {
  "yaw": -57.31,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DBB2804_202E_FB85_41BF_9390912D11B6",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DBBC804_202E_FB85_41BA_5206F1438AA5",
 "initialPosition": {
  "yaw": 87.67,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-14",
 "hfov": 360,
 "overlays": [
  "this.overlay_4F6440F1_5E40_7A14_41D7_2CD9407D7600",
  "this.overlay_4CB6F8B1_5E41_AA14_41D1_E11C9399CA49",
  "this.overlay_4BE7C4E0_5E47_9A34_41C1_120083BB954A",
  "this.overlay_9C7FA14B_8B49_21CD_41D9_0A86D29973AE",
  "this.overlay_9393A09C_8B49_FF4B_41B1_72FC57C6A9D7",
  "this.overlay_93943E39_8B49_234D_41C3_F09AC2E10863",
  "this.popup_9C483117_8B49_2145_41D4_B0C51096F608",
  "this.panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_tcap0",
  "this.popup_D9B4B83F_D6EB_DD12_41DD_58C38F38A71D",
  "this.popup_C6F8A0C6_D6EA_4D72_41A3_F57B7E281005",
  "this.popup_D913CFB6_D6EA_B312_41E4_95719FBB1846"
 ],
 "id": "panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -160.28,
   "yaw": 75.22,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41"
  },
  {
   "backwardYaw": 132.5,
   "yaw": -121.47,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69"
  },
  {
   "class": "AdjacentPanorama",
   "panorama": "this.panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2CD21610_202E_F79D_417A_3A8D8DE01C9C",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2CD22610_202E_F79D_41B1_C4B98F2CFF27",
 "initialPosition": {
  "yaw": -37.22,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D78BF92_202E_949D_41B0_F0A0D4BE02BD",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D795F92_202E_949D_41AD_086B986C8F32",
 "initialPosition": {
  "yaw": -120.21,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C0E728B2_D0F0_2C45_4182_1AF8EB7707A3",
 "yaw": -161.2,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C0E728B2_D0F0_2C45_4182_1AF8EB7707A3_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 627
   },
   {
    "url": "media/popup_C0E728B2_D0F0_2C45_4182_1AF8EB7707A3_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 412
   }
  ]
 },
 "pitch": -3.72,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DAF2822_202E_FBBD_4166_0EB6E3C551E4",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DAF3822_202E_FBBD_418F_3E6E0BEF9B6C",
 "initialPosition": {
  "yaw": 37.63,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E138169_202E_8D8F_418C_7052A9465B5F",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E13A169_202E_8D8F_41B7_2AF7C3AD8499",
 "initialPosition": {
  "yaw": -3.96,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.52,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_DF4D5460_D090_E4C5_41E2_3F0A5DA77A2E",
 "yaw": 28.01,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_DF4D5460_D090_E4C5_41E2_3F0A5DA77A2E_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 631
   },
   {
    "url": "media/popup_DF4D5460_D090_E4C5_41E2_3F0A5DA77A2E_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 415
   }
  ]
 },
 "pitch": -4.52,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D4C5FEE_202E_9485_41B5_C63BAFD776E7",
 "initialPosition": {
  "yaw": -158.39,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 6.85,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D98D3810_D6EA_5CED_41DD_B3A503553ACE",
 "yaw": -82.05,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D98D3810_D6EA_5CED_41DD_B3A503553ACE_0_0.png",
    "width": 800,
    "class": "ImageResourceLevel",
    "height": 790
   },
   {
    "url": "media/popup_D98D3810_D6EA_5CED_41DD_B3A503553ACE_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 505
   }
  ]
 },
 "pitch": 1.94,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.79,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C2E8FF66_D325_F170_41D0_9E7FB18C203C",
 "yaw": -22.88,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C2E8FF66_D325_F170_41D0_9E7FB18C203C_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 659
   },
   {
    "url": "media/popup_C2E8FF66_D325_F170_41D0_9E7FB18C203C_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 433
   }
  ]
 },
 "pitch": -6.27,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.21,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C049176A_D08F_E4C5_4187_C9C0D55A0FB8",
 "yaw": 146.25,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C049176A_D08F_E4C5_4187_C9C0D55A0FB8_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 636
   },
   {
    "url": "media/popup_C049176A_D08F_E4C5_4187_C9C0D55A0FB8_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 418
   }
  ]
 },
 "pitch": -4.83,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 2.92,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C6F8A0C6_D6EA_4D72_41A3_F57B7E281005",
 "yaw": -35.52,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C6F8A0C6_D6EA_4D72_41A3_F57B7E281005_0_1.png",
    "width": 587,
    "class": "ImageResourceLevel",
    "height": 1024
   }
  ]
 },
 "pitch": 27.66,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F7F5425_202E_8B87_41BF_F5B375D108D4",
 "initialPosition": {
  "yaw": 62.68,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2EE75B61_202E_FDBF_41A6_32B7ACED64E9",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2EE76B61_202E_FDBF_41BE_7D4F698A2055",
 "initialPosition": {
  "yaw": 25.53,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D3DE684_202E_F485_41A3_157D1215552A",
 "initialPosition": {
  "yaw": 142.68,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "levels": [
  {
   "url": "media/popup_D9F5424F_D696_4D73_41E1_23EEEFCFBBC3_0_0.png",
   "width": 1280,
   "class": "ImageResourceLevel",
   "height": 827
  },
  {
   "url": "media/popup_D9F5424F_D696_4D73_41E1_23EEEFCFBBC3_0_1.png",
   "width": 1024,
   "class": "ImageResourceLevel",
   "height": 661
  },
  {
   "url": "media/popup_D9F5424F_D696_4D73_41E1_23EEEFCFBBC3_0_2.png",
   "width": 512,
   "class": "ImageResourceLevel",
   "height": 330
  }
 ],
 "class": "ImageResource",
 "id": "ImageResource_C786F98D_D6EE_7FF7_41DB_BFC4FC805B35"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D8F906B_202E_8B83_41C0_1EBD97392E99",
 "initialPosition": {
  "yaw": 106.07,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAB5A26_5E43_AE3C_41BF_BA7BB0543797",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_camera",
 "initialPosition": {
  "yaw": -97.34,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 28
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2F5E7469_202E_8B8F_419F_C66D98C854FD",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F5E1469_202E_8B8F_41BA_1CA77E13B4D0",
 "initialPosition": {
  "yaw": -64.89,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E32D125_202E_8D87_41BD_81EC2F53E098",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E32E125_202E_8D87_41B1_5BE7EB284AD6",
 "initialPosition": {
  "yaw": -98.62,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "CEF-2017-08",
 "hfov": 360,
 "overlays": [
  "this.overlay_0A21F9D7_19FD_EBAE_41B9_786AC000F6D9",
  "this.panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_tcap0"
 ],
 "id": "panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -63.08,
   "yaw": 74.91,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_t.jpg"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "CEF-2017-06",
 "hfov": 360,
 "overlays": [
  "this.overlay_0912DEC2_19CD_A9A6_41AB_5BDFD19B269C",
  "this.overlay_0A75156C_19CC_9B62_41B4_B266D2BB2334",
  "this.overlay_0B7A8A27_19C3_A8EE_417B_4BA3849872B3",
  "this.overlay_0C12C251_19CC_78A2_4182_9B4A757968BA",
  "this.panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_tcap0"
 ],
 "id": "panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "panorama": "this.panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C"
  },
  {
   "class": "AdjacentPanorama",
   "panorama": "this.panorama_12A6D467_19BD_996E_419C_647D99E534FD"
  },
  {
   "backwardYaw": 115.11,
   "yaw": -65.59,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746"
  },
  {
   "backwardYaw": 145.13,
   "yaw": 133.96,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAB7A26_5E43_AE3C_417D_81C3D6AB32A1",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_camera",
 "initialPosition": {
  "yaw": -134.12,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -6.72
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D01EDE3_202E_F483_41A5_DE9A4DA3A57F",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D018DE2_202E_F4BD_418A_F62D327F9837",
 "initialPosition": {
  "yaw": -81.62,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C0284C66_D090_24CD_41E1_6F013884E173",
 "yaw": 153.79,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C0284C66_D090_24CD_41E1_6F013884E173_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 635
   },
   {
    "url": "media/popup_C0284C66_D090_24CD_41E1_6F013884E173_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 417
   }
  ]
 },
 "pitch": -3.93,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "CEF-2017-07",
 "hfov": 360,
 "overlays": [
  "this.overlay_0B140046_19FC_78AE_41B3_C027A83F53B0",
  "this.overlay_0B289710_19FF_B8A2_41A1_6756F7B6430C",
  "this.panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_tcap0"
 ],
 "id": "panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -65.59,
   "yaw": 115.11,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE"
  },
  {
   "backwardYaw": 74.91,
   "yaw": -63.08,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_t.jpg"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-13",
 "hfov": 360,
 "overlays": [
  "this.overlay_49213531_5D0E_27D9_41C8_C93A8734A9E3",
  "this.overlay_52FCA697_5E40_661D_4171_8B45A9D6F0E6",
  "this.overlay_4E99376C_5E41_E633_41C5_993095B80481",
  "this.overlay_40AC2DBB_6240_AA14_41CF_D95532F41AA9",
  "this.overlay_9C76287F_8B49_6FC6_41DF_C4A038B9D5DB",
  "this.overlay_9C28DB89_8B49_E14D_41D6_5DCD2CB3B961",
  "this.overlay_935E77AB_8B4E_E14E_41A4_042CF5DC4549",
  "this.overlay_9318D608_8B4F_234B_41D5_39850653618F",
  "this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_tcap0",
  "this.popup_D98D7E0E_D6EA_54F2_41D3_36841F916DCC",
  "this.popup_D9BC7DE7_D6EA_D733_41E5_D608406EDFEC",
  "this.popup_D95CEA5E_D6EA_BD12_41E0_21FCE4B06EF7",
  "this.popup_D94CDD6E_D6EA_5735_4193_08B6A0C3A054"
 ],
 "id": "panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 138.53,
   "yaw": -127.13,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55"
  },
  {
   "backwardYaw": 88.3,
   "yaw": -73.93,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA"
  },
  {
   "backwardYaw": -121.47,
   "yaw": 132.5,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA"
  },
  {
   "backwardYaw": -6,
   "yaw": 170.59,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DA23EF8_202E_F48D_41B1_E4E913D9A251",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DA2DEF8_202E_F48D_41A6_7405B5538F6B",
 "initialPosition": {
  "yaw": -92.53,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "CEF-2017-04",
 "hfov": 360,
 "overlays": [
  "this.overlay_08F7FBE8_19C4_EF62_41B9_491191D8C0DF",
  "this.overlay_0977CEA3_19C5_A9E6_41AB_6EC97C414062",
  "this.overlay_005323CB_1A44_9FA6_41AB_E77FB56E6DA5",
  "this.overlay_9D31398D_8B7A_E145_41E0_C28146784950",
  "this.overlay_9398B8A6_8B79_6F47_419D_729495D8BEC5",
  "this.overlay_9DE3875E_8B47_21C7_41CC_CDE0F8EEEC61",
  "this.panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_tcap0",
  "this.popup_DF32787E_D0F0_2CBD_41C5_82E55182878D",
  "this.popup_C049176A_D08F_E4C5_4187_C9C0D55A0FB8",
  "this.popup_C0509643_D090_64CB_41D3_8C27BAAA6356"
 ],
 "id": "panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 107.74,
   "yaw": 78.23,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB"
  },
  {
   "backwardYaw": 59.79,
   "yaw": -72.02,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61"
  },
  {
   "backwardYaw": 59.79,
   "yaw": -106.52,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.14,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C0A367D5_D090_23CF_41DC_40DB848226AD",
 "yaw": -32.02,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C0A367D5_D090_23CF_41DC_40DB848226AD_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 635
   },
   {
    "url": "media/popup_C0A367D5_D090_23CF_41DC_40DB848226AD_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 417
   }
  ]
 },
 "pitch": -3.72,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.19,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C2F4F500_D33A_76B0_41E8_691F0E88A4C6",
 "yaw": -9.88,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C2F4F500_D33A_76B0_41E8_691F0E88A4C6_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 659
   },
   {
    "url": "media/popup_C2F4F500_D33A_76B0_41E8_691F0E88A4C6_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 433
   }
  ]
 },
 "pitch": -6.86,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "levels": [
  {
   "url": "media/popup_D93636C6_D696_B572_41D3_A9389E95088F_0_0.png",
   "width": 1366,
   "class": "ImageResourceLevel",
   "height": 1033
  },
  {
   "url": "media/popup_D93636C6_D696_B572_41D3_A9389E95088F_0_1.png",
   "width": 1024,
   "class": "ImageResourceLevel",
   "height": 774
  },
  {
   "url": "media/popup_D93636C6_D696_B572_41D3_A9389E95088F_0_2.png",
   "width": 512,
   "class": "ImageResourceLevel",
   "height": 387
  }
 ],
 "class": "ImageResource",
 "id": "ImageResource_C7850994_D6EE_7F15_41C4_7FEAB5330B80"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "CEF-2017-05",
 "hfov": 360,
 "overlays": [
  "this.overlay_0C578D91_19C4_6BA2_41B1_88ABBC35615D",
  "this.overlay_0D882EA5_19C4_A9ED_4199_B7DA3E4A5E1B",
  "this.overlay_0D385434_19C3_98E2_4176_749EFE452DA2",
  "this.overlay_9D6BAABA_8B4B_634E_41BC_8D56557FB59A",
  "this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_tcap0",
  "this.popup_C1983982_D090_6C45_41BF_C4ECEB63CADF"
 ],
 "id": "panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -72.02,
   "yaw": 59.79,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C"
  },
  {
   "backwardYaw": 133.96,
   "yaw": 145.13,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE"
  },
  {
   "backwardYaw": 52.29,
   "yaw": -56.65,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A6D467_19BD_996E_419C_647D99E534FD"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DFFF08B_202E_8C83_41BF_4372E1BE076D",
 "initialPosition": {
  "yaw": -18.69,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D1C2D49_202E_F58F_41AB_5A121CDE8BE3",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D1CCD48_202E_F58E_41B4_EC020AAD32B4",
 "initialPosition": {
  "yaw": -69.88,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "levels": [
  {
   "url": "media/popup_D9CD950E_D696_54F5_41D7_E27C55E90915_0_0.png",
   "width": 1366,
   "class": "ImageResourceLevel",
   "height": 1016
  },
  {
   "url": "media/popup_D9CD950E_D696_54F5_41D7_E27C55E90915_0_1.png",
   "width": 1024,
   "class": "ImageResourceLevel",
   "height": 761
  },
  {
   "url": "media/popup_D9CD950E_D696_54F5_41D7_E27C55E90915_0_2.png",
   "width": 512,
   "class": "ImageResourceLevel",
   "height": 380
  }
 ],
 "class": "ImageResource",
 "id": "ImageResource_C7857993_D6EE_7F13_41E9_9817526D66A7"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D2636E5_202E_F487_4199_A5D5E73FA90D",
 "initialPosition": {
  "yaw": -36.82,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 2.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9B7F444_D69E_5575_41D4_2CF331886357",
 "yaw": -34.39,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9B7F444_D69E_5575_41D4_2CF331886357_0_0.png",
    "width": 773,
    "class": "ImageResourceLevel",
    "height": 629
   },
   {
    "url": "media/popup_D9B7F444_D69E_5575_41D4_2CF331886357_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 416
   }
  ]
 },
 "pitch": -4.39,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DC9890E_202E_FD85_41B9_18D67A757428",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DCA490E_202E_FD85_41AE_7C578B5075E9",
 "initialPosition": {
  "yaw": 48.73,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2FB0B4BE_202E_F485_41B3_248F6E7ED5B7",
 "initialPosition": {
  "yaw": -55.07,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.2,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C016B8FA_D090_6DC4_41E0_AE5EE75FC8A2",
 "yaw": 8.88,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C016B8FA_D090_6DC4_41E0_AE5EE75FC8A2_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 631
   },
   {
    "url": "media/popup_C016B8FA_D090_6DC4_41E0_AE5EE75FC8A2_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 415
   }
  ]
 },
 "pitch": -5.86,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E437A99_202E_FC8F_41A7_AE0DE01A62CE",
 "initialPosition": {
  "yaw": -91.7,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 3.88,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9C29F8E_D69A_53F5_41E6_1BFB17B3B974",
 "yaw": 84.86,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9C29F8E_D69A_53F5_41E6_1BFB17B3B974_0_0.png",
    "width": 830,
    "class": "ImageResourceLevel",
    "height": 735
   },
   {
    "url": "media/popup_D9C29F8E_D69A_53F5_41E6_1BFB17B3B974_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 453
   }
  ]
 },
 "pitch": 1.13,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.36,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C1D2BF53_D090_64CB_41C7_ED3885933E5C",
 "yaw": -32.55,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C1D2BF53_D090_64CB_41C7_ED3885933E5C_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 629
   },
   {
    "url": "media/popup_C1D2BF53_D090_64CB_41C7_ED3885933E5C_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 413
   }
  ]
 },
 "pitch": -4.42,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2CC2766A_202E_F78D_41BE_9D404950643E",
 "initialPosition": {
  "yaw": 48.87,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E24414B_202E_8D83_41B6_5419ACC3D59F",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E24614B_202E_8D83_41BD_CC9A131B4D59",
 "initialPosition": {
  "yaw": 100.35,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.89,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C1990537_D1B0_244B_41DA_745F287DC290",
 "yaw": -72.62,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C1990537_D1B0_244B_41DA_745F287DC290_0_0.png",
    "width": 800,
    "class": "ImageResourceLevel",
    "height": 592
   },
   {
    "url": "media/popup_C1990537_D1B0_244B_41DA_745F287DC290_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 378
   }
  ]
 },
 "pitch": -5.66,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2EB3DAC6_202E_FC85_41A3_2D7E635CB23B",
 "initialPosition": {
  "yaw": 58.53,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_t.jpg"
  }
 ],
 "label": "HIMATEKKIM-2020_04",
 "hfov": 360,
 "overlays": [
  "this.overlay_FBCBC83C_EDA5_4824_41B0_C8D01F688D93",
  "this.overlay_FB6902FE_EDA6_D824_41E9_F8DA0862F42A",
  "this.overlay_F96D7EF2_EE62_C83C_41CC_F41C3EEEE4B3",
  "this.overlay_7C925112_6243_BA14_4171_70994087672B",
  "this.overlay_9C839EBD_8BC6_E34A_41D8_5D9679DE4C9F",
  "this.panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_tcap0",
  "this.popup_C20487D9_D33A_9150_41DA_27461F12546D"
 ],
 "id": "panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 82.84,
   "yaw": -72.7,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5"
  },
  {
   "class": "AdjacentPanorama",
   "panorama": "this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6"
  },
  {
   "backwardYaw": 163.9,
   "yaw": 173.72,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD"
  },
  {
   "backwardYaw": -79.79,
   "yaw": 100.6,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.71,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D95CEA5E_D6EA_BD12_41E0_21FCE4B06EF7",
 "yaw": 49.99,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D95CEA5E_D6EA_BD12_41E0_21FCE4B06EF7_0_1.png",
    "width": 1024,
    "class": "ImageResourceLevel",
    "height": 765
   }
  ]
 },
 "pitch": 6.09,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA0A22_5E43_AE34_41D6_AB02A1CFBA2F",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_camera",
 "initialPosition": {
  "yaw": -102.39,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -6.61
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D86D6F24_D69A_B335_41E9_37CBA15E74B1",
 "yaw": 165.88,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D86D6F24_D69A_B335_41E9_37CBA15E74B1_0_0.png",
    "width": 830,
    "class": "ImageResourceLevel",
    "height": 666
   },
   {
    "url": "media/popup_D86D6F24_D69A_B335_41E9_37CBA15E74B1_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 410
   }
  ]
 },
 "pitch": -4.12,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAB0A27_5E43_AE3C_41A6_5479FDA190F4",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_camera",
 "initialPosition": {
  "yaw": -173.39,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -1.76
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-08",
 "hfov": 360,
 "overlays": [
  "this.overlay_4DA42591_59A2_0A7A_41BD_A4329674AABC",
  "this.overlay_4A00726A_59A2_0EAE_41B5_1272DBC33EAA",
  "this.overlay_4C926DFA_59A3_F5AE_41A7_2C33B596B98F",
  "this.overlay_531EEF29_5D0E_23C9_41D5_2F34D8EBF2D0",
  "this.overlay_937B4B3D_8B59_214A_41BF_DAE2BE500F87",
  "this.overlay_91B4FB58_8B46_E1CA_41C2_A908818EF4F3",
  "this.overlay_9307B0C0_8B47_FF3B_41C9_15CDAD4F9173",
  "this.overlay_9C33CFB7_8B47_2146_41AE_9A7215614BD7",
  "this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_tcap0",
  "this.popup_D9839D16_D697_F712_41E0_0E1BF02FF1D2",
  "this.popup_D9CD950E_D696_54F5_41D7_E27C55E90915",
  "this.popup_D93636C6_D696_B572_41D3_A9389E95088F",
  "this.popup_D9241176_D696_CF12_41E4_1E0ED2648F92"
 ],
 "id": "panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 16.07,
   "yaw": 157.5,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55"
  },
  {
   "backwardYaw": -117.76,
   "yaw": -131.13,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827"
  },
  {
   "backwardYaw": -117.32,
   "yaw": 92.13,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA"
  },
  {
   "backwardYaw": 94.52,
   "yaw": -92.18,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DD678F0_202E_FC9D_41AE_4D5ED9EB4823",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DD608F0_202E_FC9D_41A1_32C3D7957812",
 "initialPosition": {
  "yaw": 26.95,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E7541A8_202E_8C8D_41B5_F543FF8E4246",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E7561A8_202E_8C8D_41B1_A71B7E47779A",
 "initialPosition": {
  "yaw": 84.48,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2CC8164F_202E_F783_41BA_7ADCA04F5172",
 "initialPosition": {
  "yaw": -47.5,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "mouseControlMode": "drag_rotation",
 "class": "PanoramaPlayer",
 "buttonCardboardView": "this.IconButton_E3081138_EDA3_D82C_41E8_15DF452055EA",
 "touchControlMode": "drag_rotation",
 "gyroscopeEnabled": true,
 "viewerArea": "this.MainViewer",
 "id": "MainViewerPanoramaPlayer",
 "displayPlaybackBar": true,
 "gyroscopeVerticalDraggingEnabled": true
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DF028B7_202E_FC83_41BB_E045AF597CC6",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DF038B7_202E_FC83_41B3_E0D93B95288F",
 "initialPosition": {
  "yaw": 74.93,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2FA0D4DF_202E_F483_41B3_5398EEC39976",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2FA0F4DE_202E_F485_41B0_332E270CE42D",
 "initialPosition": {
  "yaw": -61.18,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-12",
 "hfov": 360,
 "overlays": [
  "this.overlay_4F1DD81D_5D0A_2DC9_41A3_EA15A98B1021",
  "this.overlay_49DFD4E5_5D0F_E679_41D1_146FDEF85EDB",
  "this.overlay_4DF5CA4B_5E40_AE75_41C9_F69E1636E48C",
  "this.overlay_9C071435_8B4B_2745_41D1_80690DBE70A6",
  "this.overlay_93EEB843_8B4B_6F3E_41D3_BBAF4293F9CF",
  "this.popup_9C1A63FF_8B4B_20C5_41CC_775AEA99B7BE",
  "this.panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_tcap0",
  "this.popup_D9814A4E_D695_DD75_41B1_6706B567BE99",
  "this.popup_D988123E_D695_CD12_41E2_86019132F0C0"
 ],
 "id": "panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 92.13,
   "yaw": -117.32,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F"
  },
  {
   "backwardYaw": 74.41,
   "yaw": 170.5,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55"
  },
  {
   "backwardYaw": -73.93,
   "yaw": 88.3,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CA5BA20_5E43_AE34_41D0_6BD8E374AFE8",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_camera",
 "initialPosition": {
  "yaw": -109.4,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0.41
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA0A28_5E43_AE34_41A6_93B5466D0214",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_camera",
 "initialPosition": {
  "yaw": 52.7,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -3
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E617A3E_202E_FF85_4157_67D71C2638B7",
 "initialPosition": {
  "yaw": 158.29,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F1DA3E8_202E_8C8D_41AB_0F8B04488A9B",
 "initialPosition": {
  "yaw": -163.93,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA6A22_5E43_AE34_41A8_2F60FE799227",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_camera",
 "initialPosition": {
  "yaw": -170.5,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -4.95
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 8.52,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D8030674_D69A_D516_41DE_4A7F21C38C32",
 "yaw": -119.33,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D8030674_D69A_D516_41DE_4A7F21C38C32_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 602
   },
   {
    "url": "media/popup_D8030674_D69A_D516_41DE_4A7F21C38C32_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 396
   }
  ]
 },
 "pitch": 3.1,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2F3CC3AC_202E_8C85_4186_857FC4071B80",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F3CE3AC_202E_8C85_4168_2A0DB30B9018",
 "initialPosition": {
  "yaw": -16.1,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F0DD406_202E_8B85_41B7_2424DD11B8D9",
 "initialPosition": {
  "yaw": 62.24,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.36,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C04186D6_D090_25CD_41E8_5E98D6DE4735",
 "yaw": -157.71,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C04186D6_D090_25CD_41E8_5E98D6DE4735_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 629
   },
   {
    "url": "media/popup_C04186D6_D090_25CD_41E8_5E98D6DE4735_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 413
   }
  ]
 },
 "pitch": -3.42,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_t.jpg"
  }
 ],
 "label": "CEF-2017-03",
 "hfov": 360,
 "overlays": [
  "this.overlay_048F73BE_14A9_5776_4197_902078065B08",
  "this.overlay_16D18303_19DC_98A6_41B6_420CFC7C766E",
  "this.overlay_9DBA99EE_8B79_60C7_41D6_852C0DBD2495",
  "this.panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_tcap0",
  "this.popup_C0167782_D0F0_2445_41B6_21C0F38920E4"
 ],
 "id": "panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 78.23,
   "yaw": 107.74,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C"
  },
  {
   "backwardYaw": -92.33,
   "yaw": -105.07,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1E682630_144B_2D61_419D_2C5C83D95A53"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.12,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9CD950E_D696_54F5_41D7_E27C55E90915",
 "yaw": 4.81,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9CD950E_D696_54F5_41D7_E27C55E90915_0_1.png",
    "width": 1024,
    "class": "ImageResourceLevel",
    "height": 761
   }
  ]
 },
 "pitch": 19.35,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D51F7CE_202E_F485_41B5_F89AD33F3B06",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D5197CE_202E_F485_41B7_A54C2B2F56A5",
 "initialPosition": {
  "yaw": -127.71,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "CEF-2017-15",
 "hfov": 360,
 "overlays": [
  "this.overlay_026733EF_1A44_9F7E_41B2_98B754B215F0",
  "this.overlay_061DBD61_1A44_AB62_41B7_FDA97256CA8C",
  "this.overlay_062AFC99_1A45_A9A2_41B7_628B647C4FDE",
  "this.panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_tcap0",
  "this.overlay_DC45425B_D365_B350_41DE_45A497726315"
 ],
 "id": "panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -116.37,
   "yaw": 122.24,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA"
  },
  {
   "class": "AdjacentPanorama",
   "panorama": "this.panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE"
  },
  {
   "backwardYaw": 164.8,
   "yaw": 161.38,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A6D467_19BD_996E_419C_647D99E534FD"
  },
  {
   "backwardYaw": 81.38,
   "yaw": 0.39,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.77,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D841E06C_D69A_4D36_41E8_4A20160FD43A",
 "yaw": -18.47,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D841E06C_D69A_4D36_41E8_4A20160FD43A_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 546
   },
   {
    "url": "media/popup_D841E06C_D69A_4D36_41E8_4A20160FD43A_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 359
   }
  ]
 },
 "pitch": 24.12,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.77,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D85D2714_D69E_F316_41BE_E8BEAD5AF76C",
 "yaw": 43.9,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D85D2714_D69E_F316_41BE_E8BEAD5AF76C_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 678
   },
   {
    "url": "media/popup_D85D2714_D69E_F316_41BE_E8BEAD5AF76C_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 446
   }
  ]
 },
 "pitch": 1.52,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D6A1FB0_202E_949D_417B_AE60B47D2C74",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D6A3FB0_202E_949D_4158_46EA4BF889F8",
 "initialPosition": {
  "yaw": -120.21,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.62,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D913CFB6_D6EA_B312_41E4_95719FBB1846",
 "yaw": -34.52,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D913CFB6_D6EA_B312_41E4_95719FBB1846_0_0.png",
    "width": 800,
    "class": "ImageResourceLevel",
    "height": 774
   },
   {
    "url": "media/popup_D913CFB6_D6EA_B312_41E4_95719FBB1846_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 495
   }
  ]
 },
 "pitch": -5.89,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_t.jpg"
  }
 ],
 "label": "CEF-2019_04",
 "hfov": 360,
 "overlays": [
  "this.overlay_E6CFD8ED_F6F3_825D_41E8_418C5030BC71",
  "this.overlay_E6D26A0B_F6F4_81C6_41B1_3E5C4F11DA1E",
  "this.overlay_F91D4B77_F6F5_864E_41BE_B69ACB973A21",
  "this.overlay_9C2FF11C_8BC9_6147_41DB_A3492B43B690",
  "this.panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_tcap0",
  "this.popup_C1B7142E_D190_245D_41C8_6E04A1650973"
 ],
 "id": "panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 87.47,
   "yaw": -92.14,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1"
  },
  {
   "backwardYaw": -169.99,
   "yaw": 165.54,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21"
  },
  {
   "backwardYaw": -113.95,
   "yaw": -131.27,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_t.jpg"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_t.jpg"
  }
 ],
 "label": "CEF-2019_02",
 "hfov": 360,
 "overlays": [
  "this.overlay_F46A5E5F_F8D7_1110_41B3_DD38B8485771",
  "this.overlay_F46A4E5F_F8D7_1110_41B6_DA2653D45080",
  "this.overlay_F46A6E5F_F8D7_1110_41B5_4659FF454C20",
  "this.overlay_9C06385E_8BC9_2FC6_41D9_83E1346F4C81",
  "this.overlay_9D76A5B0_8BC9_E15B_41C6_4DC6838FEF00",
  "this.panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_tcap0",
  "this.popup_C1D948AD_D1B0_2C5F_41E7_6F23D711C545",
  "this.popup_C1E128D5_D1B0_6DCF_419F_4F716E54F5F2"
 ],
 "id": "panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -153.05,
   "yaw": -161.22,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1"
  },
  {
   "backwardYaw": -131.27,
   "yaw": -113.95,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7"
  },
  {
   "backwardYaw": -155.3,
   "yaw": 102.95,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E0F59B8_202E_FC8D_4183_5779D1FDFA96",
 "initialPosition": {
  "yaw": 87.82,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DD2C0E5_202E_8C87_418F_E4785E0727BF",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DD2F0E4_202E_8C85_41B2_B54713D11158",
 "initialPosition": {
  "yaw": 63.63,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_t.jpg"
  }
 ],
 "label": "HIMATEKKIM-2020_02",
 "hfov": 360,
 "overlays": [
  "this.overlay_E2A77298_EDBF_F8EC_41DF_CED93AFA7947",
  "this.overlay_F589190F_EDA2_C9E4_41E6_16082ED87566",
  "this.overlay_FC351729_F187_BBB5_41E6_0E582C7E3B51",
  "this.overlay_9CCF1017_8BF9_7F46_41D9_289613B32ADF",
  "this.overlay_9CC15341_8BFB_213D_4195_C0AE9FC46BA2",
  "this.panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_tcap0",
  "this.popup_C3D5AEF8_D36B_B350_41E1_AE89846FB13B",
  "this.popup_C240F8CF_D36A_BFB0_41D2_86D6D46DB78E"
 ],
 "id": "panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -79.65,
   "yaw": 174.68,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5"
  },
  {
   "backwardYaw": 176.04,
   "yaw": -114.59,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6"
  },
  {
   "backwardYaw": 24.96,
   "yaw": -65.22,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FBF97638_F199_BD93_41D1_05116F346852"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 2.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D84CABAC_D69F_B336_41E6_A15D94113D39",
 "yaw": 88.16,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D84CABAC_D69F_B336_41E6_A15D94113D39_0_0.png",
    "width": 773,
    "class": "ImageResourceLevel",
    "height": 629
   },
   {
    "url": "media/popup_D84CABAC_D69F_B336_41E6_A15D94113D39_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 416
   }
  ]
 },
 "pitch": -3.53,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_t.jpg"
  }
 ],
 "label": "HIMATEKKIM-2021_04",
 "hfov": 360,
 "overlays": [
  "this.overlay_E1B2C083_F1B8_5575_41DB_BF481010F86F",
  "this.overlay_E1DA5E0D_F1B8_ED8D_41DD_E8B9F8B58EFA",
  "this.overlay_591C87A2_57BF_F5CD_41BC_A952A021E0BA",
  "this.panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_tcap0"
 ],
 "id": "panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 88.52,
   "yaw": -12.72,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B"
  },
  {
   "class": "AdjacentPanorama",
   "panorama": "this.panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2"
  },
  {
   "backwardYaw": -148.63,
   "yaw": -92.39,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F8567126_F198_57BF_41D9_3446ACE07DBB"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA3A28_5E43_AE34_41CF_F3E6A80EBFA8",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_camera",
 "initialPosition": {
  "yaw": -59.31,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 39.58
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E6F2A0F_202E_FF83_41BE_EDAD40D5FC75",
 "initialPosition": {
  "yaw": 165.52,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DED1F72_202E_F59D_41A6_03FBACC3CEC1",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DED2F72_202E_F59D_41A0_AECDDC177105",
 "initialPosition": {
  "yaw": 109.38,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.21,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C0509643_D090_64CB_41D3_8C27BAAA6356",
 "yaw": -1.58,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C0509643_D090_64CB_41D3_8C27BAAA6356_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 629
   },
   {
    "url": "media/popup_C0509643_D090_64CB_41D3_8C27BAAA6356_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 413
   }
  ]
 },
 "pitch": -4.75,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAACA24_5E43_AE3C_41CD_6BFBC35762E6",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_camera",
 "initialPosition": {
  "yaw": -85.05,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -0.83
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA1A27_5E43_AE3C_41CF_1B8605D05CF4",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_camera",
 "initialPosition": {
  "yaw": -156.85,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -6.72
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D2DCCF1_202E_F49F_41A9_24215F3C3EAC",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D2DDCF1_202E_F49F_419F_8CD57D4B7B5F",
 "initialPosition": {
  "yaw": 65.41,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C0F2D265_D0F0_3CCF_41C6_694873627FDF",
 "yaw": -10.74,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C0F2D265_D0F0_3CCF_41C6_694873627FDF_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 634
   },
   {
    "url": "media/popup_C0F2D265_D0F0_3CCF_41C6_694873627FDF_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 417
   }
  ]
 },
 "pitch": -3.83,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.56,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_DE463E4C_D090_24DD_41DC_597D5B7E8B5C",
 "yaw": 173.18,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_DE463E4C_D090_24DD_41DC_597D5B7E8B5C_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 638
   },
   {
    "url": "media/popup_DE463E4C_D090_24DD_41DC_597D5B7E8B5C_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 419
   }
  ]
 },
 "pitch": -4.26,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA4A21_5E43_AE34_41CA_A44A1433C20B",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_camera",
 "initialPosition": {
  "yaw": 153.58,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -4.54
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DBD900B_202E_8B83_41B1_29CF861019C6",
 "initialPosition": {
  "yaw": 8.27,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D8A9879_202E_FB8F_419D_8A970B9B09FD",
 "initialPosition": {
  "yaw": -91.48,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA2A28_5E43_AE34_41D5_777C478DAFBE",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_camera",
 "initialPosition": {
  "yaw": 151.89,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 7.75
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "CEF-2017-14",
 "hfov": 360,
 "overlays": [
  "this.overlay_018C2B67_19BC_AF6E_41A3_A95286655F9B",
  "this.overlay_01D8B7A8_19BC_A7E2_41B4_3E0EAA140753",
  "this.overlay_028E7B9A_1A44_6FA6_4191_F2BAECB08386",
  "this.overlay_9E8783EF_8B49_20C6_41D9_1357BC842A11",
  "this.panorama_12A6D467_19BD_996E_419C_647D99E534FD_tcap0",
  "this.popup_C00A57C2_D090_E3C5_41DF_67E1A1B3DCA0"
 ],
 "id": "panorama_12A6D467_19BD_996E_419C_647D99E534FD",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -142.37,
   "yaw": -96.02,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA"
  },
  {
   "backwardYaw": 161.38,
   "yaw": 164.8,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79"
  },
  {
   "backwardYaw": -56.65,
   "yaw": 52.29,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "levels": [
  {
   "url": "media/popup_D95CEA5E_D6EA_BD12_41E0_21FCE4B06EF7_0_0.png",
   "width": 1366,
   "class": "ImageResourceLevel",
   "height": 1021
  },
  {
   "url": "media/popup_D95CEA5E_D6EA_BD12_41E0_21FCE4B06EF7_0_1.png",
   "width": 1024,
   "class": "ImageResourceLevel",
   "height": 765
  },
  {
   "url": "media/popup_D95CEA5E_D6EA_BD12_41E0_21FCE4B06EF7_0_2.png",
   "width": 512,
   "class": "ImageResourceLevel",
   "height": 382
  }
 ],
 "class": "ImageResource",
 "id": "ImageResource_C79169A3_D6EE_7F33_41C5_047D0C373F49"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2FBF349D_202E_F487_419C_8DE1686F7A71",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2FBFC49D_202E_F487_4154_BEB5E742A32A",
 "initialPosition": {
  "yaw": 87.61,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "scaleMode": "fit_inside",
 "width": 1280,
 "label": "video biru kiri",
 "loop": false,
 "id": "video_846EE4CB_9117_507B_41C2_C25091C215F2",
 "class": "Video",
 "thumbnailUrl": "media/video_846EE4CB_9117_507B_41C2_C25091C215F2_t.jpg",
 "height": 720,
 "video": {
  "width": 1280,
  "class": "VideoResource",
  "height": 720,
  "mp4Url": "media/video_846EE4CB_9117_507B_41C2_C25091C215F2.mp4"
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-09",
 "hfov": 360,
 "overlays": [
  "this.overlay_4CA0E0B0_59A2_0BB9_41CE_F7169B1987D6",
  "this.overlay_53FC64B4_5D0E_66DE_41AF_9283CBCA0BF4",
  "this.overlay_4D1FF5F1_5DFA_2659_41C6_D805BCB0DA6C",
  "this.overlay_4C3EC86E_5DFA_EE4A_41C4_B734CD6FEA50",
  "this.overlay_40AA148D_6241_9A0D_41A2_E62171C9BAFD",
  "this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_tcap0"
 ],
 "id": "panorama_52F983E2_59A2_0DDE_41B5_47951B125E55",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 157.5,
   "yaw": 16.07,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F"
  },
  {
   "backwardYaw": -46.16,
   "yaw": -37.32,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827"
  },
  {
   "backwardYaw": 170.5,
   "yaw": 74.41,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA"
  },
  {
   "backwardYaw": -127.13,
   "yaw": 138.53,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69"
  },
  {
   "class": "AdjacentPanorama",
   "panorama": "this.panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.69,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C04CC6D5_D090_25CF_41D6_9EA31BF5C0C0",
 "yaw": 147.17,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C04CC6D5_D090_25CF_41D6_9EA31BF5C0C0_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 619
   },
   {
    "url": "media/popup_C04CC6D5_D090_25CF_41D6_9EA31BF5C0C0_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 407
   }
  ]
 },
 "pitch": -3.97,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E88D2D3_202E_8C83_41A5_5F059D634E15",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E88E2D2_202E_8C9D_4157_54B6AC447634",
 "initialPosition": {
  "yaw": -105.09,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.16,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C3D5AEF8_D36B_B350_41E1_AE89846FB13B",
 "yaw": -0.8,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C3D5AEF8_D36B_B350_41E1_AE89846FB13B_0_0.png",
    "width": 830,
    "class": "ImageResourceLevel",
    "height": 613
   },
   {
    "url": "media/popup_C3D5AEF8_D36B_B350_41E1_AE89846FB13B_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 378
   }
  ]
 },
 "pitch": -9.55,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.2,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C1E128D5_D1B0_6DCF_419F_4F716E54F5F2",
 "yaw": -33.36,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C1E128D5_D1B0_6DCF_419F_4F716E54F5F2_0_0.png",
    "width": 800,
    "class": "ImageResourceLevel",
    "height": 593
   },
   {
    "url": "media/popup_C1E128D5_D1B0_6DCF_419F_4F716E54F5F2_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 379
   }
  ]
 },
 "pitch": -6.49,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.92,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9241176_D696_CF12_41E4_1E0ED2648F92",
 "yaw": 53.18,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9241176_D696_CF12_41E4_1E0ED2648F92_0_0.png",
    "width": 773,
    "class": "ImageResourceLevel",
    "height": 607
   },
   {
    "url": "media/popup_D9241176_D696_CF12_41E4_1E0ED2648F92_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 402
   }
  ]
 },
 "pitch": 1.54,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "CEF-2017-11",
 "hfov": 360,
 "overlays": [
  "this.overlay_0E5C3788_19C4_67A2_4182_278158AF3AF9",
  "this.overlay_0FED0FF2_19C4_A766_41A4_F1F84BC7BEFD",
  "this.overlay_9D6A3547_8B5E_E1C6_41A6_B7B378B1E0D7",
  "this.overlay_9D139DFF_8B47_E0C6_41D8_DFA49AF57ED6",
  "this.popup_9E94850E_8B5E_E146_41CD_D1107F108B7E",
  "this.panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_tcap0",
  "this.popup_C0B430C5_D090_1DCF_41E1_52FC0CEBF52C",
  "this.popup_C1D2BF53_D090_64CB_41C7_ED3885933E5C"
 ],
 "id": "panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 104.78,
   "yaw": 98.38,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1E557619_144B_2D23_41A0_05CAAAB2652E"
  },
  {
   "backwardYaw": -101.33,
   "yaw": -95.52,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 9.03,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D988123E_D695_CD12_41E2_86019132F0C0",
 "yaw": -8.11,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D988123E_D695_CD12_41E2_86019132F0C0_0_0.png",
    "width": 773,
    "class": "ImageResourceLevel",
    "height": 603
   },
   {
    "url": "media/popup_D988123E_D695_CD12_41E2_86019132F0C0_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 399
   }
  ]
 },
 "pitch": 2.25,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E955B04_202E_FD85_419A_0F7494CA77D5",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E950B04_202E_FD85_4187_328A905312C9",
 "initialPosition": {
  "yaw": -20.7,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "items": [
  {
   "camera": "this.panorama_1E904061_1445_25E3_41A2_C3386908E890_camera",
   "media": "this.panorama_1E904061_1445_25E3_41A2_C3386908E890",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 0, 1)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_1E682630_144B_2D61_419D_2C5C83D95A53_camera",
   "media": "this.panorama_1E682630_144B_2D61_419D_2C5C83D95A53",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 1, 2)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_camera",
   "media": "this.panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 2, 3)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_camera",
   "media": "this.panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 3, 4)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_camera",
   "media": "this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 4, 5)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_camera",
   "media": "this.panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 5, 6)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_camera",
   "media": "this.panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 6, 7)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_camera",
   "media": "this.panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 7, 8)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_camera",
   "media": "this.panorama_1E547224_144B_E561_41A3_10C4B18B9EE7",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 8, 9)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_camera",
   "media": "this.panorama_1E557619_144B_2D23_41A0_05CAAAB2652E",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 9, 10)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_camera",
   "media": "this.panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 10, 11)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_camera",
   "media": "this.panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 11, 12)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_camera",
   "media": "this.panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 12, 13)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_12A6D467_19BD_996E_419C_647D99E534FD_camera",
   "media": "this.panorama_12A6D467_19BD_996E_419C_647D99E534FD",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 13, 14)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_camera",
   "media": "this.panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 14, 15)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_camera",
   "media": "this.panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 15, 16)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_camera",
   "media": "this.panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 16, 17)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_camera",
   "media": "this.panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 17, 18)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_camera",
   "media": "this.panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 18, 19)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_camera",
   "media": "this.panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 19, 20)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_FBF97638_F199_BD93_41D1_05116F346852_camera",
   "media": "this.panorama_FBF97638_F199_BD93_41D1_05116F346852",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 20, 21)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_camera",
   "media": "this.panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 21, 22)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_camera",
   "media": "this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 22, 23)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_camera",
   "media": "this.panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 23, 24)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_camera",
   "media": "this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 24, 25)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_camera",
   "media": "this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 25, 26)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_camera",
   "media": "this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 26, 27)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_camera",
   "media": "this.panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 27, 28)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_camera",
   "media": "this.panorama_F8567126_F198_57BF_41D9_3446ACE07DBB",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 28, 29)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_camera",
   "media": "this.panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 29, 30)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_camera",
   "media": "this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 30, 31)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_camera",
   "media": "this.panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 31, 32)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_camera",
   "media": "this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 32, 33)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_camera",
   "media": "this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 33, 34)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_camera",
   "media": "this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 34, 35)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_camera",
   "media": "this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 35, 36)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_camera",
   "media": "this.panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 36, 37)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_camera",
   "media": "this.panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 37, 38)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_camera",
   "media": "this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 38, 39)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_camera",
   "media": "this.panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 39, 40)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_camera",
   "media": "this.panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 40, 41)",
   "player": "this.MainViewerPanoramaPlayer"
  },
  {
   "camera": "this.panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_camera",
   "media": "this.panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41",
   "end": "this.trigger('tourEnded')",
   "class": "PanoramaPlayListItem",
   "begin": "this.setEndToItemIndex(this.mainPlayList, 41, 0)",
   "player": "this.MainViewerPanoramaPlayer"
  }
 ],
 "id": "mainPlayList",
 "class": "PlayList"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.18,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C240F8CF_D36A_BFB0_41D2_86D6D46DB78E",
 "yaw": 85.16,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C240F8CF_D36A_BFB0_41D2_86D6D46DB78E_0_0.png",
    "width": 830,
    "class": "ImageResourceLevel",
    "height": 693
   },
   {
    "url": "media/popup_C240F8CF_D36A_BFB0_41D2_86D6D46DB78E_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 427
   }
  ]
 },
 "pitch": -8.02,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.49,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9F5424F_D696_4D73_41E1_23EEEFCFBBC3",
 "yaw": 38.47,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9F5424F_D696_4D73_41E1_23EEEFCFBBC3_0_1.png",
    "width": 1024,
    "class": "ImageResourceLevel",
    "height": 661
   }
  ]
 },
 "pitch": 3.51,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAAAA26_5E43_AE3C_41A9_CF3A3A248EB9",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_camera",
 "initialPosition": {
  "yaw": -53.52,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 41.64
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.9,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9BEA37E_D6ED_B312_41EA_9E72B449870A",
 "yaw": -2.15,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9BEA37E_D6ED_B312_41EA_9E72B449870A_0_0.png",
    "width": 800,
    "class": "ImageResourceLevel",
    "height": 803
   },
   {
    "url": "media/popup_D9BEA37E_D6ED_B312_41EA_9E72B449870A_0_1.png",
    "width": 510,
    "class": "ImageResourceLevel",
    "height": 512
   }
  ]
 },
 "pitch": 1.95,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D1BE705_202E_F587_41BF_36FE7ECE4606",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D1BF705_202E_F587_41BB_1E3EC306B950",
 "initialPosition": {
  "yaw": 87.86,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 3.38,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C2D349AF_D33A_B1F0_41C4_223F79EA2090",
 "yaw": 22.8,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C2D349AF_D33A_B1F0_41C4_223F79EA2090_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 659
   },
   {
    "url": "media/popup_C2D349AF_D33A_B1F0_41C4_223F79EA2090_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 433
   }
  ]
 },
 "pitch": -5.98,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.18,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C1844106_D190_3C4D_41CD_8AC07C6DAFDA",
 "yaw": -24.55,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C1844106_D190_3C4D_41CD_8AC07C6DAFDA_0_0.png",
    "width": 772,
    "class": "ImageResourceLevel",
    "height": 492
   },
   {
    "url": "media/popup_C1844106_D190_3C4D_41CD_8AC07C6DAFDA_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 326
   }
  ]
 },
 "pitch": -7.98,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_t.jpg"
  }
 ],
 "label": "CEF-2017-09",
 "hfov": 360,
 "overlays": [
  "this.overlay_04FD7878_14AA_D1FB_419E_D8332CB2F5FB",
  "this.overlay_05848035_14AB_D175_4193_D0702F5E6CE3",
  "this.overlay_9EA0F137_8B4F_E145_41DE_84F65FB07BDF",
  "this.overlay_9D1C24FA_8B49_20CF_41E1_30369318529A",
  "this.panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_tcap0",
  "this.popup_DF4D5460_D090_E4C5_41E2_3F0A5DA77A2E",
  "this.popup_C0A367D5_D090_23CF_41DC_40DB848226AD"
 ],
 "id": "panorama_1E547224_144B_E561_41A3_10C4B18B9EE7",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -99.24,
   "yaw": -83.2,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1E557619_144B_2D23_41A0_05CAAAB2652E"
  },
  {
   "backwardYaw": 1,
   "yaw": -174.07,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1E904061_1445_25E3_41A2_C3386908E890"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 3.71,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9B4B83F_D6EB_DD12_41DD_58C38F38A71D",
 "yaw": -79.08,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9B4B83F_D6EB_DD12_41DD_58C38F38A71D_0_1.png",
    "width": 1024,
    "class": "ImageResourceLevel",
    "height": 785
   }
  ]
 },
 "pitch": 3.6,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E050189_202E_8C8F_41BF_B2273A9F81CB",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E052189_202E_8C8F_41BD_4673770D891B",
 "initialPosition": {
  "yaw": -155.04,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "CEF-2017-12",
 "hfov": 360,
 "overlays": [
  "this.overlay_0E301A5B_19C3_A8A6_4177_6C45785E8066",
  "this.overlay_0EC48508_19BC_98A2_4189_A90AA5401E1E",
  "this.overlay_9EAF5AAC_8B49_234A_41D8_F9B8D674A9FE",
  "this.overlay_9D4623A8_8B4B_214A_419C_06F7C8F92D95",
  "this.overlay_9D0C7238_8B4B_234B_41D3_7D305D5802E2",
  "this.popup_9EA01A76_8B49_23C6_4196_E909C8272775",
  "this.panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_tcap0",
  "this.popup_C04186D6_D090_25CD_41E8_5E98D6DE4735",
  "this.popup_C016B8FA_D090_6DC4_41E0_AE5EE75FC8A2",
  "this.popup_C04CC6D5_D090_25CF_41D6_9EA31BF5C0C0"
 ],
 "id": "panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -95.52,
   "yaw": -101.33,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363"
  },
  {
   "backwardYaw": 112.28,
   "yaw": 97.73,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.75,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C20487D9_D33A_9150_41DA_27461F12546D",
 "yaw": 17.22,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C20487D9_D33A_9150_41DA_27461F12546D_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 659
   },
   {
    "url": "media/popup_C20487D9_D33A_9150_41DA_27461F12546D_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 433
   }
  ]
 },
 "pitch": -5.56,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.2,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D86A6B0C_D695_BCF6_41AF_4489832BFFCA",
 "yaw": -18.74,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D86A6B0C_D695_BCF6_41AF_4489832BFFCA_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 609
   },
   {
    "url": "media/popup_D86A6B0C_D695_BCF6_41AF_4489832BFFCA_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 400
   }
  ]
 },
 "pitch": -5.93,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_t.jpg"
  }
 ],
 "label": "CEF-2019_05",
 "hfov": 360,
 "overlays": [
  "this.overlay_E54DEA2A_F714_81C6_41CB_2131EF6E3A78",
  "this.overlay_E54D1BDB_F715_8646_41D4_4759E563D260",
  "this.overlay_4B5A5CE9_5E40_AA37_41CB_013473BEFAE7",
  "this.panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_tcap0"
 ],
 "id": "panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "class": "AdjacentPanorama",
   "panorama": "this.panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032"
  },
  {
   "backwardYaw": 165.54,
   "yaw": -169.99,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7"
  },
  {
   "backwardYaw": 166.46,
   "yaw": 0.32,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FBF97638_F199_BD93_41D1_05116F346852"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DB8FEB8_202E_F48D_41AD_702F814D5430",
 "initialPosition": {
  "yaw": -9.5,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "items": [
  {
   "media": "this.video_8E674D6B_8349_89AB_41C6_ADBABEABEE04",
   "start": "this.MainViewerVideoPlayer.set('displayPlaybackBar', true); this.changeBackgroundWhilePlay(this.playList_2C80FE60_202E_97BD_41B0_4CBB1EB434E1, 0, '#000000'); this.pauseGlobalAudiosWhilePlayItem(this.playList_2C80FE60_202E_97BD_41B0_4CBB1EB434E1, 0)",
   "class": "VideoPlayListItem",
   "begin": "this.fixTogglePlayPauseButton(this.MainViewerVideoPlayer)",
   "player": "this.MainViewerVideoPlayer"
  }
 ],
 "id": "playList_2C80FE60_202E_97BD_41B0_4CBB1EB434E1",
 "class": "PlayList"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 2.37,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D8F7DE7C_D69F_B516_41D3_A8E99ADF690E",
 "yaw": 88.09,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D8F7DE7C_D69F_B516_41D3_A8E99ADF690E_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 604
   },
   {
    "url": "media/popup_D8F7DE7C_D69F_B516_41D3_A8E99ADF690E_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 397
   }
  ]
 },
 "pitch": 12.34,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D3066C2_202E_F4FD_41C0_0ED059FF1BB8",
 "initialPosition": {
  "yaw": -141.67,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_t.jpg"
  }
 ],
 "label": "HIMATEKKIM-2020_06",
 "hfov": 360,
 "overlays": [
  "this.overlay_FBFB3499_EE67_B8EC_41E0_3E5778D5A02F",
  "this.overlay_F854764F_EE65_5864_41DB_842409337F28",
  "this.overlay_F43B7707_EE63_F9E3_41DB_310A361F3687",
  "this.overlay_7C6C696C_6247_AA0C_41A2_932162ACA7CE",
  "this.overlay_9C8A7D29_8BCB_E14A_41D0_65E22692B97F",
  "this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_tcap0",
  "this.popup_C2F4F500_D33A_76B0_41E8_691F0E88A4C6"
 ],
 "id": "panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 159.3,
   "yaw": 124.99,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5"
  },
  {
   "backwardYaw": 173.72,
   "yaw": 163.9,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8"
  },
  {
   "backwardYaw": -79.6,
   "yaw": 60.14,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6"
  },
  {
   "backwardYaw": -154.47,
   "yaw": -143.83,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2FE2B55C_202E_F585_41BD_ACA2FFFDBA83",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2FE3055C_202E_F585_4173_126357BDD5EE",
 "initialPosition": {
  "yaw": 167.28,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2F91C4FF_202E_F483_4173_446FCBEE6161",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F91A4FF_202E_F483_41B6_70B986DD4F09",
 "initialPosition": {
  "yaw": 80.76,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DE140C2_202E_8CFD_41B4_8EF08CDCF120",
 "initialPosition": {
  "yaw": -175.9,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "HIMATEKKIM-2021-10",
 "hfov": 360,
 "overlays": [
  "this.overlay_4C0C6DAB_59A6_15AE_41D6_4F81B2B8400E",
  "this.overlay_4C1EE4F9_59A6_0BAA_41CF_BED388B1C850",
  "this.overlay_4FBAD4FE_59A6_0BA6_41C5_99B99029F74C",
  "this.overlay_4C9934DF_5DFA_6649_41CE_3A52EA046C83",
  "this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_tcap0",
  "this.overlay_A1834C92_B3C8_CF17_41D0_C553E416C836"
 ],
 "id": "panorama_52F9B72F_59A2_16A6_41D1_A287990FC827",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -131.13,
   "yaw": -117.76,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F"
  },
  {
   "backwardYaw": -37.32,
   "yaw": -46.16,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55"
  },
  {
   "backwardYaw": -179.94,
   "yaw": 4.1,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207"
  },
  {
   "backwardYaw": 38.33,
   "yaw": 54.69,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5"
  },
  {
   "backwardYaw": 143.18,
   "yaw": 122.69,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E6601D4_202E_8C85_41A4_D612BFEA677D",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E6631D4_202E_8C85_4195_90E4E054BC9D",
 "initialPosition": {
  "yaw": -67.72,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2F4FA482_202E_8B7D_41AA_B67EE80E4BE2",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F506482_202E_8B7D_41B5_51A1DB0852A2",
 "initialPosition": {
  "yaw": -34.87,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2EC8BB9F_202E_FC83_41BC_9B3EB18D2FD8",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2EC94B9F_202E_FC83_41BA_D92D65ED1362",
 "initialPosition": {
  "yaw": -13.54,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D06B740_202E_F5FD_41B2_AA14C04F4442",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D074740_202E_F5FD_41AA_608D56687353",
 "initialPosition": {
  "yaw": 18.78,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "viewerArea": "this.MainViewer",
 "class": "VideoPlayer",
 "id": "MainViewerVideoPlayer",
 "displayPlaybackBar": true
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAAEA24_5E43_AE3C_41C5_1D1D10DE1C32",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_camera",
 "initialPosition": {
  "yaw": 62.34,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -2.48
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA8A25_5E43_AE3C_41C8_ED6026A639F2",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_12A6D467_19BD_996E_419C_647D99E534FD_camera",
 "initialPosition": {
  "yaw": 72.12,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 4.44
 }
},
{
 "levels": [
  {
   "url": "media/popup_D94CDD6E_D6EA_5735_4193_08B6A0C3A054_0_0.png",
   "width": 1366,
   "class": "ImageResourceLevel",
   "height": 1048
  },
  {
   "url": "media/popup_D94CDD6E_D6EA_5735_4193_08B6A0C3A054_0_1.png",
   "width": 1024,
   "class": "ImageResourceLevel",
   "height": 785
  },
  {
   "url": "media/popup_D94CDD6E_D6EA_5735_4193_08B6A0C3A054_0_2.png",
   "width": 512,
   "class": "ImageResourceLevel",
   "height": 392
  }
 ],
 "class": "ImageResource",
 "id": "ImageResource_C797D9A5_D6EE_7F37_41E1_D944487B2AC8"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E5611FF_202E_8C83_41BE_C7C66F6285DD",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E5621FF_202E_8C83_41BF_5F324FFA5F8A",
 "initialPosition": {
  "yaw": -79.4,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_t.jpg"
  }
 ],
 "label": "HIMATEKKIM-2020_05",
 "hfov": 360,
 "overlays": [
  "this.overlay_FA62AAD6_ED9D_C865_41D5_0A2197638795",
  "this.overlay_F8F99502_EE62_B9DC_41EC_65CC37919811",
  "this.overlay_FADA04E9_EE63_782C_41DC_C64045C8870C",
  "this.overlay_FFD95735_F188_DB9D_41EB_69CDE30F447A",
  "this.overlay_9D68CD4D_8BC9_E1CA_41D7_366DB8BE8BEE",
  "this.overlay_9C1721C0_8BCA_E13A_41E0_F92A1E322A21",
  "this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_tcap0",
  "this.popup_C211C828_D32A_9EF0_41DD_EE8B9AE2E7CA",
  "this.popup_C2E6C360_D32A_7170_41E4_40E2CB04F4FF"
 ],
 "id": "panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -114.59,
   "yaw": 176.04,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C"
  },
  {
   "backwardYaw": -158.87,
   "yaw": -135.36,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5"
  },
  {
   "backwardYaw": 60.14,
   "yaw": -79.6,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD"
  },
  {
   "backwardYaw": 110.12,
   "yaw": 118.61,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_FBF97638_F199_BD93_41D1_05116F346852"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_t.jpg"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.2,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C0167782_D0F0_2445_41B6_21C0F38920E4",
 "yaw": -11.82,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C0167782_D0F0_2445_41B6_21C0F38920E4_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 630
   },
   {
    "url": "media/popup_C0167782_D0F0_2445_41B6_21C0F38920E4_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 414
   }
  ]
 },
 "pitch": -5.75,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D7D175E_202E_F585_4194_4797F75F92E4",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D7DC75E_202E_F585_41A5_5EBF1751349F",
 "initialPosition": {
  "yaw": -93.35,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D78EE02_202E_F77D_41B8_98F3C16ED613",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D78FE02_202E_F77D_41B6_442CEE5F8AFB",
 "initialPosition": {
  "yaw": 96.8,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2EF6BB45_202E_FD87_4191_9CE3E3F76560",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2EF75B45_202E_FD87_418D_C107C5AECD91",
 "initialPosition": {
  "yaw": 100.4,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA1A22_5E43_AE34_41D3_C394A36E3772",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_camera",
 "initialPosition": {
  "yaw": -68.12,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -4.54
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.16,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C1B7142E_D190_245D_41C8_6E04A1650973",
 "yaw": -12.69,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C1B7142E_D190_245D_41C8_6E04A1650973_0_0.png",
    "width": 800,
    "class": "ImageResourceLevel",
    "height": 595
   },
   {
    "url": "media/popup_C1B7142E_D190_245D_41C8_6E04A1650973_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 380
   }
  ]
 },
 "pitch": -9.28,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D3776A1_202E_F4BF_41BC_ED73B0474CBA",
 "initialPosition": {
  "yaw": 0.06,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 3.67,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D855325C_D69A_4D15_41D9_6EB8DDE333F7",
 "yaw": -61.69,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D855325C_D69A_4D15_41D9_6EB8DDE333F7_0_0.png",
    "width": 864,
    "class": "ImageResourceLevel",
    "height": 650
   },
   {
    "url": "media/popup_D855325C_D69A_4D15_41D9_6EB8DDE333F7_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 385
   }
  ]
 },
 "pitch": 1.38,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D680794_202E_F485_41AE_93BCA1EE502B",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D682794_202E_F485_417C_40268133FE2D",
 "initialPosition": {
  "yaw": 107.98,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.19,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C0A660F5_D190_1DCF_41D9_393B74A28BBE",
 "yaw": 47.59,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C0A660F5_D190_1DCF_41D9_393B74A28BBE_0_0.png",
    "width": 800,
    "class": "ImageResourceLevel",
    "height": 534
   },
   {
    "url": "media/popup_C0A660F5_D190_1DCF_41D9_393B74A28BBE_0_1.png",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 341
   }
  ]
 },
 "pitch": -6.95,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAB1A27_5E43_AE3C_41C9_556ACA6BF0FD",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_camera",
 "initialPosition": {
  "yaw": 73.36,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -8.37
 }
},
{
 "levels": [
  {
   "url": "media/popup_D9839D16_D697_F712_41E0_0E1BF02FF1D2_0_0.png",
   "width": 1280,
   "class": "ImageResourceLevel",
   "height": 898
  },
  {
   "url": "media/popup_D9839D16_D697_F712_41E0_0E1BF02FF1D2_0_1.png",
   "width": 1024,
   "class": "ImageResourceLevel",
   "height": 718
  },
  {
   "url": "media/popup_D9839D16_D697_F712_41E0_0E1BF02FF1D2_0_2.png",
   "width": 512,
   "class": "ImageResourceLevel",
   "height": 359
  }
 ],
 "class": "ImageResource",
 "id": "ImageResource_C7842991_D6EE_7FEF_41E8_F72C0B29EBCF"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DADB02C_202E_8B85_41A7_A8543EF13B30",
 "initialPosition": {
  "yaw": -87.87,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "timeToIdle": 5000,
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DE3B8D8_202E_FC8D_41B0_20BA7A048AFD",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DE458D8_202E_FC8D_41B7_E11AD1F4B602",
 "idleSequence": "this.sequence_2DE3B8D8_202E_FC8D_41B0_20BA7A048AFD",
 "initialPosition": {
  "yaw": 89.29,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E51CA6B_202E_FF83_4176_7273ED7D8093",
 "initialPosition": {
  "yaw": -41.47,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 2.33,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D818D7DC_D69E_D316_41DE_15AF326B24EA",
 "yaw": -34.81,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D818D7DC_D69E_D316_41DE_15AF326B24EA_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 604
   },
   {
    "url": "media/popup_D818D7DC_D69E_D316_41DE_15AF326B24EA_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 397
   }
  ]
 },
 "pitch": 15.82,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "scaleMode": "fit_inside",
 "width": 1280,
 "label": "Latar Loading CEVO",
 "loop": false,
 "id": "video_8E674D6B_8349_89AB_41C6_ADBABEABEE04",
 "class": "Video",
 "thumbnailUrl": "media/video_8E674D6B_8349_89AB_41C6_ADBABEABEE04_t.jpg",
 "height": 720,
 "video": {
  "width": 1280,
  "class": "VideoResource",
  "mp4Url": "media/video_8E674D6B_8349_89AB_41C6_ADBABEABEE04.mp4",
  "height": 720
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2E47F22A_202E_8F8D_41BC_65BEBFE6FA7C",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2E47922A_202E_8F8D_41B0_AC82D4D5A41D",
 "initialPosition": {
  "yaw": 36.17,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.81,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D93636C6_D696_B572_41D3_A9389E95088F",
 "yaw": 4.74,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D93636C6_D696_B572_41D3_A9389E95088F_0_1.png",
    "width": 1024,
    "class": "ImageResourceLevel",
    "height": 774
   }
  ]
 },
 "pitch": -4.56,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D271D10_202E_F59D_41BD_3B51C2F70354",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D272D0F_202E_F582_41B7_C1C11588845A",
 "initialPosition": {
  "yaw": 21.13,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D22ED30_202E_F59D_41AC_35FA0ED9F082",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D22FD30_202E_F59D_4156_929C7A8A90E0",
 "initialPosition": {
  "yaw": -119.86,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D05CF6E_202E_9585_41B8_CCD39CD64077",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D05EF6E_202E_9585_4198_BDB360E6A3BF",
 "initialPosition": {
  "yaw": -72.26,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DFE7893_202E_FC83_41A3_07A532281055",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DFE1893_202E_FC83_41B7_353A9C7D02F4",
 "initialPosition": {
  "yaw": 31.37,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_t.jpg"
  }
 ],
 "label": "CEF-2017-02",
 "hfov": 360,
 "overlays": [
  "this.overlay_04EA43E1_1477_B70D_41B3_E27355AD81D7",
  "this.overlay_04D5D9B0_147A_D30B_41A2_C74AD47B9FFE",
  "this.overlay_9EB060EB_8B79_20CD_41DA_4CA250A14181",
  "this.overlay_9E9D83D0_8B7F_60DB_41DF_339C27E56FC7",
  "this.overlay_9E9E7111_8B7F_215A_41D9_C5511487C09D",
  "this.panorama_1E682630_144B_2D61_419D_2C5C83D95A53_tcap0",
  "this.popup_C0F2D265_D0F0_3CCF_41C6_694873627FDF",
  "this.popup_C08E0602_D0F1_E445_41CD_EE518397B837",
  "this.popup_C0E728B2_D0F0_2C45_4182_1AF8EB7707A3"
 ],
 "id": "panorama_1E682630_144B_2D61_419D_2C5C83D95A53",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": -105.07,
   "yaw": -92.33,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB"
  },
  {
   "backwardYaw": -90.71,
   "yaw": 86.65,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1E904061_1445_25E3_41A2_C3386908E890"
  }
 ],
 "vfov": 180,
 "hfovMax": 908,
 "partial": false,
 "thumbnailUrl": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_t.jpg"
},
{
 "timeToIdle": 5000,
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAACA29_5E43_AE34_41D3_116EDC40EBFA",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_1E904061_1445_25E3_41A2_C3386908E890_camera",
 "idleSequence": "this.sequence_4CAACA29_5E43_AE34_41D3_116EDC40EBFA",
 "initialPosition": {
  "yaw": 4.75,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -1.34
 }
},
{
 "class": "MediaAudio",
 "data": {
  "label": "Long Background Music for CEVO 2021"
 },
 "id": "audio_2C1BAB8B_2031_FC83_41B7_BE0E20D0E485",
 "audio": {
  "mp3Url": "media/audio_2C1BAB8B_2031_FC83_41B7_BE0E20D0E485.mp3",
  "class": "AudioResource",
  "oggUrl": "media/audio_2C1BAB8B_2031_FC83_41B7_BE0E20D0E485.ogg"
 },
 "autoplay": true
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2EB7D256_202E_8F85_4191_6100BC0A2F51",
 "initialPosition": {
  "yaw": -111.28,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F6E9449_202E_8B8F_41A4_6262F23DFD5B",
 "initialPosition": {
  "yaw": -85.48,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA9A24_5E43_AE3C_41B3_9BA79087BD3A",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_camera",
 "initialPosition": {
  "yaw": -109.32,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": -3.41
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAADA28_5E43_AE34_41D5_3D7E903262FF",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_1E682630_144B_2D61_419D_2C5C83D95A53_camera",
 "initialPosition": {
  "yaw": 7.23,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 51.56
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.19,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C2E6C360_D32A_7170_41E4_40E2CB04F4FF",
 "yaw": 22.76,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C2E6C360_D32A_7170_41E4_40E2CB04F4FF_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 569
   },
   {
    "url": "media/popup_C2E6C360_D32A_7170_41E4_40E2CB04F4FF_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 374
   }
  ]
 },
 "pitch": -6.92,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2FF3253F_202E_F583_41BB_AC02F61C4D0B",
 "initialPosition": {
  "yaw": 67.7,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2F3ACBBD_202E_FC87_417E_9BCAAFA51B0B",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2F3AFBBD_202E_FC87_4186_B6B6699F638C",
 "initialPosition": {
  "yaw": -51.69,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_4CAA5A21_5E43_AE34_41D6_96B953AEBD00",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_camera",
 "initialPosition": {
  "yaw": 166.38,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 2.48
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_camera",
 "initialPosition": {
  "yaw": 0,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2EA8D27E_202E_8F85_41BB_0338B508BA50",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2EA8827E_202E_8F85_41B9_ED5D2012169B",
 "initialPosition": {
  "yaw": 88.42,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_t.jpg"
  }
 ],
 "hfovMin": "120%",
 "label": "CEF-2017-13",
 "hfov": 360,
 "overlays": [
  "this.overlay_0EBEE7B6_19BD_A7EE_41AC_72782126629B",
  "this.overlay_0E680B3B_19BC_68E6_41A7_A89523767798",
  "this.overlay_0E108A28_19BC_68E2_41A9_C6F19ECF9F1E",
  "this.overlay_9D7CBD00_8B49_213A_41A5_954D0C7DF85C",
  "this.overlay_9D688E19_8B49_234A_41C2_A6C2B0B94507",
  "this.popup_9D71ACC6_8B49_20C6_41BA_E7BE57F8E17B",
  "this.panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_tcap0",
  "this.popup_C08E4886_D090_6C4D_41E3_287AABE86716",
  "this.popup_C0140ACB_D090_EDDB_41CD_7B0EFA884291"
 ],
 "id": "panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 97.73,
   "yaw": 112.28,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6"
  },
  {
   "backwardYaw": -96.02,
   "yaw": -142.37,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A6D467_19BD_996E_419C_647D99E534FD"
  },
  {
   "backwardYaw": 122.24,
   "yaw": -116.37,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79"
  }
 ],
 "vfov": 180,
 "hfovMax": 120,
 "partial": false,
 "thumbnailUrl": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_t.jpg"
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2D4587E9_202E_F48F_41B3_071C4DE3D6F3",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2D4597E8_202E_F48D_41AA_36DA50B76686",
 "initialPosition": {
  "yaw": -101.77,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2ECBC381_202E_8D7F_41BA_50ABE91FE09C",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2ECBD381_202E_8D7F_41A6_1136CA82F253",
 "initialPosition": {
  "yaw": -97.16,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": "this.sequence_2DC2F109_202E_8D8F_41AB_B80B8114D250",
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DC28108_202E_8D8D_41B0_7B84D8104902",
 "initialPosition": {
  "yaw": -15.2,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.21,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C1D56887_D090_6C4B_41D3_51AD4D4BD31B",
 "yaw": 2.24,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C1D56887_D090_6C4B_41D3_51AD4D4BD31B_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 537
   },
   {
    "url": "media/popup_C1D56887_D090_6C4B_41D3_51AD4D4BD31B_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 353
   }
  ]
 },
 "pitch": -4.36,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.68,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_D9839D16_D697_F712_41E0_0E1BF02FF1D2",
 "yaw": -40.93,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_D9839D16_D697_F712_41E0_0E1BF02FF1D2_0_1.png",
    "width": 1024,
    "class": "ImageResourceLevel",
    "height": 718
   }
  ]
 },
 "pitch": 3.03,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2DF100A4_202E_8C85_41B4_33501D290BD1",
 "initialPosition": {
  "yaw": -9.41,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "class": "PanoramaCamera",
 "initialSequence": {
  "movements": [
   {
    "easing": "cubic_in",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   },
   {
    "easing": "linear",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 323
   },
   {
    "easing": "cubic_out",
    "yawSpeed": 7.96,
    "class": "DistancePanoramaCameraMovement",
    "yawDelta": 18.5
   }
  ],
  "restartMovementOnUserInteraction": false,
  "class": "PanoramaCameraSequence"
 },
 "manualRotationSpeed": 2000,
 "automaticZoomSpeed": 10,
 "id": "camera_2CD715EA_202E_F48D_4199_BA93A7105F41",
 "initialPosition": {
  "yaw": 81.53,
  "hfov": 90,
  "class": "PanoramaCameraPosition",
  "pitch": 0
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_C0B430C5_D090_1DCF_41E1_52FC0CEBF52C",
 "yaw": 32.67,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_C0B430C5_D090_1DCF_41E1_52FC0CEBF52C_0_0.png",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 635
   },
   {
    "url": "media/popup_C0B430C5_D090_1DCF_41E1_52FC0CEBF52C_0_1.png",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 417
   }
  ]
 },
 "pitch": -4.36,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "frames": [
  {
   "front": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/f/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/f/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/f/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/f/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "class": "CubicPanoramaFrame",
   "top": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/u/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/u/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/u/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/u/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "right": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/r/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/r/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/r/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/r/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "back": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/b/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/b/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/b/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/b/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "bottom": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/d/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/d/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/d/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/d/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "left": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/l/0/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 6,
      "colCount": 6,
      "width": 3072,
      "tags": "ondemand",
      "height": 3072
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/l/1/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 3,
      "colCount": 3,
      "width": 1536,
      "tags": "ondemand",
      "height": 1536
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/l/2/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 2,
      "colCount": 2,
      "width": 1024,
      "tags": "ondemand",
      "height": 1024
     },
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0/l/3/{row}_{column}.jpg",
      "class": "TiledImageResourceLevel",
      "rowCount": 1,
      "colCount": 1,
      "width": 512,
      "tags": [
       "ondemand",
       "preload"
      ],
      "height": 512
     }
    ]
   },
   "thumbnailUrl": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_t.jpg"
  }
 ],
 "label": "CEF-2017-01",
 "hfov": 360,
 "overlays": [
  "this.overlay_19B5EC4D_1469_511A_4199_5558E241D262",
  "this.overlay_1AB6070F_1469_5F16_41A2_EBB33DDC532F",
  "this.overlay_8414678A_8B59_214E_41E0_40F42D29F76B",
  "this.panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0",
  "this.popup_DE463E4C_D090_24DD_41DC_597D5B7E8B5C"
 ],
 "id": "panorama_1E904061_1445_25E3_41A2_C3386908E890",
 "class": "Panorama",
 "pitch": 0,
 "adjacentPanoramas": [
  {
   "backwardYaw": 86.65,
   "yaw": -90.71,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1E682630_144B_2D61_419D_2C5C83D95A53"
  },
  {
   "backwardYaw": -174.07,
   "yaw": 1,
   "class": "AdjacentPanorama",
   "distance": 1,
   "panorama": "this.panorama_1E547224_144B_E561_41A3_10C4B18B9EE7"
  }
 ],
 "vfov": 180,
 "hfovMax": 908,
 "partial": false,
 "thumbnailUrl": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_t.jpg"
},
{
 "levels": [
  {
   "url": "media/popup_C6F8A0C6_D6EA_4D72_41A3_F57B7E281005_0_0.png",
   "width": 600,
   "class": "ImageResourceLevel",
   "height": 1045
  },
  {
   "url": "media/popup_C6F8A0C6_D6EA_4D72_41A3_F57B7E281005_0_1.png",
   "width": 587,
   "class": "ImageResourceLevel",
   "height": 1024
  },
  {
   "url": "media/popup_C6F8A0C6_D6EA_4D72_41A3_F57B7E281005_0_2.png",
   "width": 293,
   "class": "ImageResourceLevel",
   "height": 512
  }
 ],
 "class": "ImageResource",
 "id": "ImageResource_C79A39A9_D6EE_7F3F_41E2_8EC6DCB271DE"
},
{
 "items": [
  {
   "media": "this.video_846EE4CB_9117_507B_41C2_C25091C215F2",
   "start": "this.MainViewerVideoPlayer.set('displayPlaybackBar', true); this.changeBackgroundWhilePlay(this.playList_2C80CE60_202E_97BD_41A4_91F54620F1A4, 0, '#000000'); this.pauseGlobalAudiosWhilePlayItem(this.playList_2C80CE60_202E_97BD_41A4_91F54620F1A4, 0)",
   "class": "VideoPlayListItem",
   "begin": "this.fixTogglePlayPauseButton(this.MainViewerVideoPlayer)",
   "player": "this.MainViewerVideoPlayer"
  }
 ],
 "id": "playList_2C80CE60_202E_97BD_41A4_91F54620F1A4",
 "class": "PlayList"
},
{
 "toolTipDisplayTime": 1000,
 "playbackBarHeadShadowHorizontalLength": 0,
 "borderRadius": 0,
 "id": "MainViewer",
 "left": 0,
 "playbackBarHeadBackgroundColorRatios": [
  0,
  1
 ],
 "paddingBottom": 0,
 "playbackBarHeadShadowBlurRadius": 3,
 "playbackBarLeft": 0,
 "width": "100%",
 "toolTipBorderRadius": 3,
 "progressBackgroundColorDirection": "vertical",
 "progressBarBorderColor": "#000000",
 "playbackBarHeadHeight": 15,
 "progressBorderColor": "#000000",
 "borderSize": 0,
 "playbackBarBottom": 5,
 "toolTipShadowSpread": 0,
 "playbackBarHeadOpacity": 1,
 "progressBarBackgroundColorRatios": [
  0
 ],
 "minHeight": 50,
 "progressBarBackgroundColor": [
  "#3399FF"
 ],
 "toolTipBorderColor": "#767676",
 "paddingRight": 0,
 "progressBackgroundColor": [
  "#FFFFFF"
 ],
 "playbackBarProgressBackgroundColorDirection": "vertical",
 "displayTooltipInTouchScreens": true,
 "minWidth": 100,
 "toolTipOpacity": 1,
 "playbackBarHeadWidth": 6,
 "toolTipShadowBlurRadius": 3,
 "toolTipFontSize": "12px",
 "playbackBarBackgroundColorDirection": "vertical",
 "toolTipTextShadowColor": "#000000",
 "playbackBarBackgroundColor": [
  "#FFFFFF"
 ],
 "playbackBarHeadShadowVerticalLength": 0,
 "playbackBarRight": 0,
 "playbackBarHeight": 10,
 "toolTipTextShadowBlurRadius": 3,
 "height": "100%",
 "toolTipFontWeight": "normal",
 "playbackBarProgressBorderSize": 0,
 "toolTipPaddingBottom": 4,
 "playbackBarProgressBorderRadius": 0,
 "toolTipShadowColor": "#333333",
 "progressBarBorderSize": 0,
 "transitionDuration": 500,
 "progressBarBorderRadius": 0,
 "paddingTop": 0,
 "shadow": false,
 "playbackBarBorderRadius": 0,
 "playbackBarHeadBorderRadius": 0,
 "playbackBarProgressBorderColor": "#000000",
 "toolTipShadowOpacity": 1,
 "toolTipFontStyle": "normal",
 "progressLeft": 0,
 "playbackBarHeadBorderColor": "#000000",
 "playbackBarHeadBorderSize": 0,
 "playbackBarProgressOpacity": 1,
 "playbackBarBorderSize": 0,
 "propagateClick": false,
 "toolTipTextShadowOpacity": 0,
 "toolTipFontFamily": "Arial",
 "vrPointerSelectionColor": "#FF6600",
 "playbackBarBackgroundOpacity": 1,
 "playbackBarHeadBackgroundColor": [
  "#111111",
  "#666666"
 ],
 "toolTipShadowHorizontalLength": 0,
 "playbackBarHeadShadowColor": "#000000",
 "vrPointerSelectionTime": 1500,
 "progressRight": 0,
 "toolTipShadowVerticalLength": 0,
 "firstTransitionDuration": 0,
 "progressOpacity": 1,
 "top": 0,
 "progressBarBackgroundColorDirection": "vertical",
 "playbackBarHeadShadow": true,
 "progressBottom": 0,
 "toolTipBackgroundColor": "#F6F6F6",
 "toolTipFontColor": "#606060",
 "progressHeight": 10,
 "playbackBarHeadBackgroundColorDirection": "vertical",
 "progressBackgroundOpacity": 1,
 "transitionMode": "blending",
 "playbackBarProgressBackgroundColor": [
  "#3399FF"
 ],
 "playbackBarOpacity": 1,
 "class": "ViewerArea",
 "vrPointerColor": "#FFFFFF",
 "progressBarOpacity": 1,
 "playbackBarHeadShadowOpacity": 0.7,
 "paddingLeft": 0,
 "playbackBarBorderColor": "#FFFFFF",
 "progressBorderSize": 0,
 "data": {
  "name": "Main Viewer"
 },
 "toolTipBorderSize": 1,
 "toolTipPaddingTop": 4,
 "toolTipPaddingLeft": 6,
 "progressBorderRadius": 0,
 "toolTipPaddingRight": 6,
 "progressBackgroundColorRatios": [
  0
 ],
 "playbackBarProgressBackgroundColorRatios": [
  0
 ]
},
{
 "cursor": "hand",
 "toolTipShadowOpacity": 1,
 "borderRadius": 0,
 "toolTipFontStyle": "normal",
 "maxWidth": 56,
 "id": "IconButton_E3C1AA4E_EDAE_C864_41E4_5820E86940F3",
 "toolTipPaddingRight": 6,
 "maxHeight": 128,
 "paddingBottom": 0,
 "right": "1.64%",
 "propagateClick": false,
 "width": 40.03,
 "toolTipBorderRadius": 3,
 "toolTipShadowHorizontalLength": 0,
 "toolTipFontFamily": "Arial",
 "toolTip": "Fullscreen",
 "borderSize": 0,
 "verticalAlign": "middle",
 "toolTipShadowSpread": 0,
 "toolTipShadowVerticalLength": 0,
 "toolTipBorderColor": "#767676",
 "minHeight": 1,
 "toolTipTextShadowOpacity": 0,
 "paddingRight": 0,
 "height": 34.04,
 "horizontalAlign": "center",
 "minWidth": 1,
 "mode": "toggle",
 "toolTipBackgroundColor": "#F6F6F6",
 "toolTipFontColor": "#606060",
 "toolTipShadowBlurRadius": 3,
 "toolTipFontSize": "12px",
 "bottom": "2.35%",
 "toolTipTextShadowColor": "#000000",
 "toolTipOpacity": 1,
 "class": "IconButton",
 "toolTipTextShadowBlurRadius": 3,
 "toolTipPaddingBottom": 4,
 "toolTipFontWeight": "normal",
 "toolTipShadowColor": "#333333",
 "paddingLeft": 0,
 "paddingTop": 0,
 "data": {
  "name": "IconButton1493"
 },
 "toolTipBorderSize": 1,
 "shadow": false,
 "toolTipPaddingTop": 4,
 "transparencyActive": true,
 "toolTipPaddingLeft": 6,
 "iconURL": "skin/IconButton_E3C1AA4E_EDAE_C864_41E4_5820E86940F3.png",
 "backgroundOpacity": 0,
 "toolTipDisplayTime": 1000
},
{
 "cursor": "hand",
 "borderRadius": 0,
 "maxWidth": 56,
 "id": "IconButton_E3081138_EDA3_D82C_41E8_15DF452055EA",
 "left": "1.36%",
 "maxHeight": 56,
 "paddingBottom": 0,
 "propagateClick": false,
 "width": 50.05,
 "verticalAlign": "middle",
 "borderSize": 0,
 "minHeight": 1,
 "paddingRight": 0,
 "height": 54,
 "horizontalAlign": "center",
 "minWidth": 1,
 "mode": "push",
 "bottom": "0.94%",
 "class": "IconButton",
 "paddingLeft": 0,
 "paddingTop": 0,
 "data": {
  "name": "IconButton14318"
 },
 "shadow": false,
 "transparencyActive": false,
 "iconURL": "skin/IconButton_E3081138_EDA3_D82C_41E8_15DF452055EA.png",
 "backgroundOpacity": 0
},
{
 "borderRadius": 0,
 "id": "veilPopupPanorama",
 "left": 0,
 "paddingBottom": 0,
 "right": 0,
 "propagateClick": false,
 "backgroundColorRatios": [
  0
 ],
 "borderSize": 0,
 "minHeight": 0,
 "showEffect": {
  "duration": 350,
  "easing": "cubic_in_out",
  "class": "FadeInEffect"
 },
 "top": 0,
 "paddingRight": 0,
 "backgroundColorDirection": "vertical",
 "minWidth": 0,
 "bottom": 0,
 "backgroundColor": [
  "#000000"
 ],
 "class": "UIComponent",
 "paddingLeft": 0,
 "paddingTop": 0,
 "data": {
  "name": "UIComponent1683"
 },
 "shadow": false,
 "visible": false,
 "backgroundOpacity": 0.55
},
{
 "borderRadius": 0,
 "id": "zoomImagePopupPanorama",
 "left": 0,
 "paddingBottom": 0,
 "right": 0,
 "propagateClick": false,
 "backgroundColorRatios": [],
 "borderSize": 0,
 "minHeight": 0,
 "top": 0,
 "paddingRight": 0,
 "backgroundColorDirection": "vertical",
 "minWidth": 0,
 "bottom": 0,
 "backgroundColor": [],
 "class": "ZoomImage",
 "paddingLeft": 0,
 "paddingTop": 0,
 "data": {
  "name": "ZoomImage1684"
 },
 "shadow": false,
 "visible": false,
 "scaleMode": "custom",
 "backgroundOpacity": 1
},
{
 "cursor": "hand",
 "layout": "horizontal",
 "shadowSpread": 1,
 "borderRadius": 0,
 "pressedIconColor": "#888888",
 "id": "closeButtonPopupPanorama",
 "iconHeight": 20,
 "paddingBottom": 5,
 "iconWidth": 20,
 "shadowColor": "#000000",
 "fontFamily": "Arial",
 "backgroundOpacity": 0.3,
 "right": 10,
 "propagateClick": false,
 "iconBeforeLabel": true,
 "backgroundColorRatios": [
  0,
  0.1,
  1
 ],
 "borderSize": 0,
 "verticalAlign": "middle",
 "iconColor": "#000000",
 "rollOverIconColor": "#666666",
 "minHeight": 0,
 "showEffect": {
  "duration": 350,
  "easing": "cubic_in_out",
  "class": "FadeInEffect"
 },
 "top": 10,
 "borderColor": "#000000",
 "paddingRight": 5,
 "backgroundColor": [
  "#DDDDDD",
  "#EEEEEE",
  "#FFFFFF"
 ],
 "backgroundColorDirection": "vertical",
 "horizontalAlign": "center",
 "minWidth": 0,
 "mode": "push",
 "fontSize": "1.29vmin",
 "label": "",
 "fontColor": "#FFFFFF",
 "class": "CloseButton",
 "gap": 5,
 "paddingLeft": 5,
 "shadowBlurRadius": 6,
 "paddingTop": 5,
 "fontStyle": "normal",
 "shadow": false,
 "visible": false,
 "iconLineWidth": 5,
 "fontWeight": "normal",
 "textDecoration": "none",
 "data": {
  "name": "CloseButton1685"
 }
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D31ACCD_202E_F487_4192_32EB345EEAEC",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DA3D83D_202E_FB87_4181_3C0977D6DD98",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D0C8DA0_202E_F4BD_41AA_C082B8E4E93E",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D3C2C8E_202E_F485_41B2_E75D2B20E741",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69, this.camera_2DF100A4_202E_8C85_41B4_33501D290BD1); this.mainPlayList.set('selectedIndex', 38)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.86,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0_HS_0_0.png",
      "width": 138,
      "class": "ImageResourceLevel",
      "height": 108
     }
    ]
   },
   "pitch": -15.52,
   "yaw": -6,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.86,
   "yaw": -6,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0_HS_0_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -15.52
  }
 ],
 "id": "overlay_4CFEC05B_5E5F_9A15_41D4_AABD7709AD2B",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41, this.camera_2DFFF08B_202E_8C83_41BF_4372E1BE076D); this.mainPlayList.set('selectedIndex', 41)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.56,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0_HS_1_0.png",
      "width": 130,
      "class": "ImageResourceLevel",
      "height": 113
     }
    ]
   },
   "pitch": -14.96,
   "yaw": 123.58,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.56,
   "yaw": 123.58,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0_HS_1_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -14.96
  }
 ],
 "id": "overlay_4B8534A4_5E40_BA33_41CC_09B777108D8E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.mainPlayList.set('selectedIndex', 26)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.55,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0_HS_2_0.png",
      "width": 81,
      "class": "ImageResourceLevel",
      "height": 75
     }
    ]
   },
   "pitch": -9.98,
   "yaw": 170.72,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.55,
   "yaw": 170.72,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_0_HS_2_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -9.98
  }
 ],
 "id": "overlay_4BF797D2_5E41_A614_4198_B17B659E7505",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E1D698D_202E_FC87_41BF_ED22121F60A2",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123, this.camera_2FE3055C_202E_F585_4173_126357BDD5EE); this.mainPlayList.set('selectedIndex', 29)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.92,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_1_HS_0_0.png",
      "width": 90,
      "class": "ImageResourceLevel",
      "height": 73
     }
    ]
   },
   "pitch": -10.67,
   "yaw": 88.52,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.92,
   "yaw": 88.52,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_1_HS_0_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -10.67
  }
 ],
 "id": "overlay_5859F4EF_57BE_8B53_41CE_078228F76AAE",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F8567126_F198_57BF_41D9_3446ACE07DBB, this.camera_2CD22610_202E_F79D_41B1_C4B98F2CFF27); this.mainPlayList.set('selectedIndex', 28)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.11,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_1_HS_1_0.png",
      "width": 95,
      "class": "ImageResourceLevel",
      "height": 64
     }
    ]
   },
   "pitch": -12.47,
   "yaw": 124.93,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.11,
   "yaw": 124.93,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_1_HS_1_0_0_map.gif",
      "width": 23,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -12.47
  }
 ],
 "id": "overlay_479BFAC5_57BA_9F57_41D3_AA6B52386CF6",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5, this.camera_2CD715EA_202E_F48D_4199_BA93A7105F41); this.mainPlayList.set('selectedIndex', 31)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.31,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_1_HS_2_0.png",
      "width": 126,
      "class": "ImageResourceLevel",
      "height": 105
     }
    ]
   },
   "pitch": -17.59,
   "yaw": 21.61,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.31,
   "yaw": 21.61,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_1_HS_2_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -17.59
  }
 ],
 "id": "overlay_466D2340_57A6_8D4D_41B5_01AC7C8BD8C2",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236, this.camera_2FF3253F_202E_F583_41BB_AC02F61C4D0B); this.mainPlayList.set('selectedIndex', 32)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.96,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_1_HS_3_0.png",
      "width": 91,
      "class": "ImageResourceLevel",
      "height": 85
     }
    ]
   },
   "pitch": -10.59,
   "yaw": -14.48,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.96,
   "yaw": -14.48,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_1_HS_3_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -10.59
  }
 ],
 "id": "overlay_491075A9_59A2_15AA_41C7_AAD8598F074E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D8030674_D69A_D516_41DE_4A7F21C38C32, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.52,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0_HS_4_0.png",
      "width": 194,
      "class": "ImageResourceLevel",
      "height": 185
     }
    ]
   },
   "pitch": 3.1,
   "yaw": -119.33,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.52,
   "yaw": -119.33,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 3.1
  }
 ],
 "id": "overlay_93D4F7E4_8B59_60FA_41D8_6992FAD86FFD",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D855325C_D69A_4D15_41D9_6EB8DDE333F7, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.67,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0_HS_5_0.png",
      "width": 83,
      "class": "ImageResourceLevel",
      "height": 83
     }
    ]
   },
   "pitch": 1.38,
   "yaw": -61.69,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.67,
   "yaw": -61.69,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 1.38
  }
 ],
 "id": "overlay_9C181819_8B5F_2F4A_41B3_68E1713731E2",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D87F1C35_D69A_5516_4171_4F32029468FE, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.41,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0_HS_6_0.png",
      "width": 106,
      "class": "ImageResourceLevel",
      "height": 101
     }
    ]
   },
   "pitch": 18.94,
   "yaw": 165.91,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.41,
   "yaw": 165.91,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0_HS_6_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 18.94
  }
 ],
 "id": "overlay_93E3C811_8B5F_2F5D_41DF_FF2BDF1AE246",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D86D6F24_D69A_B335_41E9_37CBA15E74B1, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0_HS_7_0.png",
      "width": 96,
      "class": "ImageResourceLevel",
      "height": 96
     }
    ]
   },
   "pitch": -4.12,
   "yaw": 165.88,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.22,
   "yaw": 165.88,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_0_HS_7_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.12
  }
 ],
 "id": "overlay_9C1C2DB9_8B5E_E14A_41B3_510BB5771ECE",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D11ED7E_202E_F585_41B1_23D1DC8C606F",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D597E5C_202E_F785_41B7_C5F8FB7C0BC8",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD, this.camera_2E47922A_202E_8F8D_41B0_AC82D4D5A41D); this.mainPlayList.set('selectedIndex', 25)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.17,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0_HS_0_0.png",
      "width": 172,
      "class": "ImageResourceLevel",
      "height": 149
     }
    ]
   },
   "pitch": -18.72,
   "yaw": -154.47,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.17,
   "yaw": -154.47,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0_HS_0_0_0_map.gif",
      "width": 86,
      "class": "ImageResourceLevel",
      "height": 74
     }
    ]
   },
   "pitch": -18.72
  }
 ],
 "id": "overlay_FFDE3923_F188_D7B5_41E3_8CC9D718719B",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8, this.camera_2E5621FF_202E_8C83_41BF_5F324FFA5F8A); this.mainPlayList.set('selectedIndex', 23)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.07,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0_HS_1_0.png",
      "width": 172,
      "class": "ImageResourceLevel",
      "height": 149
     }
    ]
   },
   "pitch": -20.92,
   "yaw": -79.79,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.07,
   "yaw": -79.79,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0_HS_1_0_0_map.gif",
      "width": 86,
      "class": "ImageResourceLevel",
      "height": 74
     }
    ]
   },
   "pitch": -20.92
  }
 ],
 "id": "overlay_FF3BF211_F188_5595_41CD_15A47E1C9EEF",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2, this.camera_2EA8827E_202E_8F85_41B9_ED5D2012169B); this.mainPlayList.set('selectedIndex', 27)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.65,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0_HS_2_0.png",
      "width": 131,
      "class": "ImageResourceLevel",
      "height": 114
     }
    ]
   },
   "pitch": -11.06,
   "yaw": -11.55,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.65,
   "yaw": -11.55,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0_HS_2_0_0_map.gif",
      "width": 65,
      "class": "ImageResourceLevel",
      "height": 57
     }
    ]
   },
   "pitch": -11.06
  }
 ],
 "id": "overlay_FFC3F61F_F188_FD8D_41E7_DE8A3CF60B75",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41, this.camera_2EB7D256_202E_8F85_4191_6100BC0A2F51); this.mainPlayList.set('selectedIndex', 41)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.43,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0_HS_3_0.png",
      "width": 104,
      "class": "ImageResourceLevel",
      "height": 82
     }
    ]
   },
   "pitch": -15.13,
   "yaw": 128.31,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.43,
   "yaw": 128.31,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_0_HS_3_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -15.13
  }
 ],
 "id": "overlay_7E3C4354_6241_9E1C_41CA_A3E49E2C3020",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D06EDC2_202E_F4FD_4191_18180326D9B2",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D126723_202E_F583_4183_A440FC141373",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D959F18_202E_F58D_41AE_EBFE0AE31E98",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2EEA632E_202E_8D85_41B3_E3CF1BE264EA",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2, this.camera_2FA0F4DE_202E_F485_41B0_332E270CE42D); this.mainPlayList.set('selectedIndex', 27)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.77,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_0_0.png",
      "width": 137,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -17.19,
   "yaw": -70.62,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_E1BEDE09_F1B8_AD75_41DE_C3FCE23F6C26",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123, this.camera_2FBFC49D_202E_F487_4154_BEB5E742A32A); this.mainPlayList.set('selectedIndex', 29)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.88,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_1_0.png",
      "width": 170,
      "class": "ImageResourceLevel",
      "height": 144
     }
    ]
   },
   "pitch": -23.56,
   "yaw": -148.63,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_FE4992AB_F1B8_5AB5_41BF_FC0288BD3A40",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B, this.camera_2FB0B4BE_202E_F485_41B3_248F6E7ED5B7); this.mainPlayList.set('selectedIndex', 30)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.92,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_2_0.png",
      "width": 90,
      "class": "ImageResourceLevel",
      "height": 77
     }
    ]
   },
   "pitch": -11.23,
   "yaw": 142.78,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.92,
   "yaw": 142.78,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_2_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -11.23
  }
 ],
 "id": "overlay_59E84FC8_57BD_B55C_41D1_EAF91840FC22",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D818D7DC_D69E_D316_41DE_15AF326B24EA, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 2.33,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_3_0.png",
      "width": 55,
      "class": "ImageResourceLevel",
      "height": 53
     }
    ]
   },
   "pitch": 15.82,
   "yaw": -34.81,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 2.33,
   "yaw": -34.81,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 15.82
  }
 ],
 "id": "overlay_9CEE4666_8B59_E3C6_41DB_99E53A819B6B",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9B7F444_D69E_5575_41D4_2CF331886357, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 2.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_4_0.png",
      "width": 50,
      "class": "ImageResourceLevel",
      "height": 53
     }
    ]
   },
   "pitch": -4.39,
   "yaw": -34.39,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 2.22,
   "yaw": -34.39,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.39
  }
 ],
 "id": "overlay_9CEE7666_8B59_E3C6_41AA_9BFD1783E297",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9B5EF1C_D69D_D316_41A5_2E8737A8F7BD, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.23,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_5_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": 2.02,
   "yaw": 26.52,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.23,
   "yaw": 26.52,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 2.02
  }
 ],
 "id": "overlay_9C27997A_8B5B_21CF_41C7_E2437113E264",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9C29F8E_D69A_53F5_41E6_1BFB17B3B974, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.88,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_6_0.png",
      "width": 88,
      "class": "ImageResourceLevel",
      "height": 91
     }
    ]
   },
   "pitch": 1.13,
   "yaw": 84.86,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.88,
   "yaw": 84.86,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_0_HS_6_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 1.13
  }
 ],
 "id": "overlay_9C06FA82_8B5B_E33E_41D8_6D391711E126",
 "data": {
  "label": "Image"
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 2.33,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_9CD55630_8B59_E35A_41DA_381ACC9AD802",
 "yaw": -34.81,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_9CD55630_8B59_E35A_41DA_381ACC9AD802_0_0.jpg",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 437
   },
   {
    "url": "media/popup_9CD55630_8B59_E35A_41DA_381ACC9AD802_0_1.jpg",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 287
   }
  ]
 },
 "pitch": 15.82,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 2.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_9CD40631_8B59_E35A_41D2_6FC05520124A",
 "yaw": -34.39,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_9CD40631_8B59_E35A_41D2_6FC05520124A_0_0.jpg",
    "width": 773,
    "class": "ImageResourceLevel",
    "height": 580
   },
   {
    "url": "media/popup_9CD40631_8B59_E35A_41D2_6FC05520124A_0_1.jpg",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 384
   }
  ]
 },
 "pitch": -4.39,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_F8567126_F198_57BF_41D9_3446ACE07DBB_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAAFA24_5E43_AE3C_41A5_053B7BCF6120",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2F0BDC14_202E_FB85_41AC_27D6548B78E7",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAADA23_5E43_AE34_41D5_4299AB8793BA",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAB6A26_5E43_AE3C_41B3_910691F38ECB",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E851B22_202E_FDBD_41B3_FA2403C72956",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2EDB0358_202E_8D8D_41B6_12F04D887ED1",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236, this.camera_2DBD900B_202E_8B83_41B1_29CF861019C6); this.mainPlayList.set('selectedIndex', 32)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.41,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_1_HS_0_0.png",
      "width": 130,
      "class": "ImageResourceLevel",
      "height": 113
     }
    ]
   },
   "pitch": -19.89,
   "yaw": -21.71,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.41,
   "yaw": -21.71,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_1_HS_0_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -19.89
  }
 ],
 "id": "overlay_573452C6_59A2_0FE6_41A6_649E548C964F",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B, this.camera_2D4C5FEE_202E_9485_41B5_C63BAFD776E7); this.mainPlayList.set('selectedIndex', 30)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.07,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_1_HS_1_0.png",
      "width": 121,
      "class": "ImageResourceLevel",
      "height": 104
     }
    ]
   },
   "pitch": -18.01,
   "yaw": -98.47,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.07,
   "yaw": -98.47,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_1_HS_1_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -18.01
  }
 ],
 "id": "overlay_497CB1EB_59A2_0DAE_41C5_584A2066203F",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827, this.camera_2D5C7FCE_202E_9485_41BD_59DBEBBEDD20); this.mainPlayList.set('selectedIndex', 35)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.87,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_1_HS_2_0.png",
      "width": 215,
      "class": "ImageResourceLevel",
      "height": 164
     }
    ]
   },
   "pitch": -20.72,
   "yaw": 38.33,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.87,
   "yaw": 38.33,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_1_HS_2_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -20.72
  }
 ],
 "id": "overlay_4C64AC76_5DFE_265B_41D2_0FA4E7D18335",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DFB3F54_202E_F585_4181_2A709B763D2A",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D17FD64_202E_F585_41BA_13E13A7D1F11",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D700E1F_202E_F783_4187_4D18A6B9171B",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E98D2A5_202E_8C87_41BF_76267A909538",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B, this.camera_2E6F2A0F_202E_FF83_41BE_EDAD40D5FC75); this.mainPlayList.set('selectedIndex', 30)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.3,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_1_HS_0_0.png",
      "width": 101,
      "class": "ImageResourceLevel",
      "height": 93
     }
    ]
   },
   "pitch": -16.06,
   "yaw": -112.3,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.3,
   "yaw": -112.3,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_1_HS_0_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -16.06
  }
 ],
 "id": "overlay_48AEB2F3_59AE_0FBE_41CE_47A8C4D0D010",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5, this.camera_2E617A3E_202E_FF85_4157_67D71C2638B7); this.mainPlayList.set('selectedIndex', 31)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 9.62,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0_HS_1_0.png",
      "width": 233,
      "class": "ImageResourceLevel",
      "height": 139
     }
    ]
   },
   "pitch": -20.4,
   "yaw": -171.73,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 9.62,
   "yaw": -171.73,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0_HS_1_0_0_map.gif",
      "width": 26,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -20.4
  }
 ],
 "id": "overlay_48FD6727_59A2_F6A7_41C3_6E4B7181A4E7",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F, this.camera_2E0F59B8_202E_FC8D_4183_5779D1FDFA96); this.mainPlayList.set('selectedIndex', 33)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.63,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_1_HS_2_0.png",
      "width": 108,
      "class": "ImageResourceLevel",
      "height": 87
     }
    ]
   },
   "pitch": -13.21,
   "yaw": 94.52,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.63,
   "yaw": 94.52,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_1_HS_2_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -13.21
  }
 ],
 "id": "overlay_4B3FFFD5_59A6_15FB_41BA_FE555D5B70DC",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827, this.camera_2E7F79E5_202E_FC87_414F_3284EC3993D8); this.mainPlayList.set('selectedIndex', 35)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.62,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_1_HS_3_0.png",
      "width": 157,
      "class": "ImageResourceLevel",
      "height": 132
     }
    ]
   },
   "pitch": -16.72,
   "yaw": 143.18,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.62,
   "yaw": 143.18,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_1_HS_3_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -16.72
  }
 ],
 "id": "overlay_4DE5D56C_59A2_0AA9_41D1_F577ECFE3524",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D841E06C_D69A_4D36_41E8_4A20160FD43A, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.77,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0_HS_4_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": 24.12,
   "yaw": -18.47,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.77,
   "yaw": -18.47,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 24.12
  }
 ],
 "id": "overlay_935CCCD4_8B5B_20DB_41D9_07D79E8210E2",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D86A6B0C_D695_BCF6_41AF_4489832BFFCA, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.2,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0_HS_5_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -5.93,
   "yaw": -18.74,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.2,
   "yaw": -18.74,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -5.93
  }
 ],
 "id": "overlay_9C230519_8B5B_E14D_41D8_B68FEF5FDC50",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9F5424F_D696_4D73_41E1_23EEEFCFBBC3, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, this.ImageResource_C786F98D_D6EE_7FF7_41DB_BFC4FC805B35, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.49,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0_HS_6_0.png",
      "width": 102,
      "class": "ImageResourceLevel",
      "height": 104
     }
    ]
   },
   "pitch": 3.51,
   "yaw": 38.47,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.49,
   "yaw": 38.47,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_0_HS_6_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 3.51
  }
 ],
 "id": "overlay_93CCE893_8B5B_2F5E_41D0_72C74C77A73A",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA7A21_5E43_AE34_41A6_E83039CB0202",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D89CF36_202E_F585_41AA_5B9D7CB3BE73",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C, this.camera_2F0BEC14_202E_FB85_418F_9E380A86177C); this.mainPlayList.set('selectedIndex', 21)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.81,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_0_0.png",
      "width": 170,
      "class": "ImageResourceLevel",
      "height": 143
     }
    ]
   },
   "pitch": -25.07,
   "yaw": -79.65,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.81,
   "yaw": -79.65,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_0_0_0_map.gif",
      "width": 85,
      "class": "ImageResourceLevel",
      "height": 71
     }
    ]
   },
   "pitch": -25.07
  }
 ],
 "id": "overlay_FC43B25A_EDE2_D86C_41E0_A2D926013203",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8, this.camera_2D3CCC8E_202E_F485_419D_EBDDA2B56298); this.mainPlayList.set('selectedIndex', 23)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.99,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_1_0.png",
      "width": 227,
      "class": "ImageResourceLevel",
      "height": 158
     }
    ]
   },
   "pitch": -25.95,
   "yaw": 82.84,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.99,
   "yaw": 82.84,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_1_0_0_map.gif",
      "width": 113,
      "class": "ImageResourceLevel",
      "height": 79
     }
    ]
   },
   "pitch": -25.95
  }
 ],
 "id": "overlay_FF6AAEFF_EDFD_4824_41C6_2F88D8EDF626",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6, this.camera_2D378CAC_202E_F485_41AF_2DFEF11780D3); this.mainPlayList.set('selectedIndex', 24)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.14,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_3_0.png",
      "width": 196,
      "class": "ImageResourceLevel",
      "height": 135
     }
    ]
   },
   "pitch": -19.98,
   "yaw": -158.87,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.14,
   "yaw": -158.87,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_3_0_0_map.gif",
      "width": 98,
      "class": "ImageResourceLevel",
      "height": 67
     }
    ]
   },
   "pitch": -19.98
  }
 ],
 "id": "overlay_FE412BB3_EDA5_C83C_41ED_1BDEB7D858B2",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD, this.camera_2D31BCCD_202E_F487_41BA_AEAD03F59EB4); this.mainPlayList.set('selectedIndex', 25)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.45,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_4_0.png",
      "width": 156,
      "class": "ImageResourceLevel",
      "height": 175
     }
    ]
   },
   "pitch": -20.46,
   "yaw": 159.3,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.45,
   "yaw": 159.3,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_4_0_0_map.gif",
      "width": 78,
      "class": "ImageResourceLevel",
      "height": 87
     }
    ]
   },
   "pitch": -20.46
  }
 ],
 "id": "overlay_F4CB4C26_EDA3_C824_41EC_1CE162EA19B3",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C2E8FF66_D325_F170_41D0_9E7FB18C203C, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.79,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_5_0.png",
      "width": 109,
      "class": "ImageResourceLevel",
      "height": 105
     }
    ]
   },
   "pitch": -6.27,
   "yaw": -22.88,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.79,
   "yaw": -22.88,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -6.27
  }
 ],
 "id": "overlay_93CF424D_8BF9_63CA_41D4_C75017D5278A",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C2D349AF_D33A_B1F0_41C4_223F79EA2090, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.38,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_6_0.png",
      "width": 77,
      "class": "ImageResourceLevel",
      "height": 69
     }
    ]
   },
   "pitch": -5.98,
   "yaw": 22.8,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.38,
   "yaw": 22.8,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_0_HS_6_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -5.98
  }
 ],
 "id": "overlay_9C1C6401_8BF9_273D_4192_5BAF90BDBFB1",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1E547224_144B_E561_41A3_10C4B18B9EE7, this.camera_2D78FE02_202E_F77D_41B6_442CEE5F8AFB); this.mainPlayList.set('selectedIndex', 8)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.32,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_1_HS_0_0.png",
      "width": 131,
      "class": "ImageResourceLevel",
      "height": 113
     }
    ]
   },
   "pitch": -22.55,
   "yaw": -99.24,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.32,
   "yaw": -99.24,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_1_HS_0_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -22.55
  }
 ],
 "id": "overlay_05358C10_14AA_F10B_41B0_7C9EAD488BA0",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363, this.camera_2D018DE2_202E_F4BD_418A_F62D327F9837); this.mainPlayList.set('selectedIndex', 10)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.09,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0_HS_1_0.png",
      "width": 193,
      "class": "ImageResourceLevel",
      "height": 196
     }
    ]
   },
   "pitch": -33.67,
   "yaw": 104.78,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.09,
   "yaw": 104.78,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0_HS_1_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -33.67
  }
 ],
 "id": "overlay_0FE1A958_19C4_68A2_41B4_DC984F94F0DE",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C1D56887_D090_6C4B_41D3_51AD4D4BD31B, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.21,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0_HS_2_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -4.36,
   "yaw": 2.24,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.21,
   "yaw": 2.24,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0_HS_2_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.36
  }
 ],
 "id": "overlay_9EF7E55D_8B4A_E1CA_41C3_0DE6CE32B50E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C0284C66_D090_24CD_41E1_6F013884E173, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0_HS_3_0.png",
      "width": 96,
      "class": "ImageResourceLevel",
      "height": 98
     }
    ]
   },
   "pitch": -3.93,
   "yaw": 153.79,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.22,
   "yaw": 153.79,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -3.93
  }
 ],
 "id": "overlay_9D270BC6_8B49_20C6_41E1_3A28C4DD465A",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C083DD05_D090_244F_41D1_26233EF31ADC, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.14,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0_HS_4_0.png",
      "width": 94,
      "class": "ImageResourceLevel",
      "height": 94
     }
    ]
   },
   "pitch": -2.87,
   "yaw": -153.4,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.14,
   "yaw": -153.4,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -2.87
  }
 ],
 "id": "overlay_9EC34F13_8B49_215D_41B0_CFA3001D7705",
 "data": {
  "label": "Image"
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.14,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_9EFE0EC8_8B49_20CB_41D4_DE46E9A63D0F",
 "yaw": -153.4,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_9EFE0EC8_8B49_20CB_41D4_DE46E9A63D0F_0_0.jpg",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 519
   },
   {
    "url": "media/popup_9EFE0EC8_8B49_20CB_41D4_DE46E9A63D0F_0_1.jpg",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 341
   }
  ]
 },
 "pitch": -2.87,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_1E557619_144B_2D23_41A0_05CAAAB2652E_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D722777_202E_F583_41B1_D3A492389710",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827, this.camera_2DE140C2_202E_8CFD_41B4_8EF08CDCF120); this.mainPlayList.set('selectedIndex', 35)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.3,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_1_HS_0_0.png",
      "width": 150,
      "class": "ImageResourceLevel",
      "height": 133
     }
    ]
   },
   "pitch": -17.62,
   "yaw": -179.94,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.3,
   "yaw": -179.94,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_1_HS_0_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -17.62
  }
 ],
 "id": "overlay_FBC468E4_F6E1_3BD2_41BB_6C5E0BB53CB4",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_FBC7B8E5_F6E1_3BD2_41E8_3E8E17D323D3",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.openLink('https://www.menti.com/nk238uuhac', '_blank')",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.3,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_1_HS_1_0.png",
      "width": 166,
      "class": "ImageResourceLevel",
      "height": 136
     }
    ]
   },
   "pitch": -0.15,
   "yaw": -0.12,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.3,
   "yaw": -0.12,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207_1_HS_1_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -0.15
  }
 ],
 "id": "overlay_F9D7CC49_F6E3_7AD5_4170_769834599FD7",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C, this.camera_2EFA7302_202E_8D7D_41A0_E2CA4503490B); this.mainPlayList.set('selectedIndex', 21)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.29,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0_HS_1_0.png",
      "width": 152,
      "class": "ImageResourceLevel",
      "height": 132
     }
    ]
   },
   "pitch": -19.9,
   "yaw": 24.96,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.29,
   "yaw": 24.96,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0_HS_1_0_0_map.gif",
      "width": 76,
      "class": "ImageResourceLevel",
      "height": 66
     }
    ]
   },
   "pitch": -19.9
  }
 ],
 "id": "overlay_FD59483E_F198_558F_41ED_A4B5406F8AAB",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6, this.camera_2EEA732D_202E_8D86_4189_1FF71A1F8C15); this.mainPlayList.set('selectedIndex', 24)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.35,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0_HS_2_0.png",
      "width": 155,
      "class": "ImageResourceLevel",
      "height": 131
     }
    ]
   },
   "pitch": -21.36,
   "yaw": 110.12,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.35,
   "yaw": 110.12,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0_HS_2_0_0_map.gif",
      "width": 77,
      "class": "ImageResourceLevel",
      "height": 65
     }
    ]
   },
   "pitch": -21.36
  }
 ],
 "id": "overlay_FC8ACE29_F198_6DB5_41EC_90F6FD0C1041",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move to Previous Room",
   "click": "this.startPanoramaWithCamera(this.panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21, this.camera_2EDB2358_202E_8D8D_4180_F850C47CFA0B); this.mainPlayList.set('selectedIndex', 19)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_43540203_5DC0_99F5_41B0_5EC22EFB4D81",
   "yaw": 166.46,
   "pitch": -10.5,
   "distance": 100,
   "hfov": 10.2
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 10.2,
   "yaw": 166.46,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -10.5
  }
 ],
 "id": "overlay_4B42DF27_5E40_663D_41B1_DBB0B989DD4B",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_FBF97638_F199_BD93_41D1_05116F346852_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D37FCAC_202E_F485_41B7_B3FBCBC5B764",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D5C07B1_202E_F49F_41B9_C2309F4E3817",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4ACAD994_5E43_AA1C_41C0_A9288AF316BE",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": -18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": -323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": -18.5
  }
 ],
 "id": "sequence_2F818521_202E_F5BF_41AE_60DA4B86C052",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032, this.camera_2D0CADA0_202E_F4BD_41A7_C0BD3723ED18); this.mainPlayList.set('selectedIndex', 16)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.44,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_1_HS_1_0.png",
      "width": 168,
      "class": "ImageResourceLevel",
      "height": 138
     }
    ]
   },
   "pitch": -29.56,
   "yaw": -155.3,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_F3383C22_F8EB_7130_41AB_7A61D5CAE880",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1, this.camera_2D179D64_202E_F585_419F_990ACF192814); this.mainPlayList.set('selectedIndex', 17)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.79,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_1_HS_2_0.png",
      "width": 143,
      "class": "ImageResourceLevel",
      "height": 121
     }
    ]
   },
   "pitch": -23.62,
   "yaw": 153.67,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_F338CC22_F8EB_7130_41E3_85D055426C7F",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move to Previous Room",
   "click": "this.startPanoramaWithCamera(this.panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79, this.camera_2D118D7E_202E_F585_41AB_C18F26075CF8); this.mainPlayList.set('selectedIndex', 14)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_04E992A5_1A4C_B9E2_419E_541A536AF6B5",
   "yaw": 81.38,
   "pitch": -10.9,
   "distance": 100,
   "hfov": 6.52
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.52,
   "yaw": 81.38,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -10.9
  }
 ],
 "id": "overlay_02F894F3_1A44_B966_419B_F945B01FE2A4",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C1990537_D1B0_244B_41DA_745F287DC290, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.89,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0_HS_4_0.png",
      "width": 111,
      "class": "ImageResourceLevel",
      "height": 111
     }
    ]
   },
   "pitch": -5.66,
   "yaw": -72.62,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.89,
   "yaw": -72.62,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -5.66
  }
 ],
 "id": "overlay_9D5ABC89_8BCB_674A_41C3_20678BEE6D24",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7, this.camera_2D1BF705_202E_F587_41BB_1E3EC306B950); this.mainPlayList.set('selectedIndex', 18)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_1_HS_0_0.png",
      "width": 171,
      "class": "ImageResourceLevel",
      "height": 125
     }
    ]
   },
   "pitch": -21.87,
   "yaw": 87.47,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_F45B765E_F829_1110_41D0_E5E403080B4B",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032, this.camera_2D074740_202E_F5FD_41AA_608D56687353); this.mainPlayList.set('selectedIndex', 16)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 10.32,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_1_HS_1_0.png",
      "width": 285,
      "class": "ImageResourceLevel",
      "height": 261
     }
    ]
   },
   "pitch": -34.72,
   "yaw": -153.05,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_F45B365E_F829_1110_41C3_4BE2B594F3AE",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5, this.camera_2D127723_202E_F583_41B2_5C08A3ED0D88); this.mainPlayList.set('selectedIndex', 15)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.85,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_1_HS_2_0.png",
      "width": 141,
      "class": "ImageResourceLevel",
      "height": 115
     }
    ]
   },
   "pitch": -20.19,
   "yaw": -96.2,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_F45B265E_F829_1110_41D7_9C9ADCFC9330",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C1844106_D190_3C4D_41CD_8AC07C6DAFDA, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.18,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0_HS_3_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -7.98,
   "yaw": -24.55,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.18,
   "yaw": -24.55,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -7.98
  }
 ],
 "id": "overlay_9D7B10D9_8BCF_E0CA_41CA_964BEC8F974E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C0A660F5_D190_1DCF_41D9_393B74A28BBE, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.19,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0_HS_4_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -6.95,
   "yaw": 47.59,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.19,
   "yaw": 47.59,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -6.95
  }
 ],
 "id": "overlay_9C772E2A_8BCF_634E_41BA_B5888B6FA322",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E2CA95B_202E_FD83_41B2_A28373FDF40E",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA3A22_5E43_AE34_41BA_21FD59F9B59E",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64, this.camera_2DFBCF54_202E_F585_41A4_FE2FC92AC848); this.mainPlayList.set('selectedIndex', 26)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.75,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_0_0.png",
      "width": 110,
      "class": "ImageResourceLevel",
      "height": 104
     }
    ]
   },
   "pitch": -10.87,
   "yaw": -91.58,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.75,
   "yaw": -91.58,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_0_0_0_map.gif",
      "width": 55,
      "class": "ImageResourceLevel",
      "height": 52
     }
    ]
   },
   "pitch": -10.87
  }
 ],
 "id": "overlay_E186DF6D_F189_AB8D_41D8_BF555EE000C1",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F8567126_F198_57BF_41D9_3446ACE07DBB, this.camera_2DED2F72_202E_F59D_41A0_AECDDC177105); this.mainPlayList.set('selectedIndex', 28)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.56,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_1_0.png",
      "width": 130,
      "class": "ImageResourceLevel",
      "height": 124
     }
    ]
   },
   "pitch": -14.95,
   "yaw": 118.82,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.56,
   "yaw": 118.82,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_1_0_0_map.gif",
      "width": 65,
      "class": "ImageResourceLevel",
      "height": 62
     }
    ]
   },
   "pitch": -14.95
  }
 ],
 "id": "overlay_FE3A09E5_F188_D6BD_41CD_F3F63F54B663",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D80973A1_D69E_732E_41B4_2CA10CF93671, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.39,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_2_0.png",
      "width": 100,
      "class": "ImageResourceLevel",
      "height": 95
     }
    ]
   },
   "pitch": 1.47,
   "yaw": -15.31,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.39,
   "yaw": -15.31,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_2_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 1.47
  }
 ],
 "id": "overlay_9C76EE43_8B4F_233E_41D4_9D7941BAD051",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D85D2714_D69E_F316_41BE_E8BEAD5AF76C, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.77,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_3_0.png",
      "width": 108,
      "class": "ImageResourceLevel",
      "height": 108
     }
    ]
   },
   "pitch": 1.52,
   "yaw": 43.9,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.77,
   "yaw": 43.9,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 1.52
  }
 ],
 "id": "overlay_9C6FF902_8B4B_E13E_41C1_4220D5719201",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D8F7DE7C_D69F_B516_41D3_A8E99ADF690E, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 2.37,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_4_0.png",
      "width": 55,
      "class": "ImageResourceLevel",
      "height": 53
     }
    ]
   },
   "pitch": 12.34,
   "yaw": 88.09,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 2.37,
   "yaw": 88.09,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 12.34
  }
 ],
 "id": "overlay_9C96ED0F_8B49_6146_4196_AA28599C014E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D84CABAC_D69F_B336_41E6_A15D94113D39, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 2.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_5_0.png",
      "width": 50,
      "class": "ImageResourceLevel",
      "height": 53
     }
    ]
   },
   "pitch": -3.53,
   "yaw": 88.16,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 2.22,
   "yaw": 88.16,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -3.53
  }
 ],
 "id": "overlay_9C7E6549_8B47_E1CD_41B6_842A37145C06",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_F85585EC_F198_5E8C_41C0_D2627D38A4A2_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2ED8FB7E_202E_FD85_41BA_575BD6AF8D37",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAABA25_5E43_AE3C_41A9_2422193DDF3D",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2F2CC3CB_202E_8C83_41B2_5E15BA9622F8",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D98E85E_202E_FB85_41A8_193DF40BC1ED",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D656E3D_202E_F787_41AE_4E1FDC28BDAF",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA, this.camera_2F2A3BDA_202E_FC8D_41A9_590740CCFECF); this.mainPlayList.set('selectedIndex', 39)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.94,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_1_HS_0_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 107
     }
    ]
   },
   "pitch": -18.29,
   "yaw": -160.28,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.94,
   "yaw": -160.28,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_1_HS_0_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 15
     }
    ]
   },
   "pitch": -18.29
  }
 ],
 "id": "overlay_45155A8F_5DC0_EE0C_41D7_955C01DFD58C",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372, this.camera_2F1BDBF7_202E_FC83_41BD_E264D2FCE079); this.mainPlayList.set('selectedIndex', 40)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.35,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_1_HS_1_0.png",
      "width": 125,
      "class": "ImageResourceLevel",
      "height": 101
     }
    ]
   },
   "pitch": -14.91,
   "yaw": 161.31,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.35,
   "yaw": 161.31,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_1_HS_1_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -14.91
  }
 ],
 "id": "overlay_468C0E0E_5DC3_A60C_41B3_CDF9643188CD",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64, this.camera_2F3AFBBD_202E_FC87_4186_B6B6699F638C); this.mainPlayList.set('selectedIndex', 26)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.79,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_1_HS_2_0.png",
      "width": 87,
      "class": "ImageResourceLevel",
      "height": 72
     }
    ]
   },
   "pitch": -11.56,
   "yaw": 68.72,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.79,
   "yaw": 68.72,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_1_HS_2_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -11.56
  }
 ],
 "id": "overlay_439E6B92_5DC0_6E17_41AB_59157436BB5E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_D98D3810_D6EA_5CED_41DD_B3A503553ACE, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.85,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0_HS_3_0.png",
      "width": 155,
      "class": "ImageResourceLevel",
      "height": 152
     }
    ]
   },
   "pitch": 1.94,
   "yaw": -82.05,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.85,
   "yaw": -82.05,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 1.94
  }
 ],
 "id": "overlay_93E39622_8B4B_237E_41E0_DF458B61011D",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9BEA37E_D6ED_B312_41EA_9E72B449870A, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.75,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0_HS_4_0.png",
      "width": 130,
      "class": "ImageResourceLevel",
      "height": 134
     }
    ]
   },
   "pitch": 1.95,
   "yaw": -2.15,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.75,
   "yaw": -2.15,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 1.95
  }
 ],
 "id": "overlay_93E4A031_8B49_5F5D_41BB_4337338B6E06",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E3AF92C_202E_FD85_41A2_B682FC271CA1",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA2A23_5E43_AE34_41CE_B00E3AE1B649",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2EFA6302_202E_8D7D_41B3_72BAEE8981E0",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CA59A1F_5E43_AE0C_41D3_1A5B008EC1A2",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DBB2804_202E_FB85_41BF_9390912D11B6",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.mainPlayList.set('selectedIndex', 40)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 11.74,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_0_0.png",
      "width": 290,
      "class": "ImageResourceLevel",
      "height": 208
     }
    ]
   },
   "pitch": -23.25,
   "yaw": 172.79,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 11.74,
   "yaw": 172.79,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_0_0_0_map.gif",
      "width": 22,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -23.25
  }
 ],
 "id": "overlay_4F6440F1_5E40_7A14_41D7_2CD9407D7600",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69, this.camera_2CC8164F_202E_F783_41BA_7ADCA04F5172); this.mainPlayList.set('selectedIndex', 38)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.6,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_1_0.png",
      "width": 107,
      "class": "ImageResourceLevel",
      "height": 86
     }
    ]
   },
   "pitch": -13.4,
   "yaw": -121.47,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.6,
   "yaw": -121.47,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_1_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -13.4
  }
 ],
 "id": "overlay_4CB6F8B1_5E41_AA14_41D1_E11C9399CA49",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_4529999D_5DC0_AA0C_41D2_93A53498AB41, this.camera_2CCCE630_202E_F79D_41B6_94B4D74F5E35); this.mainPlayList.set('selectedIndex', 41)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.26,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_2_0.png",
      "width": 173,
      "class": "ImageResourceLevel",
      "height": 134
     }
    ]
   },
   "pitch": -17.51,
   "yaw": 75.22,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.26,
   "yaw": 75.22,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_2_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -17.51
  }
 ],
 "id": "overlay_4BE7C4E0_5E47_9A34_41C1_120083BB954A",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9B4B83F_D6EB_DD12_41DD_58C38F38A71D, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, this.ImageResource_C795F9A8_D6EE_7F3D_41AF_F973F2EC667F, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.71,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_3_0.png",
      "width": 84,
      "class": "ImageResourceLevel",
      "height": 82
     }
    ]
   },
   "pitch": 3.6,
   "yaw": -79.08,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.71,
   "yaw": -79.08,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 3.6
  }
 ],
 "id": "overlay_9C7FA14B_8B49_21CD_41D9_0A86D29973AE",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_C6F8A0C6_D6EA_4D72_41A3_F57B7E281005, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, this.ImageResource_C79A39A9_D6EE_7F3F_41E2_8EC6DCB271DE, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_4_0.png",
      "width": 128,
      "class": "ImageResourceLevel",
      "height": 130
     }
    ]
   },
   "pitch": 27.66,
   "yaw": -35.52,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5,
   "yaw": -35.52,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 27.66
  }
 ],
 "id": "overlay_9393A09C_8B49_FF4B_41B1_72FC57C6A9D7",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_D913CFB6_D6EA_B312_41E4_95719FBB1846, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.62,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_5_0.png",
      "width": 128,
      "class": "ImageResourceLevel",
      "height": 126
     }
    ]
   },
   "pitch": -5.89,
   "yaw": -34.52,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.62,
   "yaw": -34.52,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -5.89
  }
 ],
 "id": "overlay_93943E39_8B49_234D_41C3_F09AC2E10863",
 "data": {
  "label": "Image"
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 3.71,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_9C483117_8B49_2145_41D4_B0C51096F608",
 "yaw": -79.08,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_9C483117_8B49_2145_41D4_B0C51096F608_0_0.jpg",
    "width": 773,
    "class": "ImageResourceLevel",
    "height": 435
   },
   {
    "url": "media/popup_9C483117_8B49_2145_41D4_B0C51096F608_0_1.jpg",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 288
   }
  ]
 },
 "pitch": 3.6,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2CD21610_202E_F79D_417A_3A8D8DE01C9C",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D78BF92_202E_949D_41B0_F0A0D4BE02BD",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DAF2822_202E_FBBD_4166_0EB6E3C551E4",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E138169_202E_8D8F_418C_7052A9465B5F",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2EE75B61_202E_FDBF_41A6_32B7ACED64E9",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAB5A26_5E43_AE3C_41BF_BA7BB0543797",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2F5E7469_202E_8B8F_419F_C66D98C854FD",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E32D125_202E_8D87_41BD_81EC2F53E098",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746, this.camera_2D06FDC2_202E_F4FD_4190_BD88032CC5E2); this.mainPlayList.set('selectedIndex', 6)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.78,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_1_HS_0_0.png",
      "width": 88,
      "class": "ImageResourceLevel",
      "height": 73
     }
    ]
   },
   "pitch": -12.42,
   "yaw": 74.91,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.78,
   "yaw": 74.91,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_1_HS_0_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -12.42
  }
 ],
 "id": "overlay_0A21F9D7_19FD_EBAE_41B9_786AC000F6D9",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61, this.camera_2F506482_202E_8B7D_41B5_51A1DB0852A2); this.mainPlayList.set('selectedIndex', 4)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.16,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_1_HS_0_0.png",
      "width": 193,
      "class": "ImageResourceLevel",
      "height": 166
     }
    ]
   },
   "pitch": -32.46,
   "yaw": 133.96,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.16,
   "yaw": 133.96,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_1_HS_0_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -32.46
  }
 ],
 "id": "overlay_0912DEC2_19CD_A9A6_41AB_5BDFD19B269C",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.mainPlayList.set('selectedIndex', 3)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.56,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_1_HS_1_0.png",
      "width": 136,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -21.97,
   "yaw": 176.35,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.56,
   "yaw": 176.35,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_1_HS_1_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -21.97
  }
 ],
 "id": "overlay_0A75156C_19CC_9B62_41B4_B266D2BB2334",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746, this.camera_2F5E1469_202E_8B8F_41BA_1CA77E13B4D0); this.mainPlayList.set('selectedIndex', 6)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_04E10296_1A4C_B9AE_41A2_4A4261DC5F6B",
   "yaw": -65.59,
   "pitch": 46.67,
   "distance": 100,
   "hfov": 3.79
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.79,
   "yaw": -65.59,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_1_HS_2_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 46.67
  }
 ],
 "id": "overlay_0B7A8A27_19C3_A8EE_417B_4BA3849872B3",
 "data": {
  "label": "Circle Arrow 02"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.mainPlayList.set('selectedIndex', 13)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.25,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_1_HS_3_0.png",
      "width": 101,
      "class": "ImageResourceLevel",
      "height": 124
     }
    ]
   },
   "pitch": -17.58,
   "yaw": 98.09,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.25,
   "yaw": 98.09,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_1_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 19
     }
    ]
   },
   "pitch": -17.58
  }
 ],
 "id": "overlay_0C12C251_19CC_78A2_4182_9B4A757968BA",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAB7A26_5E43_AE3C_417D_81C3D6AB32A1",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D01EDE3_202E_F483_41A5_DE9A4DA3A57F",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE, this.camera_2E98F2A5_202E_8C87_41B1_03EA6EF3B9F9); this.mainPlayList.set('selectedIndex', 5)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 17.87,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_1_HS_0_0.png",
      "width": 410,
      "class": "ImageResourceLevel",
      "height": 255
     }
    ]
   },
   "pitch": -36.46,
   "roll": 0,
   "yaw": 115.11,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 17.87,
   "yaw": 115.11,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_1_HS_0_1_0_map.gif",
      "width": 200,
      "class": "ImageResourceLevel",
      "height": 123
     }
    ]
   },
   "pitch": -36.46
  }
 ],
 "id": "overlay_0B140046_19FC_78AE_41B3_C027A83F53B0",
 "data": {
  "label": "Polygon"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_129EC1E7_19BC_BB6E_41B6_D560A38CE404, this.camera_2E88E2D2_202E_8C9D_4157_54B6AC447634); this.mainPlayList.set('selectedIndex', 7)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.51,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_1_HS_1_0.png",
      "width": 81,
      "class": "ImageResourceLevel",
      "height": 75
     }
    ]
   },
   "pitch": -12.62,
   "yaw": -63.08,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.51,
   "yaw": -63.08,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_1_HS_1_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -12.62
  }
 ],
 "id": "overlay_0B289710_19FF_B8A2_41A1_6756F7B6430C",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_12A762AD_19BC_F9E2_41A7_28C13BCDD746_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA, this.camera_2E437A99_202E_FC8F_41A7_AE0DE01A62CE); this.mainPlayList.set('selectedIndex', 37)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.26,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_1_HS_0_0.png",
      "width": 150,
      "class": "ImageResourceLevel",
      "height": 126
     }
    ]
   },
   "pitch": -18.52,
   "yaw": -73.93,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.26,
   "yaw": -73.93,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_1_HS_0_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -18.52
  }
 ],
 "id": "overlay_49213531_5D0E_27D9_41C8_C93A8734A9E3",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_545F4D5A_5E40_6A14_4190_82573CA6B5DA, this.camera_2EB3DAC6_202E_FC85_41A3_2D7E635CB23B); this.mainPlayList.set('selectedIndex', 39)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.69,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_1_HS_1_0.png",
      "width": 86,
      "class": "ImageResourceLevel",
      "height": 79
     }
    ]
   },
   "pitch": -12.48,
   "yaw": 132.5,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.69,
   "yaw": 132.5,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_1_HS_1_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -12.48
  }
 ],
 "id": "overlay_52FCA697_5E40_661D_4171_8B45A9D6F0E6",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_54BB529E_5E40_9E0F_41C2_D57778CAA372, this.camera_2EA50AE9_202E_FC8F_41BF_1584192CA2A7); this.mainPlayList.set('selectedIndex', 40)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.19,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_2_0.png",
      "width": 124,
      "class": "ImageResourceLevel",
      "height": 102
     }
    ]
   },
   "pitch": -18.22,
   "yaw": 170.59,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.19,
   "yaw": 170.59,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_2_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -18.22
  }
 ],
 "id": "overlay_4E99376C_5E41_E633_41C5_993095B80481",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55, this.camera_2E51CA6B_202E_FF83_4176_7273ED7D8093); this.mainPlayList.set('selectedIndex', 34)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.65,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_3_0.png",
      "width": 109,
      "class": "ImageResourceLevel",
      "height": 84
     }
    ]
   },
   "pitch": -14.03,
   "yaw": -127.13,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.65,
   "yaw": -127.13,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_3_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -14.03
  }
 ],
 "id": "overlay_40AC2DBB_6240_AA14_41CF_D95532F41AA9",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_D98D7E0E_D6EA_54F2_41D3_36841F916DCC, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.05,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_4_0.png",
      "width": 99,
      "class": "ImageResourceLevel",
      "height": 102
     }
    ]
   },
   "pitch": 21.37,
   "yaw": -25.29,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.05,
   "yaw": -25.29,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 21.37
  }
 ],
 "id": "overlay_9C76287F_8B49_6FC6_41DF_C4A038B9D5DB",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9BC7DE7_D6EA_D733_41E5_D608406EDFEC, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.57,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_5_0.png",
      "width": 104,
      "class": "ImageResourceLevel",
      "height": 106
     }
    ]
   },
   "pitch": -5.18,
   "yaw": -25.38,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.57,
   "yaw": -25.38,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -5.18
  }
 ],
 "id": "overlay_9C28DB89_8B49_E14D_41D6_5DCD2CB3B961",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_D95CEA5E_D6EA_BD12_41E0_21FCE4B06EF7, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, this.ImageResource_C79169A3_D6EE_7F33_41C5_047D0C373F49, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.71,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_6_0.png",
      "width": 107,
      "class": "ImageResourceLevel",
      "height": 112
     }
    ]
   },
   "pitch": 6.09,
   "yaw": 49.99,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.71,
   "yaw": 49.99,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_6_0_0_map.gif",
      "width": 15,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 6.09
  }
 ],
 "id": "overlay_935E77AB_8B4E_E14E_41A4_042CF5DC4549",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_D94CDD6E_D6EA_5735_4193_08B6A0C3A054, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, this.ImageResource_C797D9A5_D6EE_7F37_41E1_D944487B2AC8, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.67,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_7_0.png",
      "width": 83,
      "class": "ImageResourceLevel",
      "height": 80
     }
    ]
   },
   "pitch": 3.1,
   "yaw": 91.98,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.67,
   "yaw": 91.98,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_0_HS_7_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 3.1
  }
 ],
 "id": "overlay_9318D608_8B4F_234B_41D5_39850653618F",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DA23EF8_202E_F48D_41B1_E4E913D9A251",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB, this.camera_2D05EF6E_202E_9585_4198_BDB360E6A3BF); this.mainPlayList.set('selectedIndex', 2)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.36,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_1_HS_0_0.png",
      "width": 166,
      "class": "ImageResourceLevel",
      "height": 161
     }
    ]
   },
   "pitch": -29.57,
   "yaw": 78.23,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.36,
   "yaw": 78.23,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_1_HS_0_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 15
     }
    ]
   },
   "pitch": -29.57
  }
 ],
 "id": "overlay_08F7FBE8_19C4_EF62_41B9_491191D8C0DF",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61, this.camera_2D795F92_202E_949D_41AD_086B986C8F32); this.mainPlayList.set('selectedIndex', 4)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.25,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_1_HS_1_0.png",
      "width": 153,
      "class": "ImageResourceLevel",
      "height": 136
     }
    ]
   },
   "pitch": -22.52,
   "yaw": -72.02,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.25,
   "yaw": -72.02,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_1_HS_1_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -22.52
  }
 ],
 "id": "overlay_0977CEA3_19C5_A9E6_41AB_6EC97C414062",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61, this.camera_2D6A3FB0_202E_949D_4158_46EA4BF889F8); this.mainPlayList.set('selectedIndex', 4)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.35,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_1_HS_2_0.png",
      "width": 130,
      "class": "ImageResourceLevel",
      "height": 113
     }
    ]
   },
   "pitch": -21.75,
   "yaw": -106.52,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.35,
   "yaw": -106.52,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_1_HS_2_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -21.75
  }
 ],
 "id": "overlay_005323CB_1A44_9FA6_41AB_E77FB56E6DA5",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C0509643_D090_64CB_41D3_8C27BAAA6356, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.21,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0_HS_3_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -4.75,
   "yaw": -1.58,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.21,
   "yaw": -1.58,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.75
  }
 ],
 "id": "overlay_9D31398D_8B7A_E145_41E0_C28146784950",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C049176A_D08F_E4C5_4187_C9C0D55A0FB8, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.21,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0_HS_4_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -4.83,
   "yaw": 146.25,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.21,
   "yaw": 146.25,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.83
  }
 ],
 "id": "overlay_9398B8A6_8B79_6F47_419D_729495D8BEC5",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_DF32787E_D0F0_2CBD_41C5_82E55182878D, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.26,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0_HS_5_0.png",
      "width": 97,
      "class": "ImageResourceLevel",
      "height": 94
     }
    ]
   },
   "pitch": -3.6,
   "yaw": -159.01,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.26,
   "yaw": -159.01,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -3.6
  }
 ],
 "id": "overlay_9DE3875E_8B47_21C7_41CC_CDE0F8EEEC61",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C, this.camera_2D682794_202E_F485_417C_40268133FE2D); this.mainPlayList.set('selectedIndex', 3)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.83,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_1_HS_0_0.png",
      "width": 117,
      "class": "ImageResourceLevel",
      "height": 105
     }
    ]
   },
   "pitch": -20.42,
   "yaw": 59.79,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.83,
   "yaw": 59.79,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_1_HS_0_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -20.42
  }
 ],
 "id": "overlay_0C578D91_19C4_6BA2_41B1_88ABBC35615D",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE, this.camera_2D5C17B1_202E_F49F_41B7_99932AB08C68); this.mainPlayList.set('selectedIndex', 5)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.3,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_1_HS_1_0.png",
      "width": 166,
      "class": "ImageResourceLevel",
      "height": 134
     }
    ]
   },
   "pitch": -30.38,
   "yaw": 145.13,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.3,
   "yaw": 145.13,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_1_HS_1_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -30.38
  }
 ],
 "id": "overlay_0D882EA5_19C4_A9ED_4199_B7DA3E4A5E1B",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A6D467_19BD_996E_419C_647D99E534FD, this.camera_2D5197CE_202E_F485_41B7_A54C2B2F56A5); this.mainPlayList.set('selectedIndex', 13)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.4,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_1_HS_2_0.png",
      "width": 131,
      "class": "ImageResourceLevel",
      "height": 117
     }
    ]
   },
   "pitch": -20.17,
   "yaw": -56.65,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.4,
   "yaw": -56.65,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_1_HS_2_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -20.17
  }
 ],
 "id": "overlay_0D385434_19C3_98E2_4176_749EFE452DA2",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C1983982_D090_6C45_41BF_C4ECEB63CADF, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.21,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0_HS_3_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -4.71,
   "yaw": 4.16,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.21,
   "yaw": 4.16,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.71
  }
 ],
 "id": "overlay_9D6BAABA_8B4B_634E_41BC_8D56557FB59A",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D1C2D49_202E_F58F_41AB_5A121CDE8BE3",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DC9890E_202E_FD85_41B9_18D67A757428",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E24414B_202E_8D83_41B6_5419ACC3D59F",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5, this.camera_2ECBD381_202E_8D7F_41A6_1136CA82F253); this.mainPlayList.set('selectedIndex', 22)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 9.89,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0_HS_0_0.png",
      "width": 261,
      "class": "ImageResourceLevel",
      "height": 201
     }
    ]
   },
   "pitch": -30.64,
   "yaw": -72.7,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 9.89,
   "yaw": -72.7,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0_HS_0_0_0_map.gif",
      "width": 130,
      "class": "ImageResourceLevel",
      "height": 100
     }
    ]
   },
   "pitch": -30.64
  }
 ],
 "id": "overlay_FBCBC83C_EDA5_4824_41B0_C8D01F688D93",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD, this.camera_2F3CE3AC_202E_8C85_4168_2A0DB30B9018); this.mainPlayList.set('selectedIndex', 25)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.95,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_1_HS_1_0.png",
      "width": 226,
      "class": "ImageResourceLevel",
      "height": 195
     }
    ]
   },
   "pitch": -25.84,
   "yaw": 173.72,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_FB6902FE_EDA6_D824_41E9_F8DA0862F42A",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.mainPlayList.set('selectedIndex', 24)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.9,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0_HS_2_0.png",
      "width": 162,
      "class": "ImageResourceLevel",
      "height": 140
     }
    ]
   },
   "pitch": -15.42,
   "yaw": -130.15,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.9,
   "yaw": -130.15,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0_HS_2_0_0_map.gif",
      "width": 81,
      "class": "ImageResourceLevel",
      "height": 70
     }
    ]
   },
   "pitch": -15.42
  }
 ],
 "id": "overlay_F96D7EF2_EE62_C83C_41CC_F41C3EEEE4B3",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move to the Next Room",
   "click": "this.startPanoramaWithCamera(this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64, this.camera_2F2CD3CB_202E_8C83_41AA_20A0B2F4F9B4); this.mainPlayList.set('selectedIndex', 26)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_7F409AC2_6240_6E74_41B4_AE95012B9380",
   "yaw": 100.6,
   "pitch": -25.12,
   "distance": 100,
   "hfov": 17.36
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 17.36,
   "yaw": 100.6,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0_HS_6_0_0_map.gif",
      "width": 27,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -25.12
  }
 ],
 "id": "overlay_7C925112_6243_BA14_4171_70994087672B",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C20487D9_D33A_9150_41DA_27461F12546D, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.75,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0_HS_7_0.png",
      "width": 108,
      "class": "ImageResourceLevel",
      "height": 103
     }
    ]
   },
   "pitch": -5.56,
   "yaw": 17.22,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.75,
   "yaw": 17.22,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0_HS_7_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 15
     }
    ]
   },
   "pitch": -5.56
  }
 ],
 "id": "overlay_9C839EBD_8BC6_E34A_41D8_5D9679DE4C9F",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA0A22_5E43_AE34_41D6_AB02A1CFBA2F",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAB0A27_5E43_AE3C_41A6_5479FDA190F4",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236, this.camera_2F6E9449_202E_8B8F_41A4_6262F23DFD5B); this.mainPlayList.set('selectedIndex', 32)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.87,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_1_HS_0_0.png",
      "width": 89,
      "class": "ImageResourceLevel",
      "height": 76
     }
    ]
   },
   "pitch": -11.49,
   "yaw": -92.18,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.87,
   "yaw": -92.18,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_1_HS_0_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -11.49
  }
 ],
 "id": "overlay_4DA42591_59A2_0A7A_41BD_A4329674AABC",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55, this.camera_2F1DA3E8_202E_8C8D_41AB_0F8B04488A9B); this.mainPlayList.set('selectedIndex', 34)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.96,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_1_HS_1_0.png",
      "width": 143,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -18.56,
   "yaw": 157.5,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.96,
   "yaw": 157.5,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_1_HS_1_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -18.56
  }
 ],
 "id": "overlay_4A00726A_59A2_0EAE_41B5_1272DBC33EAA",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827, this.camera_2F0DD406_202E_8B85_41B7_2424DD11B8D9); this.mainPlayList.set('selectedIndex', 35)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 9.09,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_1_HS_2_0.png",
      "width": 220,
      "class": "ImageResourceLevel",
      "height": 178
     }
    ]
   },
   "pitch": -20.38,
   "yaw": -131.13,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 9.09,
   "yaw": -131.13,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_1_HS_2_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -20.38
  }
 ],
 "id": "overlay_4C926DFA_59A3_F5AE_41A7_2C33B596B98F",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA, this.camera_2F7F5425_202E_8B87_41BF_F5B375D108D4); this.mainPlayList.set('selectedIndex', 37)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_1_HS_3_0.png",
      "width": 124,
      "class": "ImageResourceLevel",
      "height": 106
     }
    ]
   },
   "pitch": -17.98,
   "yaw": 92.13,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.22,
   "yaw": 92.13,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_1_HS_3_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -17.98
  }
 ],
 "id": "overlay_531EEF29_5D0E_23C9_41D5_2F34D8EBF2D0",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9839D16_D697_F712_41E0_0E1BF02FF1D2, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, this.ImageResource_C7842991_D6EE_7FEF_41E8_F72C0B29EBCF, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.68,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0_HS_4_0.png",
      "width": 106,
      "class": "ImageResourceLevel",
      "height": 104
     }
    ]
   },
   "pitch": 3.03,
   "yaw": -40.93,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.68,
   "yaw": -40.93,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 3.03
  }
 ],
 "id": "overlay_937B4B3D_8B59_214A_41BF_DAE2BE500F87",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9CD950E_D696_54F5_41D7_E27C55E90915, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, this.ImageResource_C7857993_D6EE_7F13_41E9_9817526D66A7, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.12,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0_HS_5_0.png",
      "width": 99,
      "class": "ImageResourceLevel",
      "height": 100
     }
    ]
   },
   "pitch": 19.35,
   "yaw": 4.81,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.12,
   "yaw": 4.81,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 19.35
  }
 ],
 "id": "overlay_91B4FB58_8B46_E1CA_41C2_A908818EF4F3",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D93636C6_D696_B572_41D3_A9389E95088F, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, this.ImageResource_C7850994_D6EE_7F15_41C4_7FEAB5330B80, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.81,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0_HS_6_0.png",
      "width": 109,
      "class": "ImageResourceLevel",
      "height": 104
     }
    ]
   },
   "pitch": -4.56,
   "yaw": 4.74,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.81,
   "yaw": 4.74,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0_HS_6_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.56
  }
 ],
 "id": "overlay_9307B0C0_8B47_FF3B_41C9_15CDAD4F9173",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9241176_D696_CF12_41E4_1E0ED2648F92, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.92,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0_HS_7_0.png",
      "width": 112,
      "class": "ImageResourceLevel",
      "height": 112
     }
    ]
   },
   "pitch": 1.54,
   "yaw": 53.18,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.92,
   "yaw": 53.18,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_0_HS_7_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 1.54
  }
 ],
 "id": "overlay_9C33CFB7_8B47_2146_41AE_9A7215614BD7",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DD678F0_202E_FC9D_41AE_4D5ED9EB4823",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E7541A8_202E_8C8D_41B5_F543FF8E4246",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DF028B7_202E_FC83_41BB_E045AF597CC6",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2FA0D4DF_202E_F483_41B3_5398EEC39976",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F, this.camera_2DADB02C_202E_8B85_41A7_A8543EF13B30); this.mainPlayList.set('selectedIndex', 33)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.62,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_1_HS_0_0.png",
      "width": 179,
      "class": "ImageResourceLevel",
      "height": 140
     }
    ]
   },
   "pitch": -14.7,
   "yaw": -117.32,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.62,
   "yaw": -117.32,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_1_HS_0_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -14.7
  }
 ],
 "id": "overlay_4F1DD81D_5D0A_2DC9_41A3_EA15A98B1021",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69, this.camera_2D8F906B_202E_8B83_41C0_1EBD97392E99); this.mainPlayList.set('selectedIndex', 38)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.82,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_1_HS_1_0.png",
      "width": 139,
      "class": "ImageResourceLevel",
      "height": 111
     }
    ]
   },
   "pitch": -18.11,
   "yaw": 88.3,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.82,
   "yaw": 88.3,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_1_HS_1_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -18.11
  }
 ],
 "id": "overlay_49DFD4E5_5D0F_E679_41D1_146FDEF85EDB",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55, this.camera_2D9E004B_202E_8B83_41B1_B05C59816D3A); this.mainPlayList.set('selectedIndex', 34)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.21,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0_HS_2_0.png",
      "width": 202,
      "class": "ImageResourceLevel",
      "height": 159
     }
    ]
   },
   "pitch": -22.84,
   "yaw": 170.5,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.21,
   "yaw": 170.5,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0_HS_2_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -22.84
  }
 ],
 "id": "overlay_4DF5CA4B_5E40_AE75_41C9_F69E1636E48C",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_D9814A4E_D695_DD75_41B1_6706B567BE99, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.1,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0_HS_3_0.png",
      "width": 116,
      "class": "ImageResourceLevel",
      "height": 125
     }
    ]
   },
   "pitch": 1.78,
   "yaw": -73.23,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.1,
   "yaw": -73.23,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 17
     }
    ]
   },
   "pitch": 1.78
  }
 ],
 "id": "overlay_9C071435_8B4B_2745_41D1_80690DBE70A6",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.showPopupPanoramaOverlay(this.popup_D988123E_D695_CD12_41E2_86019132F0C0, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 9.03,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0_HS_4_0.png",
      "width": 205,
      "class": "ImageResourceLevel",
      "height": 173
     }
    ]
   },
   "pitch": 2.25,
   "yaw": -8.11,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 9.03,
   "yaw": -8.11,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_0_HS_4_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": 2.25
  }
 ],
 "id": "overlay_93EEB843_8B4B_6F3E_41D3_BBAF4293F9CF",
 "data": {
  "label": "Image"
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 5.1,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_9C1A63FF_8B4B_20C5_41CC_775AEA99B7BE",
 "yaw": -73.23,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_9C1A63FF_8B4B_20C5_41CC_775AEA99B7BE_0_0.jpg",
    "width": 773,
    "class": "ImageResourceLevel",
    "height": 435
   },
   {
    "url": "media/popup_9C1A63FF_8B4B_20C5_41CC_775AEA99B7BE_0_1.jpg",
    "width": 512,
    "class": "ImageResourceLevel",
    "height": 288
   }
  ]
 },
 "pitch": 1.78,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CA5BA20_5E43_AE34_41D0_6BD8E374AFE8",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA0A28_5E43_AE34_41A6_93B5466D0214",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA6A22_5E43_AE34_41A8_2F60FE799227",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2F3CC3AC_202E_8C85_4186_857FC4071B80",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1E682630_144B_2D61_419D_2C5C83D95A53, this.camera_2DBBC804_202E_FB85_41BA_5206F1438AA5); this.mainPlayList.set('selectedIndex', 1)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.26,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_1_HS_0_0.png",
      "width": 131,
      "class": "ImageResourceLevel",
      "height": 114
     }
    ]
   },
   "pitch": -24.07,
   "yaw": -105.07,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.26,
   "yaw": -105.07,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_1_HS_0_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -24.07
  }
 ],
 "id": "overlay_048F73BE_14A9_5776_4197_902078065B08",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1270D5D6_19BC_7BAE_41B8_F3FB0F40085C, this.camera_2D4597E8_202E_F48D_41AA_36DA50B76686); this.mainPlayList.set('selectedIndex', 3)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.3,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0_HS_1_0.png",
      "width": 212,
      "class": "ImageResourceLevel",
      "height": 168
     }
    ]
   },
   "pitch": -27.22,
   "yaw": 107.74,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.3,
   "yaw": 107.74,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0_HS_1_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -27.22
  }
 ],
 "id": "overlay_16D18303_19DC_98A6_41B6_420CFC7C766E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C0167782_D0F0_2445_41B6_21C0F38920E4, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.2,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0_HS_2_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -5.75,
   "yaw": -11.82,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.2,
   "yaw": -11.82,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_0_HS_2_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -5.75
  }
 ],
 "id": "overlay_9DBA99EE_8B79_60C7_41D6_852C0DBD2495",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D51F7CE_202E_F485_41B5_F89AD33F3B06",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move to the Next Room",
   "click": "this.startPanoramaWithCamera(this.panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5, this.camera_2E32E125_202E_8D87_41B1_5BE7EB284AD6); this.mainPlayList.set('selectedIndex', 15)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_04EB62A2_1A4C_B9E6_41AD_1AF31ED9E4EB",
   "yaw": 0.39,
   "pitch": -9.89,
   "distance": 100,
   "hfov": 8.38
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.38,
   "yaw": 0.39,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_1_HS_0_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -9.89
  }
 ],
 "id": "overlay_026733EF_1A44_9F7E_41B2_98B754B215F0",
 "data": {
  "label": "Circle Door 02"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA, this.camera_2DD2F0E4_202E_8C85_41B2_B54713D11158); this.mainPlayList.set('selectedIndex', 12)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.24,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_1_HS_1_0.png",
      "width": 131,
      "class": "ImageResourceLevel",
      "height": 114
     }
    ]
   },
   "pitch": -24.57,
   "yaw": 122.24,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.24,
   "yaw": 122.24,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_1_HS_1_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -24.57
  }
 ],
 "id": "overlay_061DBD61_1A44_AB62_41B7_FDA97256CA8C",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.mainPlayList.set('selectedIndex', 5)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.55,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_1_HS_2_0.png",
      "width": 130,
      "class": "ImageResourceLevel",
      "height": 114
     }
    ]
   },
   "pitch": -15.41,
   "yaw": -113.62,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.55,
   "yaw": -113.62,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_1_HS_2_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -15.41
  }
 ],
 "id": "overlay_062AFC99_1A45_A9A2_41B7_628B647C4FDE",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "click": "this.startPanoramaWithCamera(this.panorama_12A6D467_19BD_996E_419C_647D99E534FD, this.camera_2DC28108_202E_8D8D_41B0_7B84D8104902); this.mainPlayList.set('selectedIndex', 13)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.61,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0_HS_3_0.png",
      "width": 148,
      "class": "ImageResourceLevel",
      "height": 136
     }
    ]
   },
   "pitch": -31.05,
   "yaw": 161.38,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.61,
   "yaw": 161.38,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_0_HS_3_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -31.05
  }
 ],
 "id": "overlay_DC45425B_D365_B350_41DE_45A497726315",
 "data": {
  "label": "Image"
 }
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D6A1FB0_202E_949D_417B_AE60B47D2C74",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21, this.camera_2D95AF17_202E_F582_41A5_32476D415251); this.mainPlayList.set('selectedIndex', 19)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 10.67,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_1_HS_0_0.png",
      "width": 310,
      "class": "ImageResourceLevel",
      "height": 259
     }
    ]
   },
   "pitch": -38.44,
   "yaw": 165.54,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_E6CFD8ED_F6F3_825D_41E8_418C5030BC71",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1, this.camera_2DA2DEF8_202E_F48D_41A6_7405B5538F6B); this.mainPlayList.set('selectedIndex', 17)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.37,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_1_HS_1_0.png",
      "width": 205,
      "class": "ImageResourceLevel",
      "height": 204
     }
    ]
   },
   "pitch": -22.22,
   "yaw": -92.14,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_E6D26A0B_F6F4_81C6_41B1_3E5C4F11DA1E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032, this.camera_2D89EF36_202E_F585_41B0_95D8B409BF23); this.mainPlayList.set('selectedIndex', 16)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.47,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_1_HS_2_0.png",
      "width": 130,
      "class": "ImageResourceLevel",
      "height": 113
     }
    ]
   },
   "pitch": -18.07,
   "yaw": -131.27,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_F91D4B77_F6F5_864E_41BE_B69ACB973A21",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C1B7142E_D190_245D_41C8_6E04A1650973, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.16,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0_HS_3_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -9.28,
   "yaw": -12.69,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.16,
   "yaw": -12.69,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -9.28
  }
 ],
 "id": "overlay_9C2FF11C_8BC9_6147_41DB_A3492B43B690",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5, this.camera_2E3A992C_202E_FD85_41AE_6770856C3922); this.mainPlayList.set('selectedIndex', 15)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.01,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_1_HS_0_0.png",
      "width": 174,
      "class": "ImageResourceLevel",
      "height": 153
     }
    ]
   },
   "pitch": -23.98,
   "yaw": 102.95,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_F46A5E5F_F8D7_1110_41B3_DD38B8485771",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F45B465D_F829_1113_41C8_5404C0C6EEB1, this.camera_2DD608F0_202E_FC9D_41A1_32C3D7957812); this.mainPlayList.set('selectedIndex', 17)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 10.09,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_1_HS_1_0.png",
      "width": 271,
      "class": "ImageResourceLevel",
      "height": 236
     }
    ]
   },
   "pitch": -32.28,
   "yaw": -161.22,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_F46A4E5F_F8D7_1110_41B6_DA2653D45080",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7, this.camera_2DCA490E_202E_FD85_41AE_7C578B5075E9); this.mainPlayList.set('selectedIndex', 18)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.62,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_1_HS_2_0.png",
      "width": 135,
      "class": "ImageResourceLevel",
      "height": 128
     }
    ]
   },
   "pitch": -18.85,
   "yaw": -113.95,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_F46A6E5F_F8D7_1110_41B5_4659FF454C20",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C1D948AD_D1B0_2C5F_41E7_6F23D711C545, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.19,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0_HS_3_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -6.69,
   "yaw": 28.34,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.19,
   "yaw": 28.34,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -6.69
  }
 ],
 "id": "overlay_9C06385E_8BC9_2FC6_41D9_83E1346F4C81",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C1E128D5_D1B0_6DCF_419F_4F716E54F5F2, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.2,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0_HS_4_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -6.49,
   "yaw": -33.36,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.2,
   "yaw": -33.36,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -6.49
  }
 ],
 "id": "overlay_9D76A5B0_8BC9_E15B_41C6_4DC6838FEF00",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_F469BE5F_F8D7_1110_41D1_BAF9172CD032_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DD2C0E5_202E_8C87_418F_E4785E0727BF",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5, this.camera_2E24614B_202E_8D83_41BD_CC9A131B4D59); this.mainPlayList.set('selectedIndex', 22)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.91,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0_HS_0_0.png",
      "width": 221,
      "class": "ImageResourceLevel",
      "height": 190
     }
    ]
   },
   "pitch": -23.81,
   "yaw": 174.68,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.91,
   "yaw": 174.68,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0_HS_0_0_0_map.gif",
      "width": 110,
      "class": "ImageResourceLevel",
      "height": 95
     }
    ]
   },
   "pitch": -23.81
  }
 ],
 "id": "overlay_E2A77298_EDBF_F8EC_41DF_CED93AFA7947",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6, this.camera_2E13A169_202E_8D8F_41B7_2AF7C3AD8499); this.mainPlayList.set('selectedIndex', 24)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.86,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0_HS_2_0.png",
      "width": 189,
      "class": "ImageResourceLevel",
      "height": 155
     }
    ]
   },
   "pitch": -19.18,
   "yaw": -114.59,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.86,
   "yaw": -114.59,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0_HS_2_0_0_map.gif",
      "width": 94,
      "class": "ImageResourceLevel",
      "height": 77
     }
    ]
   },
   "pitch": -19.18
  }
 ],
 "id": "overlay_F589190F_EDA2_C9E4_41E6_16082ED87566",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FBF97638_F199_BD93_41D1_05116F346852, this.camera_2E052189_202E_8C8F_41BD_4673770D891B); this.mainPlayList.set('selectedIndex', 20)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.43,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0_HS_3_0.png",
      "width": 131,
      "class": "ImageResourceLevel",
      "height": 114
     }
    ]
   },
   "pitch": -19.38,
   "yaw": -65.22,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.43,
   "yaw": -65.22,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0_HS_3_0_0_map.gif",
      "width": 65,
      "class": "ImageResourceLevel",
      "height": 57
     }
    ]
   },
   "pitch": -19.38
  }
 ],
 "id": "overlay_FC351729_F187_BBB5_41E6_0E582C7E3B51",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C3D5AEF8_D36B_B350_41E1_AE89846FB13B, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.16,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0_HS_4_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -9.55,
   "yaw": -0.8,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.16,
   "yaw": -0.8,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -9.55
  }
 ],
 "id": "overlay_9CCF1017_8BF9_7F46_41D9_289613B32ADF",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C240F8CF_D36A_BFB0_41D2_86D6D46DB78E, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.18,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0_HS_5_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -8.02,
   "yaw": 85.16,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.18,
   "yaw": 85.16,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -8.02
  }
 ],
 "id": "overlay_9CC15341_8BFB_213D_4195_C0AE9FC46BA2",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_F8567126_F198_57BF_41D9_3446ACE07DBB, this.camera_2DFE1893_202E_FC83_41B7_353A9C7D02F4); this.mainPlayList.set('selectedIndex', 28)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 13.09,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0_HS_0_0.png",
      "width": 323,
      "class": "ImageResourceLevel",
      "height": 205
     }
    ]
   },
   "pitch": -23.13,
   "yaw": -92.39,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_E1B2C083_F1B8_5575_41DB_BF481010F86F",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.mainPlayList.set('selectedIndex', 27)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 10.45,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0_HS_1_0.png",
      "width": 248,
      "class": "ImageResourceLevel",
      "height": 145
     }
    ]
   },
   "pitch": -17.03,
   "yaw": -158.03,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_E1DA5E0D_F1B8_ED8D_41DD_E8B9F8B58EFA",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_5C95097D_57AD_BD37_41D2_11CA2C70C95B, this.camera_2D8A9879_202E_FB8F_419D_8A970B9B09FD); this.mainPlayList.set('selectedIndex', 30)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.34,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0_HS_2_0.png",
      "width": 169,
      "class": "ImageResourceLevel",
      "height": 96
     }
    ]
   },
   "pitch": -10.91,
   "yaw": -12.72,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.34,
   "yaw": -12.72,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_0_HS_2_0_0_map.gif",
      "width": 28,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -10.91
  }
 ],
 "id": "overlay_591C87A2_57BF_F5CD_41BC_A952A021E0BA",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_F855BBCD_F19F_AA8D_41B4_DCFDAEA66123_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA3A28_5E43_AE34_41CF_F3E6A80EBFA8",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DED1F72_202E_F59D_41A6_03FBACC3CEC1",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAACA24_5E43_AE3C_41CD_6BFBC35762E6",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA1A27_5E43_AE3C_41CF_1B8605D05CF4",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D2DCCF1_202E_F49F_41A9_24215F3C3EAC",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA4A21_5E43_AE34_41CA_A44A1433C20B",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA2A28_5E43_AE34_41D5_777C478DAFBE",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA, this.camera_2DAF3822_202E_FBBD_418F_3E6E0BEF9B6C); this.mainPlayList.set('selectedIndex', 12)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.65,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_1_HS_0_0.png",
      "width": 173,
      "class": "ImageResourceLevel",
      "height": 162
     }
    ]
   },
   "pitch": -28.97,
   "yaw": -96.02,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.65,
   "yaw": -96.02,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_1_HS_0_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -28.97
  }
 ],
 "id": "overlay_018C2B67_19BC_AF6E_41A3_A95286655F9B",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A43F46_19BC_A8AF_41A6_39925F9B5B61, this.camera_2D98F85E_202E_FB85_41A1_CDD03168AE22); this.mainPlayList.set('selectedIndex', 4)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.23,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_1_HS_1_0.png",
      "width": 126,
      "class": "ImageResourceLevel",
      "height": 111
     }
    ]
   },
   "pitch": -19.29,
   "yaw": 52.29,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.23,
   "yaw": 52.29,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_1_HS_1_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -19.29
  }
 ],
 "id": "overlay_01D8B7A8_19BC_A7E2_41B4_3E0EAA140753",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79, this.camera_2DA3E83D_202E_FB87_4163_565A7AB008F3); this.mainPlayList.set('selectedIndex', 14)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.17,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_1_HS_2_0.png",
      "width": 211,
      "class": "ImageResourceLevel",
      "height": 178
     }
    ]
   },
   "pitch": -28.41,
   "yaw": 164.8,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.17,
   "yaw": 164.8,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_1_HS_2_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -28.41
  }
 ],
 "id": "overlay_028E7B9A_1A44_6FA6_4191_F2BAECB08386",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C00A57C2_D090_E3C5_41DF_67E1A1B3DCA0, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.2,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0_HS_3_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -5.65,
   "yaw": -8.41,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.2,
   "yaw": -8.41,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A6D467_19BD_996E_419C_647D99E534FD_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -5.65
  }
 ],
 "id": "overlay_9E8783EF_8B49_20C6_41D9_1357BC842A11",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_12A6D467_19BD_996E_419C_647D99E534FD_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2FBF349D_202E_F487_419C_8DE1686F7A71",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52F9B72F_59A2_16A6_41D1_A287990FC827, this.camera_2D436E9B_202E_F483_41B9_1AF7EA69FB3C); this.mainPlayList.set('selectedIndex', 35)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.3,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_1_HS_0_0.png",
      "width": 126,
      "class": "ImageResourceLevel",
      "height": 106
     }
    ]
   },
   "pitch": -16.98,
   "yaw": -37.32,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.3,
   "yaw": -37.32,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_1_HS_0_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -16.98
  }
 ],
 "id": "overlay_4CA0E0B0_59A2_0BB9_41CE_F7169B1987D6",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_57B341F8_5CFA_6890_41CF_496C6F8BE6EA, this.camera_2DB8FEB8_202E_F48D_41AD_702F814D5430); this.mainPlayList.set('selectedIndex', 37)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.76,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_1_HS_1_0.png",
      "width": 163,
      "class": "ImageResourceLevel",
      "height": 132
     }
    ]
   },
   "pitch": -19.6,
   "yaw": 74.41,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.76,
   "yaw": 74.41,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_1_HS_1_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -19.6
  }
 ],
 "id": "overlay_53FC64B4_5D0E_66DE_41AF_9283CBCA0BF4",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F, this.camera_2D4EAE79_202E_F78F_41BD_30DD8137F7DF); this.mainPlayList.set('selectedIndex', 33)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.85,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_1_HS_2_0.png",
      "width": 114,
      "class": "ImageResourceLevel",
      "height": 109
     }
    ]
   },
   "pitch": -15.13,
   "yaw": 16.07,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.85,
   "yaw": 16.07,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_1_HS_2_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -15.13
  }
 ],
 "id": "overlay_4D1FF5F1_5DFA_2659_41C6_D805BCB0DA6C",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_575F3E00_5CFA_3B71_41D3_7AA8FB483A69, this.camera_2DAC8ED8_202E_F48D_41BA_DD88DAD8F87B); this.mainPlayList.set('selectedIndex', 38)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.08,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0_HS_3_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 80
     }
    ]
   },
   "pitch": -12.48,
   "yaw": 138.53,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.08,
   "yaw": 138.53,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0_HS_3_0_0_map.gif",
      "width": 23,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -12.48
  }
 ],
 "id": "overlay_4C3EC86E_5DFA_EE4A_41C4_B734CD6FEA50",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.mainPlayList.set('selectedIndex', 40)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.96,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0_HS_4_0.png",
      "width": 92,
      "class": "ImageResourceLevel",
      "height": 71
     }
    ]
   },
   "pitch": -12.79,
   "yaw": -171.58,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.96,
   "yaw": -171.58,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_0_HS_4_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -12.79
  }
 ],
 "id": "overlay_40AA148D_6241_9A0D_41A2_E62171C9BAFD",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_52F983E2_59A2_0DDE_41B5_47951B125E55_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E88D2D3_202E_8C83_41A5_5F059D634E15",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1E557619_144B_2D23_41A0_05CAAAB2652E, this.camera_2E2D595B_202E_FD83_419C_62AAE77CDC9D); this.mainPlayList.set('selectedIndex', 9)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.84,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_1_HS_0_0.png",
      "width": 249,
      "class": "ImageResourceLevel",
      "height": 207
     }
    ]
   },
   "pitch": -36.12,
   "yaw": 98.38,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.84,
   "yaw": 98.38,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_1_HS_0_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -36.12
  }
 ],
 "id": "overlay_0E5C3788_19C4_67A2_4182_278158AF3AF9",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6, this.camera_2E1D798D_202E_FC87_41B1_8E021C87D608); this.mainPlayList.set('selectedIndex', 11)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.85,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_1_HS_1_0.png",
      "width": 146,
      "class": "ImageResourceLevel",
      "height": 129
     }
    ]
   },
   "pitch": -24.85,
   "yaw": -95.52,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.85,
   "yaw": -95.52,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_1_HS_1_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -24.85
  }
 ],
 "id": "overlay_0FED0FF2_19C4_A766_41A4_F1F84BC7BEFD",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C0B430C5_D090_1DCF_41E1_52FC0CEBF52C, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0_HS_2_0.png",
      "width": 96,
      "class": "ImageResourceLevel",
      "height": 98
     }
    ]
   },
   "pitch": -4.36,
   "yaw": 32.67,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.22,
   "yaw": 32.67,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0_HS_2_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.36
  }
 ],
 "id": "overlay_9D6A3547_8B5E_E1C6_41A6_B7B378B1E0D7",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C1D2BF53_D090_64CB_41C7_ED3885933E5C, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.36,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0_HS_3_0.png",
      "width": 99,
      "class": "ImageResourceLevel",
      "height": 99
     }
    ]
   },
   "pitch": -4.42,
   "yaw": -32.55,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.36,
   "yaw": -32.55,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.42
  }
 ],
 "id": "overlay_9D139DFF_8B47_E0C6_41D8_DFA49AF57ED6",
 "data": {
  "label": "Image"
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.22,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_9E94850E_8B5E_E146_41CD_D1107F108B7E",
 "yaw": 32.67,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_9E94850E_8B5E_E146_41CD_D1107F108B7E_0_0.jpg",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 519
   },
   {
    "url": "media/popup_9E94850E_8B5E_E146_41CD_D1107F108B7E_0_1.jpg",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 341
   }
  ]
 },
 "pitch": -4.36,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E955B04_202E_FD85_419A_0F7494CA77D5",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAAAA26_5E43_AE3C_41A9_CF3A3A248EB9",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D1BE705_202E_F587_41BF_36FE7ECE4606",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1E904061_1445_25E3_41A2_C3386908E890, this.camera_2F819521_202E_F5BF_41B5_199C2A9CC0D5); this.mainPlayList.set('selectedIndex', 0)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.11,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_1_HS_0_0.png",
      "width": 95,
      "class": "ImageResourceLevel",
      "height": 69
     }
    ]
   },
   "pitch": -12.68,
   "yaw": -174.07,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.11,
   "yaw": -174.07,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_1_HS_0_0_0_map.gif",
      "width": 22,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -12.68
  }
 ],
 "id": "overlay_04FD7878_14AA_D1FB_419E_D8332CB2F5FB",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1E557619_144B_2D23_41A0_05CAAAB2652E, this.camera_2F91A4FF_202E_F483_41B6_70B986DD4F09); this.mainPlayList.set('selectedIndex', 9)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.27,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_1_HS_1_0.png",
      "width": 131,
      "class": "ImageResourceLevel",
      "height": 114
     }
    ]
   },
   "pitch": -23.7,
   "yaw": -83.2,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.27,
   "yaw": -83.2,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_1_HS_1_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -23.7
  }
 ],
 "id": "overlay_05848035_14AB_D175_4193_D0702F5E6CE3",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_DF4D5460_D090_E4C5_41E2_3F0A5DA77A2E, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.52,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0_HS_2_0.png",
      "width": 103,
      "class": "ImageResourceLevel",
      "height": 102
     }
    ]
   },
   "pitch": -4.52,
   "yaw": 28.01,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.52,
   "yaw": 28.01,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0_HS_2_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.52
  }
 ],
 "id": "overlay_9EA0F137_8B4F_E145_41DE_84F65FB07BDF",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C0A367D5_D090_23CF_41DC_40DB848226AD, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.14,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0_HS_3_0.png",
      "width": 94,
      "class": "ImageResourceLevel",
      "height": 94
     }
    ]
   },
   "pitch": -3.72,
   "yaw": -32.02,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.14,
   "yaw": -32.02,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -3.72
  }
 ],
 "id": "overlay_9D1C24FA_8B49_20CF_41E1_30369318529A",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_1E547224_144B_E561_41A3_10C4B18B9EE7_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E050189_202E_8C8F_41BF_B2273A9F81CB",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A7BFD3_19BC_A7A6_4192_2CE9A30C7363, this.camera_2E7561A8_202E_8C8D_41B1_A71B7E47779A); this.mainPlayList.set('selectedIndex', 10)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.89,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_1_HS_0_0.png",
      "width": 174,
      "class": "ImageResourceLevel",
      "height": 151
     }
    ]
   },
   "pitch": -26.05,
   "yaw": -101.33,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.89,
   "yaw": -101.33,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_1_HS_0_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -26.05
  }
 ],
 "id": "overlay_0E301A5B_19C3_A8A6_4177_6C45785E8066",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA, this.camera_2E6631D4_202E_8C85_4195_90E4E054BC9D); this.mainPlayList.set('selectedIndex', 12)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.18,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_1_HS_1_0.png",
      "width": 219,
      "class": "ImageResourceLevel",
      "height": 197
     }
    ]
   },
   "pitch": -32.03,
   "yaw": 97.73,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.18,
   "yaw": 97.73,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_1_HS_1_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 15
     }
    ]
   },
   "pitch": -32.03
  }
 ],
 "id": "overlay_0EC48508_19BC_98A2_4189_A90AA5401E1E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C04186D6_D090_25CD_41E8_5E98D6DE4735, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.36,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0_HS_2_0.png",
      "width": 99,
      "class": "ImageResourceLevel",
      "height": 99
     }
    ]
   },
   "pitch": -3.42,
   "yaw": -157.71,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.36,
   "yaw": -157.71,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0_HS_2_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -3.42
  }
 ],
 "id": "overlay_9EAF5AAC_8B49_234A_41D8_F9B8D674A9FE",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C016B8FA_D090_6DC4_41E0_AE5EE75FC8A2, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.2,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0_HS_3_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -5.86,
   "yaw": 8.88,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.2,
   "yaw": 8.88,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -5.86
  }
 ],
 "id": "overlay_9D4623A8_8B4B_214A_419C_06F7C8F92D95",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C04CC6D5_D090_25CF_41D6_9EA31BF5C0C0, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.69,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0_HS_4_0.png",
      "width": 107,
      "class": "ImageResourceLevel",
      "height": 99
     }
    ]
   },
   "pitch": -3.97,
   "yaw": 147.17,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.69,
   "yaw": 147.17,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_0_HS_4_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -3.97
  }
 ],
 "id": "overlay_9D0C7238_8B4B_234B_41D3_7D305D5802E2",
 "data": {
  "label": "Image"
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.36,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_9EA01A76_8B49_23C6_4196_E909C8272775",
 "yaw": -157.71,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_9EA01A76_8B49_23C6_4196_E909C8272775_0_0.jpg",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 519
   },
   {
    "url": "media/popup_9EA01A76_8B49_23C6_4196_E909C8272775_0_1.jpg",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 341
   }
  ]
 },
 "pitch": -3.42,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FF0D2CAE_F6EC_82DF_41E7_6A206E306DB7, this.camera_2ED89B7E_202E_FD85_416E_F10861B43D7A); this.mainPlayList.set('selectedIndex', 18)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 11.4,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_1_HS_1_0.png",
      "width": 323,
      "class": "ImageResourceLevel",
      "height": 281
     }
    ]
   },
   "pitch": -36.74,
   "yaw": -169.99,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_E54DEA2A_F714_81C6_41CB_2131EF6E3A78",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "image",
   "toolTip": "Click to Move",
   "click": "this.mainPlayList.set('selectedIndex', 16)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.48,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_1_HS_2_0.png",
      "width": 184,
      "class": "ImageResourceLevel",
      "height": 178
     }
    ]
   },
   "pitch": -22.31,
   "yaw": 74.64,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [],
 "id": "overlay_E54D1BDB_F715_8646_41D4_4759E563D260",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move to the Next Room",
   "click": "this.startPanoramaWithCamera(this.panorama_FBF97638_F199_BD93_41D1_05116F346852, this.camera_2EC94B9F_202E_FC83_41BA_D92D65ED1362); this.mainPlayList.set('selectedIndex', 20)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_435551FB_5DC0_9A15_41B4_E4DBF2C5E277",
   "yaw": 0.32,
   "pitch": -8.07,
   "distance": 100,
   "hfov": 7.61
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.61,
   "yaw": 0.32,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -8.07
  }
 ],
 "id": "overlay_4B5A5CE9_5E40_AA37_41CB_013473BEFAE7",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6, this.camera_2EF75B45_202E_FD87_418D_C107C5AECD91); this.mainPlayList.set('selectedIndex', 24)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.69,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0_HS_0_0.png",
      "width": 185,
      "class": "ImageResourceLevel",
      "height": 145
     }
    ]
   },
   "pitch": -19.84,
   "yaw": 60.14,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.69,
   "yaw": 60.14,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0_HS_0_0_0_map.gif",
      "width": 92,
      "class": "ImageResourceLevel",
      "height": 72
     }
    ]
   },
   "pitch": -19.84
  }
 ],
 "id": "overlay_FBFB3499_EE67_B8EC_41E0_3E5778D5A02F",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5, this.camera_2E950B04_202E_FD85_4187_328A905312C9); this.mainPlayList.set('selectedIndex', 22)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.25,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0_HS_1_0.png",
      "width": 150,
      "class": "ImageResourceLevel",
      "height": 126
     }
    ]
   },
   "pitch": -18.59,
   "yaw": 124.99,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.25,
   "yaw": 124.99,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0_HS_1_0_0_map.gif",
      "width": 75,
      "class": "ImageResourceLevel",
      "height": 63
     }
    ]
   },
   "pitch": -18.59
  }
 ],
 "id": "overlay_F854764F_EE65_5864_41DB_842409337F28",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8, this.camera_2E852B21_202E_FDBE_41BA_85DEFE735063); this.mainPlayList.set('selectedIndex', 23)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.77,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0_HS_2_0.png",
      "width": 138,
      "class": "ImageResourceLevel",
      "height": 124
     }
    ]
   },
   "pitch": -19.1,
   "yaw": 163.9,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.77,
   "yaw": 163.9,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0_HS_2_0_0_map.gif",
      "width": 69,
      "class": "ImageResourceLevel",
      "height": 62
     }
    ]
   },
   "pitch": -19.1
  }
 ],
 "id": "overlay_F43B7707_EE63_F9E3_41DB_310A361F3687",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move to the Next Room",
   "click": "this.startPanoramaWithCamera(this.panorama_F85BB6EC_F198_FAB3_41C2_F38FB5F79B64, this.camera_2EE76B61_202E_FDBF_41BE_7D4F698A2055); this.mainPlayList.set('selectedIndex', 26)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_7F41BAC5_6240_6E7C_41D7_9D44F6AB5CD6",
   "yaw": -143.83,
   "pitch": -19.6,
   "distance": 100,
   "hfov": 10.56
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 10.56,
   "yaw": -143.83,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0_HS_5_0_0_map.gif",
      "width": 27,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -19.6
  }
 ],
 "id": "overlay_7C6C696C_6247_AA0C_41A2_932162ACA7CE",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C2F4F500_D33A_76B0_41E8_691F0E88A4C6, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.19,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0_HS_6_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -6.86,
   "yaw": -9.88,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.19,
   "yaw": -9.88,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0_HS_6_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -6.86
  }
 ],
 "id": "overlay_9C8A7D29_8BCB_E14A_41D0_65E22692B97F",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2FE2B55C_202E_F585_41BD_ACA2FFFDBA83",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2F91C4FF_202E_F483_4173_446FCBEE6161",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52F983E2_59A2_0DDE_41B5_47951B125E55, this.camera_2D3DE684_202E_F485_41A3_157D1215552A); this.mainPlayList.set('selectedIndex', 34)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.25,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_1_HS_0_0.png",
      "width": 149,
      "class": "ImageResourceLevel",
      "height": 117
     }
    ]
   },
   "pitch": -17.69,
   "yaw": -46.16,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.25,
   "yaw": -46.16,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_1_HS_0_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -17.69
  }
 ],
 "id": "overlay_4C0C6DAB_59A6_15AE_41D6_4F81B2B8400E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52CE4BD5_59A2_FDFA_41D1_8AB8F6AA9236, this.camera_2D2636E5_202E_F487_4199_A5D5E73FA90D); this.mainPlayList.set('selectedIndex', 32)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.99,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_1_HS_1_0.png",
      "width": 173,
      "class": "ImageResourceLevel",
      "height": 162
     }
    ]
   },
   "pitch": -23.57,
   "yaw": 122.69,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.99,
   "yaw": 122.69,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_1_HS_1_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -23.57
  }
 ],
 "id": "overlay_4C1EE4F9_59A6_0BAA_41CF_BED388B1C850",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_5CEA9B5A_57AD_FD7D_41B1_0019341AB7A5, this.camera_2D3066C2_202E_F4FD_41C0_0ED059FF1BB8); this.mainPlayList.set('selectedIndex', 31)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.15,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_1_HS_2_0.png",
      "width": 123,
      "class": "ImageResourceLevel",
      "height": 106
     }
    ]
   },
   "pitch": -18.52,
   "yaw": 54.69,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.15,
   "yaw": 54.69,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_1_HS_2_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -18.52
  }
 ],
 "id": "overlay_4FBAD4FE_59A6_0BA6_41C5_99B99029F74C",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_52F9012D_59A2_0AAA_41D2_5C0D6471E98F, this.camera_2CC2766A_202E_F78D_41BE_9D404950643E); this.mainPlayList.set('selectedIndex', 33)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.52,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_1_HS_3_0.png",
      "width": 158,
      "class": "ImageResourceLevel",
      "height": 125
     }
    ]
   },
   "pitch": -20.23,
   "yaw": -117.76,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.52,
   "yaw": -117.76,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_1_HS_3_0_0_map.gif",
      "width": 20,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -20.23
  }
 ],
 "id": "overlay_4C9934DF_5DFA_6649_41CE_3A52EA046C83",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FBC4B8E4_F6E1_3BD2_41AF_6F6394D31207, this.camera_2D3776A1_202E_F4BF_41BC_ED73B0474CBA); this.mainPlayList.set('selectedIndex', 36)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "class": "HotspotPanoramaOverlayImage",
   "image": "this.AnimatedImageResource_A106C4BD_B3F8_DF12_41CA_E6F410E41466",
   "yaw": 4.1,
   "pitch": -13.26,
   "distance": 100,
   "hfov": 28.34
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 28.34,
   "yaw": 4.1,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0_HS_5_0_0_map.gif",
      "width": 48,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -13.26
  }
 ],
 "id": "overlay_A1834C92_B3C8_CF17_41D0_C553E416C836",
 "data": {
  "label": "Circle Point 03b"
 }
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E6601D4_202E_8C85_41A4_D612BFEA677D",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2F4FA482_202E_8B7D_41AA_B67EE80E4BE2",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2EC8BB9F_202E_FC83_41BC_9B3EB18D2FD8",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D06B740_202E_F5FD_41B2_AA14C04F4442",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAAEA24_5E43_AE3C_41C5_1D1D10DE1C32",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA8A25_5E43_AE3C_41C8_ED6026A639F2",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E5611FF_202E_8C83_41BE_C7C66F6285DD",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD, this.camera_2D22FD30_202E_F59D_4156_929C7A8A90E0); this.mainPlayList.set('selectedIndex', 25)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 10.1,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_0_0.png",
      "width": 241,
      "class": "ImageResourceLevel",
      "height": 158
     }
    ]
   },
   "pitch": -18,
   "yaw": -79.6,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 10.1,
   "yaw": -79.6,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_0_0_0_map.gif",
      "width": 120,
      "class": "ImageResourceLevel",
      "height": 79
     }
    ]
   },
   "pitch": -18
  }
 ],
 "id": "overlay_FA62AAD6_ED9D_C865_41D5_0A2197638795",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_E4DEA25A_ED9D_586D_41E3_03E3F860A9F5, this.camera_2D272D0F_202E_F582_41B7_C1C11588845A); this.mainPlayList.set('selectedIndex', 22)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 7.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_1_0.png",
      "width": 172,
      "class": "ImageResourceLevel",
      "height": 149
     }
    ]
   },
   "pitch": -17.63,
   "yaw": -135.36,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 7.22,
   "yaw": -135.36,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_1_0_0_map.gif",
      "width": 86,
      "class": "ImageResourceLevel",
      "height": 74
     }
    ]
   },
   "pitch": -17.63
  }
 ],
 "id": "overlay_F8F99502_EE62_B9DC_41EC_65CC37919811",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_E4064EA3_ED9F_48DC_41E6_6DEF336DB07C, this.camera_2D2DDCF1_202E_F49F_419F_8CD57D4B7B5F); this.mainPlayList.set('selectedIndex', 21)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.75,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_2_0.png",
      "width": 162,
      "class": "ImageResourceLevel",
      "height": 153
     }
    ]
   },
   "pitch": -18.64,
   "yaw": 176.04,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.75,
   "yaw": 176.04,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_2_0_0_map.gif",
      "width": 81,
      "class": "ImageResourceLevel",
      "height": 76
     }
    ]
   },
   "pitch": -18.64
  }
 ],
 "id": "overlay_FADA04E9_EE63_782C_41DC_C64045C8870C",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_FBF97638_F199_BD93_41D1_05116F346852, this.camera_2D1CCD48_202E_F58E_41B4_EC020AAD32B4); this.mainPlayList.set('selectedIndex', 20)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.16,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_3_0.png",
      "width": 131,
      "class": "ImageResourceLevel",
      "height": 114
     }
    ]
   },
   "pitch": -26.35,
   "yaw": 118.61,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.16,
   "yaw": 118.61,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_3_0_0_map.gif",
      "width": 65,
      "class": "ImageResourceLevel",
      "height": 57
     }
    ]
   },
   "pitch": -26.35
  }
 ],
 "id": "overlay_FFD95735_F188_DB9D_41EB_69CDE30F447A",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move to the Next Room",
   "click": "this.showPopupPanoramaOverlay(this.popup_C2E6C360_D32A_7170_41E4_40E2CB04F4FF, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.19,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_4_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -6.92,
   "yaw": 22.76,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.19,
   "yaw": 22.76,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -6.92
  }
 ],
 "id": "overlay_9D68CD4D_8BC9_E1CA_41D7_366DB8BE8BEE",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move to the Next Room",
   "click": "this.showPopupPanoramaOverlay(this.popup_C211C828_D32A_9EF0_41DD_EE8B9AE2E7CA, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 3.34,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_5_0.png",
      "width": 76,
      "class": "ImageResourceLevel",
      "height": 83
     }
    ]
   },
   "pitch": -6.14,
   "yaw": -26.76,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 3.34,
   "yaw": -26.76,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_0_HS_5_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 17
     }
    ]
   },
   "pitch": -6.14
  }
 ],
 "id": "overlay_9C1721C0_8BCA_E13A_41E0_F92A1E322A21",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_FEABF267_EDAF_B824_41C7_7E22D34F09C6_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D7D175E_202E_F585_4194_4797F75F92E4",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D78EE02_202E_F77D_41B8_98F3C16ED613",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2EF6BB45_202E_FD87_4191_9CE3E3F76560",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA1A22_5E43_AE34_41D3_C394A36E3772",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D680794_202E_F485_41AE_93BCA1EE502B",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAB1A27_5E43_AE3C_41C9_556ACA6BF0FD",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": -18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": -323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": -18.5
  }
 ],
 "id": "sequence_2DE3B8D8_202E_FC8D_41B0_20BA7A048AFD",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2E47F22A_202E_8F8D_41BC_65BEBFE6FA7C",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D271D10_202E_F59D_41BD_3B51C2F70354",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D22ED30_202E_F59D_41AC_35FA0ED9F082",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D05CF6E_202E_9585_41B8_CCD39CD64077",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DFE7893_202E_FC83_41A3_07A532281055",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1E904061_1445_25E3_41A2_C3386908E890, this.camera_2DE458D8_202E_FC8D_41B7_E11AD1F4B602); this.mainPlayList.set('selectedIndex', 0)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.29,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_1_HS_0_0.png",
      "width": 131,
      "class": "ImageResourceLevel",
      "height": 113
     }
    ]
   },
   "pitch": -23.27,
   "yaw": 86.65,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.29,
   "yaw": 86.65,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_1_HS_0_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -23.27
  }
 ],
 "id": "overlay_04EA43E1_1477_B70D_41B3_E27355AD81D7",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1E54B5E0_144B_6EE0_41B3_C194D90CB6AB, this.camera_2DF038B7_202E_FC83_41B3_E0D93B95288F); this.mainPlayList.set('selectedIndex', 2)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.42,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_1_HS_1_0.png",
      "width": 131,
      "class": "ImageResourceLevel",
      "height": 114
     }
    ]
   },
   "pitch": -19.77,
   "yaw": -92.33,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.42,
   "yaw": -92.33,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_1_HS_1_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -19.77
  }
 ],
 "id": "overlay_04D5D9B0_147A_D30B_41A2_C74AD47B9FFE",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C0F2D265_D0F0_3CCF_41C6_694873627FDF, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0_HS_2_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -3.83,
   "yaw": -10.74,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.22,
   "yaw": -10.74,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0_HS_2_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -3.83
  }
 ],
 "id": "overlay_9EB060EB_8B79_20CD_41DA_4CA250A14181",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C08E0602_D0F1_E445_41CD_EE518397B837, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0_HS_3_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -3.81,
   "yaw": 138.97,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.22,
   "yaw": 138.97,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0_HS_3_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -3.81
  }
 ],
 "id": "overlay_9E9D83D0_8B7F_60DB_41DF_339C27E56FC7",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C0E728B2_D0F0_2C45_4182_1AF8EB7707A3, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0_HS_4_0.png",
      "width": 119,
      "class": "ImageResourceLevel",
      "height": 119
     }
    ]
   },
   "pitch": -3.72,
   "yaw": -161.2,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.22,
   "yaw": -161.2,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E682630_144B_2D61_419D_2C5C83D95A53_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -3.72
  }
 ],
 "id": "overlay_9E9E7111_8B7F_215A_41D9_C5511487C09D",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_1E682630_144B_2D61_419D_2C5C83D95A53_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": -18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": -323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": -18.5
  }
 ],
 "id": "sequence_4CAACA29_5E43_AE34_41D3_116EDC40EBFA",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA9A24_5E43_AE3C_41B3_9BA79087BD3A",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAADA28_5E43_AE34_41D5_3D7E903262FF",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2F3ACBBD_202E_FC87_417E_9BCAAFA51B0B",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_4CAA5A21_5E43_AE34_41D6_96B953AEBD00",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2EA8D27E_202E_8F85_41BB_0338B508BA50",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A7DD0D_19BC_68A2_41A1_70778E8A18D6, this.camera_2D702E1F_202E_F783_41AE_B94F2513E629); this.mainPlayList.set('selectedIndex', 11)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 8.06,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_1_HS_0_0.png",
      "width": 214,
      "class": "ImageResourceLevel",
      "height": 194
     }
    ]
   },
   "pitch": -31.4,
   "yaw": 112.28,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 8.06,
   "yaw": 112.28,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_1_HS_0_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -31.4
  }
 ],
 "id": "overlay_0EBEE7B6_19BD_A7EE_41AC_72782126629B",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A6D467_19BD_996E_419C_647D99E534FD, this.camera_2D650E3D_202E_F787_41A4_6EBEB4D91438); this.mainPlayList.set('selectedIndex', 13)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 6.86,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_1_HS_1_0.png",
      "width": 175,
      "class": "ImageResourceLevel",
      "height": 142
     }
    ]
   },
   "pitch": -27.15,
   "yaw": -142.37,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 6.86,
   "yaw": -142.37,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_1_HS_1_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -27.15
  }
 ],
 "id": "overlay_0E680B3B_19BC_68E6_41A7_A89523767798",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79, this.camera_2D590E5C_202E_F785_41B9_CA974A00F91E); this.mainPlayList.set('selectedIndex', 14)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.52,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0_HS_2_0.png",
      "width": 108,
      "class": "ImageResourceLevel",
      "height": 87
     }
    ]
   },
   "pitch": -19.06,
   "yaw": -116.37,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.52,
   "yaw": -116.37,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0_HS_2_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -19.06
  }
 ],
 "id": "overlay_0E108A28_19BC_68E2_41A9_C6F19ECF9F1E",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C08E4886_D090_6C4D_41E3_287AABE86716, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.68,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0_HS_3_0.png",
      "width": 107,
      "class": "ImageResourceLevel",
      "height": 99
     }
    ]
   },
   "pitch": -5.04,
   "yaw": 33.26,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.68,
   "yaw": 33.26,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0_HS_3_0_0_map.gif",
      "width": 17,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -5.04
  }
 ],
 "id": "overlay_9D7CBD00_8B49_213A_41A5_954D0C7DF85C",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_C0140ACB_D090_EDDB_41CD_7B0EFA884291, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.22,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0_HS_4_0.png",
      "width": 118,
      "class": "ImageResourceLevel",
      "height": 118
     }
    ]
   },
   "pitch": -4.17,
   "yaw": -34.81,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.22,
   "yaw": -34.81,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_0_HS_4_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.17
  }
 ],
 "id": "overlay_9D688E19_8B49_234A_41C2_A6C2B0B94507",
 "data": {
  "label": "Image"
 }
},
{
 "rotationY": 0,
 "rotationX": 0,
 "hfov": 4.68,
 "rotationZ": 0,
 "showEasing": "cubic_in",
 "id": "popup_9D71ACC6_8B49_20C6_41BA_E7BE57F8E17B",
 "yaw": 33.26,
 "popupDistance": 100,
 "hideEasing": "cubic_out",
 "class": "PopupPanoramaOverlay",
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/popup_9D71ACC6_8B49_20C6_41BA_E7BE57F8E17B_0_0.jpg",
    "width": 778,
    "class": "ImageResourceLevel",
    "height": 519
   },
   {
    "url": "media/popup_9D71ACC6_8B49_20C6_41BA_E7BE57F8E17B_0_1.jpg",
    "width": 511,
    "class": "ImageResourceLevel",
    "height": 341
   }
  ]
 },
 "pitch": -5.04,
 "hideDuration": 500,
 "showDuration": 500,
 "popupMaxHeight": "95%",
 "popupMaxWidth": "95%"
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_12A43B5B_19BD_A8A6_4174_AC322499C0AA_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2D4587E9_202E_F48F_41B3_071C4DE3D6F3",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2ECBC381_202E_8D7F_41BA_50ABE91FE09C",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "movements": [
  {
   "easing": "cubic_in",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  },
  {
   "easing": "linear",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 323
  },
  {
   "easing": "cubic_out",
   "yawSpeed": 7.96,
   "class": "DistancePanoramaCameraMovement",
   "yawDelta": 18.5
  }
 ],
 "id": "sequence_2DC2F109_202E_8D8F_41AB_B80B8114D250",
 "restartMovementOnUserInteraction": false,
 "class": "PanoramaCameraSequence"
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1E682630_144B_2D61_419D_2C5C83D95A53, this.camera_2D7DC75E_202E_F585_41A5_5EBF1751349F); this.mainPlayList.set('selectedIndex', 1)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 5.38,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_1_HS_0_0.png",
      "width": 130,
      "class": "ImageResourceLevel",
      "height": 113
     }
    ]
   },
   "pitch": -20.76,
   "yaw": -90.71,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 5.38,
   "yaw": -90.71,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_1_HS_0_0_0_map.gif",
      "width": 18,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -20.76
  }
 ],
 "id": "overlay_19B5EC4D_1469_511A_4199_5558E241D262",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to Move",
   "click": "this.startPanoramaWithCamera(this.panorama_1E547224_144B_E561_41A3_10C4B18B9EE7, this.camera_2D723777_202E_F583_419C_B9A38ABEDA4C); this.mainPlayList.set('selectedIndex', 8)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.18,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_1_HS_1_0.png",
      "width": 97,
      "class": "ImageResourceLevel",
      "height": 79
     }
    ]
   },
   "pitch": -13.13,
   "yaw": 1,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.18,
   "yaw": 1,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_1_HS_1_0_0_map.gif",
      "width": 19,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -13.13
  }
 ],
 "id": "overlay_1AB6070F_1469_5F16_41A2_EBB33DDC532F",
 "data": {
  "label": "Image"
 }
},
{
 "rollOverDisplay": false,
 "areas": [
  {
   "mapColor": "#FF0000",
   "toolTip": "Click to See Image",
   "click": "this.showPopupPanoramaOverlay(this.popup_DE463E4C_D090_24DD_41DC_597D5B7E8B5C, {'pressedIconColor':'#888888','rollOverBackgroundOpacity':0.3,'pressedBorderSize':0,'rollOverIconHeight':20,'rollOverIconLineWidth':5,'iconHeight':20,'paddingBottom':5,'rollOverBorderSize':0,'pressedBackgroundColorRatios':[0,0.09803921568627451,1],'iconColor':'#000000','iconWidth':20,'rollOverBackgroundColorDirection':'vertical','backgroundColorRatios':[0,0.09803921568627451,1],'pressedIconLineWidth':5,'pressedBackgroundColorDirection':'vertical','paddingLeft':5,'borderSize':0,'rollOverIconColor':'#666666','pressedIconHeight':20,'rollOverBackgroundColorRatios':[0,0.09803921568627451,1],'iconLineWidth':5,'pressedBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'rollOverBorderColor':'#000000','rollOverBackgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBackgroundOpacity':0.3,'borderColor':'#000000','paddingRight':5,'rollOverIconWidth':20,'backgroundOpacity':0.3,'paddingTop':5,'pressedIconWidth':20,'backgroundColor':['#DDDDDD','#EEEEEE','#FFFFFF'],'pressedBorderColor':'#000000','backgroundColorDirection':'vertical'}, null, null, null, null, null, false)",
   "class": "HotspotPanoramaOverlayArea"
  }
 ],
 "items": [
  {
   "hfov": 4.56,
   "class": "HotspotPanoramaOverlayImage",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0_HS_2_0.png",
      "width": 104,
      "class": "ImageResourceLevel",
      "height": 99
     }
    ]
   },
   "pitch": -4.26,
   "yaw": 173.18,
   "distance": 50
  }
 ],
 "class": "HotspotPanoramaOverlay",
 "enabledInCardboard": true,
 "useHandCursor": true,
 "maps": [
  {
   "hfov": 4.56,
   "yaw": 173.18,
   "class": "HotspotPanoramaOverlayMap",
   "image": {
    "class": "ImageResource",
    "levels": [
     {
      "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_0_HS_2_0_0_map.gif",
      "width": 16,
      "class": "ImageResourceLevel",
      "height": 16
     }
    ]
   },
   "pitch": -4.26
  }
 ],
 "id": "overlay_8414678A_8B59_214E_41E0_40F42D29F76B",
 "data": {
  "label": "Image"
 }
},
{
 "hfov": 16.5,
 "class": "TripodCapPanoramaOverlay",
 "distance": 50,
 "image": {
  "class": "ImageResource",
  "levels": [
   {
    "url": "media/panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0.png",
    "width": 1080,
    "class": "ImageResourceLevel",
    "height": 1080
   }
  ]
 },
 "rotate": false,
 "id": "panorama_1E904061_1445_25E3_41A2_C3386908E890_tcap0",
 "angle": 0,
 "inertia": false
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_43540203_5DC0_99F5_41B0_5EC22EFB4D81",
 "class": "AnimatedImageResource",
 "colCount": 4,
 "frameCount": 24,
 "levels": [
  {
   "url": "media/panorama_FBF97638_F199_BD93_41D1_05116F346852_0_HS_3_0.png",
   "width": 800,
   "class": "ImageResourceLevel",
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_04E992A5_1A4C_B9E2_419E_541A536AF6B5",
 "class": "AnimatedImageResource",
 "colCount": 4,
 "frameCount": 24,
 "levels": [
  {
   "url": "media/panorama_F339CC21_F8EB_7130_41E6_1AD4314F4BB5_0_HS_3_0.png",
   "width": 800,
   "class": "ImageResourceLevel",
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_04E10296_1A4C_B9AE_41A2_4A4261DC5F6B",
 "class": "AnimatedImageResource",
 "colCount": 4,
 "frameCount": 24,
 "levels": [
  {
   "url": "media/panorama_12BBA7DC_19BC_E7A2_41B1_97EA90D734AE_1_HS_2_0.png",
   "width": 800,
   "class": "ImageResourceLevel",
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_7F409AC2_6240_6E74_41B4_AE95012B9380",
 "class": "AnimatedImageResource",
 "colCount": 4,
 "frameCount": 21,
 "levels": [
  {
   "url": "media/panorama_E4A0CE7F_ED9D_C823_41D1_D0D965B63AD8_0_HS_6_0.png",
   "width": 480,
   "class": "ImageResourceLevel",
   "height": 420
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_04EB62A2_1A4C_B9E6_41AD_1AF31ED9E4EB",
 "class": "AnimatedImageResource",
 "colCount": 4,
 "frameCount": 24,
 "levels": [
  {
   "url": "media/panorama_12A77A37_19BD_E8EE_41B0_2F62953E1D79_1_HS_0_0.png",
   "width": 800,
   "class": "ImageResourceLevel",
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_435551FB_5DC0_9A15_41B4_E4DBF2C5E277",
 "class": "AnimatedImageResource",
 "colCount": 4,
 "frameCount": 24,
 "levels": [
  {
   "url": "media/panorama_FF0CF949_F6ED_8242_41D2_688AC6B38A21_0_HS_3_0.png",
   "width": 800,
   "class": "ImageResourceLevel",
   "height": 1200
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_7F41BAC5_6240_6E7C_41D7_9D44F6AB5CD6",
 "class": "AnimatedImageResource",
 "colCount": 4,
 "frameCount": 21,
 "levels": [
  {
   "url": "media/panorama_FECE04B4_EDAE_D824_41CB_BBBDDFACCDAD_0_HS_5_0.png",
   "width": 480,
   "class": "ImageResourceLevel",
   "height": 420
  }
 ],
 "rowCount": 6
},
{
 "frameDuration": 41,
 "id": "AnimatedImageResource_A106C4BD_B3F8_DF12_41CA_E6F410E41466",
 "class": "AnimatedImageResource",
 "colCount": 4,
 "frameCount": 24,
 "levels": [
  {
   "url": "media/panorama_52F9B72F_59A2_16A6_41D1_A287990FC827_0_HS_5_0.png",
   "width": 1200,
   "class": "ImageResourceLevel",
   "height": 600
  }
 ],
 "rowCount": 6
}],
 "scrollBarColor": "#000000",
 "horizontalAlign": "left",
 "mobileMipmappingEnabled": false,
 "minWidth": 20,
 "scrollBarVisible": "rollOver",
 "class": "Player",
 "gap": 10,
 "scrollBarMargin": 2,
 "paddingLeft": 0,
 "buttonToggleFullscreen": "this.IconButton_E3C1AA4E_EDAE_C864_41E4_5820E86940F3",
 "scripts": {
  "showComponentsWhileMouseOver": function(parentComponent, components, durationVisibleWhileOut){  var setVisibility = function(visible){ for(var i = 0, length = components.length; i<length; i++){ var component = components[i]; if(component.get('class') == 'HTMLText' && (component.get('html') == '' || component.get('html') == undefined)) { continue; } component.set('visible', visible); } }; if (this.rootPlayer.get('touchDevice') == true){ setVisibility(true); } else { var timeoutID = -1; var rollOverFunction = function(){ setVisibility(true); if(timeoutID >= 0) clearTimeout(timeoutID); parentComponent.unbind('rollOver', rollOverFunction, this); parentComponent.bind('rollOut', rollOutFunction, this); }; var rollOutFunction = function(){ var timeoutFunction = function(){ setVisibility(false); parentComponent.unbind('rollOver', rollOverFunction, this); }; parentComponent.unbind('rollOut', rollOutFunction, this); parentComponent.bind('rollOver', rollOverFunction, this); timeoutID = setTimeout(timeoutFunction, durationVisibleWhileOut); }; parentComponent.bind('rollOver', rollOverFunction, this); } },
  "fixTogglePlayPauseButton": function(player){  var state = player.get('state'); var buttons = player.get('buttonPlayPause'); if(typeof buttons !== 'undefined' && player.get('state') == 'playing'){ if(!Array.isArray(buttons)) buttons = [buttons]; for(var i = 0; i<buttons.length; ++i) buttons[i].set('pressed', true); } },
  "getMediaHeight": function(media){  switch(media.get('class')){ case 'Video360': var res = media.get('video'); if(res instanceof Array){ var maxH=0; for(var i=0; i<res.length; i++){ var r = res[i]; if(r.get('height') > maxH) maxH = r.get('height'); } return maxH; }else{ return r.get('height') } default: return media.get('height'); } },
  "getCurrentPlayers": function(){  var players = this.getByClassName('PanoramaPlayer'); players = players.concat(this.getByClassName('VideoPlayer')); players = players.concat(this.getByClassName('Video360Player')); players = players.concat(this.getByClassName('PhotoAlbumPlayer')); return players; },
  "keepComponentVisibility": function(component, keep){  var key = 'keepVisibility_' + component.get('id'); var value = this.getKey(key); if(value == undefined && keep) { this.registerKey(key, keep); } else if(value != undefined && !keep) { this.unregisterKey(key); } },
  "unregisterKey": function(key){  delete window[key]; },
  "setMainMediaByIndex": function(index){  var item = undefined; if(index >= 0 && index < this.mainPlayList.get('items').length){ this.mainPlayList.set('selectedIndex', index); item = this.mainPlayList.get('items')[index]; } return item; },
  "shareFacebook": function(url){  window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, '_blank'); },
  "showPopupPanoramaVideoOverlay": function(popupPanoramaOverlay, closeButtonProperties, stopAudios){  var self = this; var showEndFunction = function() { popupPanoramaOverlay.unbind('showEnd', showEndFunction); closeButton.bind('click', hideFunction, this); setCloseButtonPosition(); closeButton.set('visible', true); }; var endFunction = function() { if(!popupPanoramaOverlay.get('loop')) hideFunction(); }; var hideFunction = function() { self.MainViewer.set('toolTipEnabled', true); popupPanoramaOverlay.set('visible', false); closeButton.set('visible', false); closeButton.unbind('click', hideFunction, self); popupPanoramaOverlay.unbind('end', endFunction, self); popupPanoramaOverlay.unbind('hideEnd', hideFunction, self, true); self.resumePlayers(playersPaused, true); if(stopAudios) { self.resumeGlobalAudios(); } }; var setCloseButtonPosition = function() { var right = 10; var top = 10; closeButton.set('right', right); closeButton.set('top', top); }; this.MainViewer.set('toolTipEnabled', false); var closeButton = this.closeButtonPopupPanorama; if(closeButtonProperties){ for(var key in closeButtonProperties){ closeButton.set(key, closeButtonProperties[key]); } } var playersPaused = this.pauseCurrentPlayers(true); if(stopAudios) { this.pauseGlobalAudios(); } popupPanoramaOverlay.bind('end', endFunction, this, true); popupPanoramaOverlay.bind('showEnd', showEndFunction, this, true); popupPanoramaOverlay.bind('hideEnd', hideFunction, this, true); popupPanoramaOverlay.set('visible', true); },
  "getPlayListItemByMedia": function(playList, media){  var items = playList.get('items'); for(var j = 0, countJ = items.length; j<countJ; ++j){ var item = items[j]; if(item.get('media') == media) return item; } return undefined; },
  "getPanoramaOverlayByName": function(panorama, name){  var overlays = this.getOverlays(panorama); for(var i = 0, count = overlays.length; i<count; ++i){ var overlay = overlays[i]; var data = overlay.get('data'); if(data != undefined && data.label == name){ return overlay; } } return undefined; },
  "setStartTimeVideo": function(video, time){  var items = this.getPlayListItems(video); var startTimeBackup = []; var restoreStartTimeFunc = function() { for(var i = 0; i<items.length; ++i){ var item = items[i]; item.set('startTime', startTimeBackup[i]); item.unbind('stop', restoreStartTimeFunc, this); } }; for(var i = 0; i<items.length; ++i) { var item = items[i]; var player = item.get('player'); if(player.get('video') == video && player.get('state') == 'playing') { player.seek(time); } else { startTimeBackup.push(item.get('startTime')); item.set('startTime', time); item.bind('stop', restoreStartTimeFunc, this); } } },
  "startPanoramaWithCamera": function(media, camera){  if(window.currentPanoramasWithCameraChanged != undefined && window.currentPanoramasWithCameraChanged.indexOf(media) != -1){ return; } var playLists = this.getByClassName('PlayList'); if(playLists.length == 0) return; var restoreItems = []; for(var i = 0, count = playLists.length; i<count; ++i){ var playList = playLists[i]; var items = playList.get('items'); for(var j = 0, countJ = items.length; j<countJ; ++j){ var item = items[j]; if(item.get('media') == media && (item.get('class') == 'PanoramaPlayListItem' || item.get('class') == 'Video360PlayListItem')){ restoreItems.push({camera: item.get('camera'), item: item}); item.set('camera', camera); } } } if(restoreItems.length > 0) { if(window.currentPanoramasWithCameraChanged == undefined) { window.currentPanoramasWithCameraChanged = [media]; } else { window.currentPanoramasWithCameraChanged.push(media); } var restoreCameraOnStop = function(){ var index = window.currentPanoramasWithCameraChanged.indexOf(media); if(index != -1) { window.currentPanoramasWithCameraChanged.splice(index, 1); } for (var i = 0; i < restoreItems.length; i++) { restoreItems[i].item.set('camera', restoreItems[i].camera); restoreItems[i].item.unbind('stop', restoreCameraOnStop, this); } }; for (var i = 0; i < restoreItems.length; i++) { restoreItems[i].item.bind('stop', restoreCameraOnStop, this); } } },
  "setPanoramaCameraWithCurrentSpot": function(playListItem){  var currentPlayer = this.getActivePlayerWithViewer(this.MainViewer); if(currentPlayer == undefined){ return; } var playerClass = currentPlayer.get('class'); if(playerClass != 'PanoramaPlayer' && playerClass != 'Video360Player'){ return; } var fromMedia = currentPlayer.get('panorama'); if(fromMedia == undefined) { fromMedia = currentPlayer.get('video'); } var panorama = playListItem.get('media'); var newCamera = this.cloneCamera(playListItem.get('camera')); this.setCameraSameSpotAsMedia(newCamera, fromMedia); this.startPanoramaWithCamera(panorama, newCamera); },
  "shareTwitter": function(url){  window.open('https://twitter.com/intent/tweet?source=webclient&url=' + url, '_blank'); },
  "historyGoForward": function(playList){  var history = this.get('data')['history'][playList.get('id')]; if(history != undefined) { history.forward(); } },
  "setOverlayBehaviour": function(overlay, media, action){  var executeFunc = function() { switch(action){ case 'triggerClick': this.triggerOverlay(overlay, 'click'); break; case 'stop': case 'play': case 'pause': overlay[action](); break; case 'togglePlayPause': case 'togglePlayStop': if(overlay.get('state') == 'playing') overlay[action == 'togglePlayPause' ? 'pause' : 'stop'](); else overlay.play(); break; } if(window.overlaysDispatched == undefined) window.overlaysDispatched = {}; var id = overlay.get('id'); window.overlaysDispatched[id] = true; setTimeout(function(){ delete window.overlaysDispatched[id]; }, 2000); }; if(window.overlaysDispatched != undefined && overlay.get('id') in window.overlaysDispatched) return; var playList = this.getPlayListWithMedia(media, true); if(playList != undefined){ var item = this.getPlayListItemByMedia(playList, media); if(playList.get('items').indexOf(item) != playList.get('selectedIndex')){ var beginFunc = function(e){ item.unbind('begin', beginFunc, this); executeFunc.call(this); }; item.bind('begin', beginFunc, this); return; } } executeFunc.call(this); },
  "setCameraSameSpotAsMedia": function(camera, media){  var player = this.getCurrentPlayerWithMedia(media); if(player != undefined) { var position = camera.get('initialPosition'); position.set('yaw', player.get('yaw')); position.set('pitch', player.get('pitch')); position.set('hfov', player.get('hfov')); } },
  "stopGlobalAudio": function(audio){  var audios = window.currentGlobalAudios; if(audios){ audio = audios[audio.get('id')]; if(audio){ delete audios[audio.get('id')]; if(Object.keys(audios).length == 0){ window.currentGlobalAudios = undefined; } } } if(audio) audio.stop(); },
  "executeFunctionWhenChange": function(playList, index, endFunction, changeFunction){  var endObject = undefined; var changePlayListFunction = function(event){ if(event.data.previousSelectedIndex == index){ if(changeFunction) changeFunction.call(this); if(endFunction && endObject) endObject.unbind('end', endFunction, this); playList.unbind('change', changePlayListFunction, this); } }; if(endFunction){ var playListItem = playList.get('items')[index]; if(playListItem.get('class') == 'PanoramaPlayListItem'){ var camera = playListItem.get('camera'); if(camera != undefined) endObject = camera.get('initialSequence'); if(endObject == undefined) endObject = camera.get('idleSequence'); } else{ endObject = playListItem.get('media'); } if(endObject){ endObject.bind('end', endFunction, this); } } playList.bind('change', changePlayListFunction, this); },
  "historyGoBack": function(playList){  var history = this.get('data')['history'][playList.get('id')]; if(history != undefined) { history.back(); } },
  "getPlayListItems": function(media, player){  var itemClass = (function() { switch(media.get('class')) { case 'Panorama': case 'LivePanorama': case 'HDRPanorama': return 'PanoramaPlayListItem'; case 'Video360': return 'Video360PlayListItem'; case 'PhotoAlbum': return 'PhotoAlbumPlayListItem'; case 'Map': return 'MapPlayListItem'; case 'Video': return 'VideoPlayListItem'; } })(); if (itemClass != undefined) { var items = this.getByClassName(itemClass); for (var i = items.length-1; i>=0; --i) { var item = items[i]; if(item.get('media') != media || (player != undefined && item.get('player') != player)) { items.splice(i, 1); } } return items; } else { return []; } },
  "showPopupMedia": function(w, media, playList, popupMaxWidth, popupMaxHeight, autoCloseWhenFinished, stopAudios){  var self = this; var closeFunction = function(){ playList.set('selectedIndex', -1); self.MainViewer.set('toolTipEnabled', true); if(stopAudios) { self.resumeGlobalAudios(); } this.resumePlayers(playersPaused, !stopAudios); if(isVideo) { this.unbind('resize', resizeFunction, this); } w.unbind('close', closeFunction, this); }; var endFunction = function(){ w.hide(); }; var resizeFunction = function(){ var getWinValue = function(property){ return w.get(property) || 0; }; var parentWidth = self.get('actualWidth'); var parentHeight = self.get('actualHeight'); var mediaWidth = self.getMediaWidth(media); var mediaHeight = self.getMediaHeight(media); var popupMaxWidthNumber = parseFloat(popupMaxWidth) / 100; var popupMaxHeightNumber = parseFloat(popupMaxHeight) / 100; var windowWidth = popupMaxWidthNumber * parentWidth; var windowHeight = popupMaxHeightNumber * parentHeight; var footerHeight = getWinValue('footerHeight'); var headerHeight = getWinValue('headerHeight'); if(!headerHeight) { var closeButtonHeight = getWinValue('closeButtonIconHeight') + getWinValue('closeButtonPaddingTop') + getWinValue('closeButtonPaddingBottom'); var titleHeight = self.getPixels(getWinValue('titleFontSize')) + getWinValue('titlePaddingTop') + getWinValue('titlePaddingBottom'); headerHeight = closeButtonHeight > titleHeight ? closeButtonHeight : titleHeight; headerHeight += getWinValue('headerPaddingTop') + getWinValue('headerPaddingBottom'); } var contentWindowWidth = windowWidth - getWinValue('bodyPaddingLeft') - getWinValue('bodyPaddingRight') - getWinValue('paddingLeft') - getWinValue('paddingRight'); var contentWindowHeight = windowHeight - headerHeight - footerHeight - getWinValue('bodyPaddingTop') - getWinValue('bodyPaddingBottom') - getWinValue('paddingTop') - getWinValue('paddingBottom'); var parentAspectRatio = contentWindowWidth / contentWindowHeight; var mediaAspectRatio = mediaWidth / mediaHeight; if(parentAspectRatio > mediaAspectRatio) { windowWidth = contentWindowHeight * mediaAspectRatio + getWinValue('bodyPaddingLeft') + getWinValue('bodyPaddingRight') + getWinValue('paddingLeft') + getWinValue('paddingRight'); } else { windowHeight = contentWindowWidth / mediaAspectRatio + headerHeight + footerHeight + getWinValue('bodyPaddingTop') + getWinValue('bodyPaddingBottom') + getWinValue('paddingTop') + getWinValue('paddingBottom'); } if(windowWidth > parentWidth * popupMaxWidthNumber) { windowWidth = parentWidth * popupMaxWidthNumber; } if(windowHeight > parentHeight * popupMaxHeightNumber) { windowHeight = parentHeight * popupMaxHeightNumber; } w.set('width', windowWidth); w.set('height', windowHeight); w.set('x', (parentWidth - getWinValue('actualWidth')) * 0.5); w.set('y', (parentHeight - getWinValue('actualHeight')) * 0.5); }; if(autoCloseWhenFinished){ this.executeFunctionWhenChange(playList, 0, endFunction); } var mediaClass = media.get('class'); var isVideo = mediaClass == 'Video' || mediaClass == 'Video360'; playList.set('selectedIndex', 0); if(isVideo){ this.bind('resize', resizeFunction, this); resizeFunction(); playList.get('items')[0].get('player').play(); } else { w.set('width', popupMaxWidth); w.set('height', popupMaxHeight); } this.MainViewer.set('toolTipEnabled', false); if(stopAudios) { this.pauseGlobalAudios(); } var playersPaused = this.pauseCurrentPlayers(!stopAudios); w.bind('close', closeFunction, this); w.show(this, true); },
  "showWindow": function(w, autoCloseMilliSeconds, containsAudio){  if(w.get('visible') == true){ return; } var closeFunction = function(){ clearAutoClose(); this.resumePlayers(playersPaused, !containsAudio); w.unbind('close', closeFunction, this); }; var clearAutoClose = function(){ w.unbind('click', clearAutoClose, this); if(timeoutID != undefined){ clearTimeout(timeoutID); } }; var timeoutID = undefined; if(autoCloseMilliSeconds){ var autoCloseFunction = function(){ w.hide(); }; w.bind('click', clearAutoClose, this); timeoutID = setTimeout(autoCloseFunction, autoCloseMilliSeconds); } var playersPaused = this.pauseCurrentPlayers(!containsAudio); w.bind('close', closeFunction, this); w.show(this, true); },
  "getPlayListWithMedia": function(media, onlySelected){  var playLists = this.getByClassName('PlayList'); for(var i = 0, count = playLists.length; i<count; ++i){ var playList = playLists[i]; if(onlySelected && playList.get('selectedIndex') == -1) continue; if(this.getPlayListItemByMedia(playList, media) != undefined) return playList; } return undefined; },
  "updateVideoCues": function(playList, index){  var playListItem = playList.get('items')[index]; var video = playListItem.get('media'); if(video.get('cues').length == 0) return; var player = playListItem.get('player'); var cues = []; var changeFunction = function(){ if(playList.get('selectedIndex') != index){ video.unbind('cueChange', cueChangeFunction, this); playList.unbind('change', changeFunction, this); } }; var cueChangeFunction = function(event){ var activeCues = event.data.activeCues; for(var i = 0, count = cues.length; i<count; ++i){ var cue = cues[i]; if(activeCues.indexOf(cue) == -1 && (cue.get('startTime') > player.get('currentTime') || cue.get('endTime') < player.get('currentTime')+0.5)){ cue.trigger('end'); } } cues = activeCues; }; video.bind('cueChange', cueChangeFunction, this); playList.bind('change', changeFunction, this); },
  "setMapLocation": function(panoramaPlayListItem, mapPlayer){  var resetFunction = function(){ panoramaPlayListItem.unbind('stop', resetFunction, this); player.set('mapPlayer', null); }; panoramaPlayListItem.bind('stop', resetFunction, this); var player = panoramaPlayListItem.get('player'); player.set('mapPlayer', mapPlayer); },
  "getKey": function(key){  return window[key]; },
  "init": function(){  if(!Object.hasOwnProperty('values')) { Object.values = function(o){ return Object.keys(o).map(function(e) { return o[e]; }); }; } var history = this.get('data')['history']; var playListChangeFunc = function(e){ var playList = e.source; var index = playList.get('selectedIndex'); if(index < 0) return; var id = playList.get('id'); if(!history.hasOwnProperty(id)) history[id] = new HistoryData(playList); history[id].add(index); }; var playLists = this.getByClassName('PlayList'); for(var i = 0, count = playLists.length; i<count; ++i) { var playList = playLists[i]; playList.bind('change', playListChangeFunc, this); } },
  "getPixels": function(value){  var result = new RegExp('((\\+|\\-)?\\d+(\\.\\d*)?)(px|vw|vh|vmin|vmax)?', 'i').exec(value); if (result == undefined) { return 0; } var num = parseFloat(result[1]); var unit = result[4]; var vw = this.rootPlayer.get('actualWidth') / 100; var vh = this.rootPlayer.get('actualHeight') / 100; switch(unit) { case 'vw': return num * vw; case 'vh': return num * vh; case 'vmin': return num * Math.min(vw, vh); case 'vmax': return num * Math.max(vw, vh); default: return num; } },
  "getMediaFromPlayer": function(player){  switch(player.get('class')){ case 'PanoramaPlayer': return player.get('panorama') || player.get('video'); case 'VideoPlayer': case 'Video360Player': return player.get('video'); case 'PhotoAlbumPlayer': return player.get('photoAlbum'); case 'MapPlayer': return player.get('map'); } },
  "setStartTimeVideoSync": function(video, player){  this.setStartTimeVideo(video, player.get('currentTime')); },
  "stopAndGoCamera": function(camera, ms){  var sequence = camera.get('initialSequence'); sequence.pause(); var timeoutFunction = function(){ sequence.play(); }; setTimeout(timeoutFunction, ms); },
  "changeBackgroundWhilePlay": function(playList, index, color){  var stopFunction = function(event){ playListItem.unbind('stop', stopFunction, this); if((color == viewerArea.get('backgroundColor')) && (colorRatios == viewerArea.get('backgroundColorRatios'))){ viewerArea.set('backgroundColor', backgroundColorBackup); viewerArea.set('backgroundColorRatios', backgroundColorRatiosBackup); } }; var playListItem = playList.get('items')[index]; var player = playListItem.get('player'); var viewerArea = player.get('viewerArea'); var backgroundColorBackup = viewerArea.get('backgroundColor'); var backgroundColorRatiosBackup = viewerArea.get('backgroundColorRatios'); var colorRatios = [0]; if((color != backgroundColorBackup) || (colorRatios != backgroundColorRatiosBackup)){ viewerArea.set('backgroundColor', color); viewerArea.set('backgroundColorRatios', colorRatios); playListItem.bind('stop', stopFunction, this); } },
  "updateMediaLabelFromPlayList": function(playList, htmlText, playListItemStopToDispose){  var changeFunction = function(){ var index = playList.get('selectedIndex'); if(index >= 0){ var beginFunction = function(){ playListItem.unbind('begin', beginFunction); setMediaLabel(index); }; var setMediaLabel = function(index){ var media = playListItem.get('media'); var text = media.get('data'); if(!text) text = media.get('label'); setHtml(text); }; var setHtml = function(text){ if(text !== undefined) { htmlText.set('html', '<div style=\"text-align:left\"><SPAN STYLE=\"color:#FFFFFF;font-size:12px;font-family:Verdana\"><span color=\"white\" font-family=\"Verdana\" font-size=\"12px\">' + text + '</SPAN></div>'); } else { htmlText.set('html', ''); } }; var playListItem = playList.get('items')[index]; if(htmlText.get('html')){ setHtml('Loading...'); playListItem.bind('begin', beginFunction); } else{ setMediaLabel(index); } } }; var disposeFunction = function(){ htmlText.set('html', undefined); playList.unbind('change', changeFunction, this); playListItemStopToDispose.unbind('stop', disposeFunction, this); }; if(playListItemStopToDispose){ playListItemStopToDispose.bind('stop', disposeFunction, this); } playList.bind('change', changeFunction, this); changeFunction(); },
  "getCurrentPlayerWithMedia": function(media){  var playerClass = undefined; var mediaPropertyName = undefined; switch(media.get('class')) { case 'Panorama': case 'LivePanorama': case 'HDRPanorama': playerClass = 'PanoramaPlayer'; mediaPropertyName = 'panorama'; break; case 'Video360': playerClass = 'PanoramaPlayer'; mediaPropertyName = 'video'; break; case 'PhotoAlbum': playerClass = 'PhotoAlbumPlayer'; mediaPropertyName = 'photoAlbum'; break; case 'Map': playerClass = 'MapPlayer'; mediaPropertyName = 'map'; break; case 'Video': playerClass = 'VideoPlayer'; mediaPropertyName = 'video'; break; }; if(playerClass != undefined) { var players = this.getByClassName(playerClass); for(var i = 0; i<players.length; ++i){ var player = players[i]; if(player.get(mediaPropertyName) == media) { return player; } } } else { return undefined; } },
  "getMediaByName": function(name){  var list = this.getByClassName('Media'); for(var i = 0, count = list.length; i<count; ++i){ var media = list[i]; if((media.get('class') == 'Audio' && media.get('data').label == name) || media.get('label') == name){ return media; } } return undefined; },
  "showPopupPanoramaOverlay": function(popupPanoramaOverlay, closeButtonProperties, imageHD, toggleImage, toggleImageHD, autoCloseMilliSeconds, audio, stopBackgroundAudio){  var self = this; this.MainViewer.set('toolTipEnabled', false); var cardboardEnabled = this.isCardboardViewMode(); if(!cardboardEnabled) { var zoomImage = this.zoomImagePopupPanorama; var showDuration = popupPanoramaOverlay.get('showDuration'); var hideDuration = popupPanoramaOverlay.get('hideDuration'); var playersPaused = this.pauseCurrentPlayers(audio == null || !stopBackgroundAudio); var popupMaxWidthBackup = popupPanoramaOverlay.get('popupMaxWidth'); var popupMaxHeightBackup = popupPanoramaOverlay.get('popupMaxHeight'); var showEndFunction = function() { var loadedFunction = function(){ if(!self.isCardboardViewMode()) popupPanoramaOverlay.set('visible', false); }; popupPanoramaOverlay.unbind('showEnd', showEndFunction, self); popupPanoramaOverlay.set('showDuration', 1); popupPanoramaOverlay.set('hideDuration', 1); self.showPopupImage(imageHD, toggleImageHD, popupPanoramaOverlay.get('popupMaxWidth'), popupPanoramaOverlay.get('popupMaxHeight'), null, null, closeButtonProperties, autoCloseMilliSeconds, audio, stopBackgroundAudio, loadedFunction, hideFunction); }; var hideFunction = function() { var restoreShowDurationFunction = function(){ popupPanoramaOverlay.unbind('showEnd', restoreShowDurationFunction, self); popupPanoramaOverlay.set('visible', false); popupPanoramaOverlay.set('showDuration', showDuration); popupPanoramaOverlay.set('popupMaxWidth', popupMaxWidthBackup); popupPanoramaOverlay.set('popupMaxHeight', popupMaxHeightBackup); }; self.resumePlayers(playersPaused, audio == null || !stopBackgroundAudio); var currentWidth = zoomImage.get('imageWidth'); var currentHeight = zoomImage.get('imageHeight'); popupPanoramaOverlay.bind('showEnd', restoreShowDurationFunction, self, true); popupPanoramaOverlay.set('showDuration', 1); popupPanoramaOverlay.set('hideDuration', hideDuration); popupPanoramaOverlay.set('popupMaxWidth', currentWidth); popupPanoramaOverlay.set('popupMaxHeight', currentHeight); if(popupPanoramaOverlay.get('visible')) restoreShowDurationFunction(); else popupPanoramaOverlay.set('visible', true); self.MainViewer.set('toolTipEnabled', true); }; if(!imageHD){ imageHD = popupPanoramaOverlay.get('image'); } if(!toggleImageHD && toggleImage){ toggleImageHD = toggleImage; } popupPanoramaOverlay.bind('showEnd', showEndFunction, this, true); } else { var hideEndFunction = function() { self.resumePlayers(playersPaused, audio == null || stopBackgroundAudio); if(audio){ if(stopBackgroundAudio){ self.resumeGlobalAudios(); } self.stopGlobalAudio(audio); } popupPanoramaOverlay.unbind('hideEnd', hideEndFunction, self); self.MainViewer.set('toolTipEnabled', true); }; var playersPaused = this.pauseCurrentPlayers(audio == null || !stopBackgroundAudio); if(audio){ if(stopBackgroundAudio){ this.pauseGlobalAudios(); } this.playGlobalAudio(audio); } popupPanoramaOverlay.bind('hideEnd', hideEndFunction, this, true); } popupPanoramaOverlay.set('visible', true); },
  "getComponentByName": function(name){  var list = this.getByClassName('UIComponent'); for(var i = 0, count = list.length; i<count; ++i){ var component = list[i]; var data = component.get('data'); if(data != undefined && data.name == name){ return component; } } return undefined; },
  "autotriggerAtStart": function(playList, callback, once){  var onChange = function(event){ callback(); if(once == true) playList.unbind('change', onChange, this); }; playList.bind('change', onChange, this); },
  "pauseGlobalAudios": function(caller, exclude){  if (window.pauseGlobalAudiosState == undefined) window.pauseGlobalAudiosState = {}; if (window.pauseGlobalAudiosList == undefined) window.pauseGlobalAudiosList = []; if (caller in window.pauseGlobalAudiosState) { return; } var audios = this.getByClassName('Audio').concat(this.getByClassName('VideoPanoramaOverlay')); if (window.currentGlobalAudios != undefined) audios = audios.concat(Object.values(window.currentGlobalAudios)); var audiosPaused = []; var values = Object.values(window.pauseGlobalAudiosState); for (var i = 0, count = values.length; i<count; ++i) { var objAudios = values[i]; for (var j = 0; j<objAudios.length; ++j) { var a = objAudios[j]; if(audiosPaused.indexOf(a) == -1) audiosPaused.push(a); } } window.pauseGlobalAudiosState[caller] = audiosPaused; for (var i = 0, count = audios.length; i < count; ++i) { var a = audios[i]; if (a.get('state') == 'playing' && (exclude == undefined || exclude.indexOf(a) == -1)) { a.pause(); audiosPaused.push(a); } } },
  "setMainMediaByName": function(name){  var items = this.mainPlayList.get('items'); for(var i = 0; i<items.length; ++i){ var item = items[i]; if(item.get('media').get('label') == name) { this.mainPlayList.set('selectedIndex', i); return item; } } },
  "setMediaBehaviour": function(playList, index, mediaDispatcher){  var self = this; var stateChangeFunction = function(event){ if(event.data.state == 'stopped'){ dispose.call(this, true); } }; var onBeginFunction = function() { item.unbind('begin', onBeginFunction, self); var media = item.get('media'); if(media.get('class') != 'Panorama' || (media.get('camera') != undefined && media.get('camera').get('initialSequence') != undefined)){ player.bind('stateChange', stateChangeFunction, self); } }; var changeFunction = function(){ var index = playListDispatcher.get('selectedIndex'); if(index != -1){ indexDispatcher = index; dispose.call(this, false); } }; var disposeCallback = function(){ dispose.call(this, false); }; var dispose = function(forceDispose){ if(!playListDispatcher) return; var media = item.get('media'); if((media.get('class') == 'Video360' || media.get('class') == 'Video') && media.get('loop') == true && !forceDispose) return; playList.set('selectedIndex', -1); if(panoramaSequence && panoramaSequenceIndex != -1){ if(panoramaSequence) { if(panoramaSequenceIndex > 0 && panoramaSequence.get('movements')[panoramaSequenceIndex-1].get('class') == 'TargetPanoramaCameraMovement'){ var initialPosition = camera.get('initialPosition'); var oldYaw = initialPosition.get('yaw'); var oldPitch = initialPosition.get('pitch'); var oldHfov = initialPosition.get('hfov'); var previousMovement = panoramaSequence.get('movements')[panoramaSequenceIndex-1]; initialPosition.set('yaw', previousMovement.get('targetYaw')); initialPosition.set('pitch', previousMovement.get('targetPitch')); initialPosition.set('hfov', previousMovement.get('targetHfov')); var restoreInitialPositionFunction = function(event){ initialPosition.set('yaw', oldYaw); initialPosition.set('pitch', oldPitch); initialPosition.set('hfov', oldHfov); itemDispatcher.unbind('end', restoreInitialPositionFunction, this); }; itemDispatcher.bind('end', restoreInitialPositionFunction, this); } panoramaSequence.set('movementIndex', panoramaSequenceIndex); } } if(player){ item.unbind('begin', onBeginFunction, this); player.unbind('stateChange', stateChangeFunction, this); for(var i = 0; i<buttons.length; ++i) { buttons[i].unbind('click', disposeCallback, this); } } if(sameViewerArea){ var currentMedia = this.getMediaFromPlayer(player); if(currentMedia == undefined || currentMedia == item.get('media')){ playListDispatcher.set('selectedIndex', indexDispatcher); } if(playList != playListDispatcher) playListDispatcher.unbind('change', changeFunction, this); } else{ viewerArea.set('visible', viewerVisibility); } playListDispatcher = undefined; }; var mediaDispatcherByParam = mediaDispatcher != undefined; if(!mediaDispatcher){ var currentIndex = playList.get('selectedIndex'); var currentPlayer = (currentIndex != -1) ? playList.get('items')[playList.get('selectedIndex')].get('player') : this.getActivePlayerWithViewer(this.MainViewer); if(currentPlayer) { mediaDispatcher = this.getMediaFromPlayer(currentPlayer); } } var playListDispatcher = mediaDispatcher ? this.getPlayListWithMedia(mediaDispatcher, true) : undefined; if(!playListDispatcher){ playList.set('selectedIndex', index); return; } var indexDispatcher = playListDispatcher.get('selectedIndex'); if(playList.get('selectedIndex') == index || indexDispatcher == -1){ return; } var item = playList.get('items')[index]; var itemDispatcher = playListDispatcher.get('items')[indexDispatcher]; var player = item.get('player'); var viewerArea = player.get('viewerArea'); var viewerVisibility = viewerArea.get('visible'); var sameViewerArea = viewerArea == itemDispatcher.get('player').get('viewerArea'); if(sameViewerArea){ if(playList != playListDispatcher){ playListDispatcher.set('selectedIndex', -1); playListDispatcher.bind('change', changeFunction, this); } } else{ viewerArea.set('visible', true); } var panoramaSequenceIndex = -1; var panoramaSequence = undefined; var camera = itemDispatcher.get('camera'); if(camera){ panoramaSequence = camera.get('initialSequence'); if(panoramaSequence) { panoramaSequenceIndex = panoramaSequence.get('movementIndex'); } } playList.set('selectedIndex', index); var buttons = []; var addButtons = function(property){ var value = player.get(property); if(value == undefined) return; if(Array.isArray(value)) buttons = buttons.concat(value); else buttons.push(value); }; addButtons('buttonStop'); for(var i = 0; i<buttons.length; ++i) { buttons[i].bind('click', disposeCallback, this); } if(player != itemDispatcher.get('player') || !mediaDispatcherByParam){ item.bind('begin', onBeginFunction, self); } this.executeFunctionWhenChange(playList, index, disposeCallback); },
  "playGlobalAudio": function(audio, endCallback){  var endFunction = function(){ audio.unbind('end', endFunction, this); this.stopGlobalAudio(audio); if(endCallback) endCallback(); }; audio = this.getGlobalAudio(audio); var audios = window.currentGlobalAudios; if(!audios){ audios = window.currentGlobalAudios = {}; } audios[audio.get('id')] = audio; if(audio.get('state') == 'playing'){ return audio; } if(!audio.get('loop')){ audio.bind('end', endFunction, this); } audio.play(); return audio; },
  "getOverlays": function(media){  switch(media.get('class')){ case 'Panorama': var overlays = media.get('overlays').concat() || []; var frames = media.get('frames'); for(var j = 0; j<frames.length; ++j){ overlays = overlays.concat(frames[j].get('overlays') || []); } return overlays; case 'Video360': case 'Map': return media.get('overlays') || []; default: return []; } },
  "getGlobalAudio": function(audio){  var audios = window.currentGlobalAudios; if(audios != undefined && audio.get('id') in audios){ audio = audios[audio.get('id')]; } return audio; },
  "resumeGlobalAudios": function(caller){  if (window.pauseGlobalAudiosState == undefined || !(caller in window.pauseGlobalAudiosState)) return; var audiosPaused = window.pauseGlobalAudiosState[caller]; delete window.pauseGlobalAudiosState[caller]; var values = Object.values(window.pauseGlobalAudiosState); for (var i = 0, count = values.length; i<count; ++i) { var objAudios = values[i]; for (var j = audiosPaused.length-1; j>=0; --j) { var a = audiosPaused[j]; if(objAudios.indexOf(a) != -1) audiosPaused.splice(j, 1); } } for (var i = 0, count = audiosPaused.length; i<count; ++i) { var a = audiosPaused[i]; if (a.get('state') == 'paused') a.play(); } },
  "triggerOverlay": function(overlay, eventName){  if(overlay.get('areas') != undefined) { var areas = overlay.get('areas'); for(var i = 0; i<areas.length; ++i) { areas[i].trigger(eventName); } } else { overlay.trigger(eventName); } },
  "getActivePlayerWithViewer": function(viewerArea){  var players = this.getByClassName('PanoramaPlayer'); players = players.concat(this.getByClassName('VideoPlayer')); players = players.concat(this.getByClassName('Video360Player')); players = players.concat(this.getByClassName('PhotoAlbumPlayer')); players = players.concat(this.getByClassName('MapPlayer')); var i = players.length; while(i-- > 0){ var player = players[i]; if(player.get('viewerArea') == viewerArea) { var playerClass = player.get('class'); if(playerClass == 'PanoramaPlayer' && (player.get('panorama') != undefined || player.get('video') != undefined)) return player; else if((playerClass == 'VideoPlayer' || playerClass == 'Video360Player') && player.get('video') != undefined) return player; else if(playerClass == 'PhotoAlbumPlayer' && player.get('photoAlbum') != undefined) return player; else if(playerClass == 'MapPlayer' && player.get('map') != undefined) return player; } } return undefined; },
  "resumePlayers": function(players, onlyResumeCameraIfPanorama){  for(var i = 0; i<players.length; ++i){ var player = players[i]; if(onlyResumeCameraIfPanorama && player.get('class') == 'PanoramaPlayer' && typeof player.get('video') === 'undefined'){ player.resumeCamera(); } else{ player.play(); } } },
  "initGA": function(){  var sendFunc = function(category, event, label) { ga('send', 'event', category, event, label); }; var media = this.getByClassName('Panorama'); media = media.concat(this.getByClassName('Video360')); media = media.concat(this.getByClassName('Map')); for(var i = 0, countI = media.length; i<countI; ++i){ var m = media[i]; var mediaLabel = m.get('label'); var overlays = this.getOverlays(m); for(var j = 0, countJ = overlays.length; j<countJ; ++j){ var overlay = overlays[j]; var overlayLabel = overlay.get('data') != undefined ? mediaLabel + ' - ' + overlay.get('data')['label'] : mediaLabel; switch(overlay.get('class')) { case 'HotspotPanoramaOverlay': case 'HotspotMapOverlay': var areas = overlay.get('areas'); for (var z = 0; z<areas.length; ++z) { areas[z].bind('click', sendFunc.bind(this, 'Hotspot', 'click', overlayLabel), this); } break; case 'CeilingCapPanoramaOverlay': case 'TripodCapPanoramaOverlay': overlay.bind('click', sendFunc.bind(this, 'Cap', 'click', overlayLabel), this); break; } } } var components = this.getByClassName('Button'); components = components.concat(this.getByClassName('IconButton')); for(var i = 0, countI = components.length; i<countI; ++i){ var c = components[i]; var componentLabel = c.get('data')['name']; c.bind('click', sendFunc.bind(this, 'Skin', 'click', componentLabel), this); } var items = this.getByClassName('PlayListItem'); var media2Item = {}; for(var i = 0, countI = items.length; i<countI; ++i) { var item = items[i]; var media = item.get('media'); if(!(media.get('id') in media2Item)) { item.bind('begin', sendFunc.bind(this, 'Media', 'play', media.get('label')), this); media2Item[media.get('id')] = item; } } },
  "setPanoramaCameraWithSpot": function(playListItem, yaw, pitch){  var panorama = playListItem.get('media'); var newCamera = this.cloneCamera(playListItem.get('camera')); var initialPosition = newCamera.get('initialPosition'); initialPosition.set('yaw', yaw); initialPosition.set('pitch', pitch); this.startPanoramaWithCamera(panorama, newCamera); },
  "pauseGlobalAudiosWhilePlayItem": function(playList, index, exclude){  var self = this; var item = playList.get('items')[index]; var media = item.get('media'); var player = item.get('player'); var caller = media.get('id'); var endFunc = function(){ if(playList.get('selectedIndex') != index) { if(hasState){ player.unbind('stateChange', stateChangeFunc, self); } self.resumeGlobalAudios(caller); } }; var stateChangeFunc = function(event){ var state = event.data.state; if(state == 'stopped'){ this.resumeGlobalAudios(caller); } else if(state == 'playing'){ this.pauseGlobalAudios(caller, exclude); } }; var mediaClass = media.get('class'); var hasState = mediaClass == 'Video360' || mediaClass == 'Video'; if(hasState){ player.bind('stateChange', stateChangeFunc, this); } this.pauseGlobalAudios(caller, exclude); this.executeFunctionWhenChange(playList, index, endFunc, endFunc); },
  "changePlayListWithSameSpot": function(playList, newIndex){  var currentIndex = playList.get('selectedIndex'); if (currentIndex >= 0 && newIndex >= 0 && currentIndex != newIndex) { var currentItem = playList.get('items')[currentIndex]; var newItem = playList.get('items')[newIndex]; var currentPlayer = currentItem.get('player'); var newPlayer = newItem.get('player'); if ((currentPlayer.get('class') == 'PanoramaPlayer' || currentPlayer.get('class') == 'Video360Player') && (newPlayer.get('class') == 'PanoramaPlayer' || newPlayer.get('class') == 'Video360Player')) { var newCamera = this.cloneCamera(newItem.get('camera')); this.setCameraSameSpotAsMedia(newCamera, currentItem.get('media')); this.startPanoramaWithCamera(newItem.get('media'), newCamera); } } },
  "cloneCamera": function(camera){  var newCamera = this.rootPlayer.createInstance(camera.get('class')); newCamera.set('id', camera.get('id') + '_copy'); newCamera.set('idleSequence', camera.get('initialSequence')); return newCamera; },
  "visibleComponentsIfPlayerFlagEnabled": function(components, playerFlag){  var enabled = this.get(playerFlag); for(var i in components){ components[i].set('visible', enabled); } },
  "playAudioList": function(audios){  if(audios.length == 0) return; var currentAudioCount = -1; var currentAudio; var playGlobalAudioFunction = this.playGlobalAudio; var playNext = function(){ if(++currentAudioCount >= audios.length) currentAudioCount = 0; currentAudio = audios[currentAudioCount]; playGlobalAudioFunction(currentAudio, playNext); }; playNext(); },
  "setComponentVisibility": function(component, visible, applyAt, effect, propertyEffect, ignoreClearTimeout){  var keepVisibility = this.getKey('keepVisibility_' + component.get('id')); if(keepVisibility) return; this.unregisterKey('visibility_'+component.get('id')); var changeVisibility = function(){ if(effect && propertyEffect){ component.set(propertyEffect, effect); } component.set('visible', visible); if(component.get('class') == 'ViewerArea'){ try{ if(visible) component.restart(); else if(component.get('playbackState') == 'playing') component.pause(); } catch(e){}; } }; var effectTimeoutName = 'effectTimeout_'+component.get('id'); if(!ignoreClearTimeout && window.hasOwnProperty(effectTimeoutName)){ var effectTimeout = window[effectTimeoutName]; if(effectTimeout instanceof Array){ for(var i=0; i<effectTimeout.length; i++){ clearTimeout(effectTimeout[i]) } }else{ clearTimeout(effectTimeout); } delete window[effectTimeoutName]; } else if(visible == component.get('visible') && !ignoreClearTimeout) return; if(applyAt && applyAt > 0){ var effectTimeout = setTimeout(function(){ if(window[effectTimeoutName] instanceof Array) { var arrayTimeoutVal = window[effectTimeoutName]; var index = arrayTimeoutVal.indexOf(effectTimeout); arrayTimeoutVal.splice(index, 1); if(arrayTimeoutVal.length == 0){ delete window[effectTimeoutName]; } }else{ delete window[effectTimeoutName]; } changeVisibility(); }, applyAt); if(window.hasOwnProperty(effectTimeoutName)){ window[effectTimeoutName] = [window[effectTimeoutName], effectTimeout]; }else{ window[effectTimeoutName] = effectTimeout; } } else{ changeVisibility(); } },
  "pauseGlobalAudio": function(audio){  var audios = window.currentGlobalAudios; if(audios){ audio = audios[audio.get('id')]; } if(audio.get('state') == 'playing') audio.pause(); },
  "getMediaWidth": function(media){  switch(media.get('class')){ case 'Video360': var res = media.get('video'); if(res instanceof Array){ var maxW=0; for(var i=0; i<res.length; i++){ var r = res[i]; if(r.get('width') > maxW) maxW = r.get('width'); } return maxW; }else{ return r.get('width') } default: return media.get('width'); } },
  "pauseCurrentPlayers": function(onlyPauseCameraIfPanorama){  var players = this.getCurrentPlayers(); var i = players.length; while(i-- > 0){ var player = players[i]; if(player.get('state') == 'playing') { if(onlyPauseCameraIfPanorama && player.get('class') == 'PanoramaPlayer' && typeof player.get('video') === 'undefined'){ player.pauseCamera(); } else { player.pause(); } } else { players.splice(i, 1); } } return players; },
  "existsKey": function(key){  return key in window; },
  "loadFromCurrentMediaPlayList": function(playList, delta){  var currentIndex = playList.get('selectedIndex'); var totalItems = playList.get('items').length; var newIndex = (currentIndex + delta) % totalItems; while(newIndex < 0){ newIndex = totalItems + newIndex; }; if(currentIndex != newIndex){ playList.set('selectedIndex', newIndex); } },
  "shareWhatsapp": function(url){  window.open('https://api.whatsapp.com/send/?text=' + encodeURIComponent(url), '_blank'); },
  "showPopupImage": function(image, toggleImage, customWidth, customHeight, showEffect, hideEffect, closeButtonProperties, autoCloseMilliSeconds, audio, stopBackgroundAudio, loadedCallback, hideCallback){  var self = this; var closed = false; var playerClickFunction = function() { zoomImage.unbind('loaded', loadedFunction, self); hideFunction(); }; var clearAutoClose = function(){ zoomImage.unbind('click', clearAutoClose, this); if(timeoutID != undefined){ clearTimeout(timeoutID); } }; var resizeFunction = function(){ setTimeout(setCloseButtonPosition, 0); }; var loadedFunction = function(){ self.unbind('click', playerClickFunction, self); veil.set('visible', true); setCloseButtonPosition(); closeButton.set('visible', true); zoomImage.unbind('loaded', loadedFunction, this); zoomImage.bind('userInteractionStart', userInteractionStartFunction, this); zoomImage.bind('userInteractionEnd', userInteractionEndFunction, this); zoomImage.bind('resize', resizeFunction, this); timeoutID = setTimeout(timeoutFunction, 200); }; var timeoutFunction = function(){ timeoutID = undefined; if(autoCloseMilliSeconds){ var autoCloseFunction = function(){ hideFunction(); }; zoomImage.bind('click', clearAutoClose, this); timeoutID = setTimeout(autoCloseFunction, autoCloseMilliSeconds); } zoomImage.bind('backgroundClick', hideFunction, this); if(toggleImage) { zoomImage.bind('click', toggleFunction, this); zoomImage.set('imageCursor', 'hand'); } closeButton.bind('click', hideFunction, this); if(loadedCallback) loadedCallback(); }; var hideFunction = function() { self.MainViewer.set('toolTipEnabled', true); closed = true; if(timeoutID) clearTimeout(timeoutID); if (timeoutUserInteractionID) clearTimeout(timeoutUserInteractionID); if(autoCloseMilliSeconds) clearAutoClose(); if(hideCallback) hideCallback(); zoomImage.set('visible', false); if(hideEffect && hideEffect.get('duration') > 0){ hideEffect.bind('end', endEffectFunction, this); } else{ zoomImage.set('image', null); } closeButton.set('visible', false); veil.set('visible', false); self.unbind('click', playerClickFunction, self); zoomImage.unbind('backgroundClick', hideFunction, this); zoomImage.unbind('userInteractionStart', userInteractionStartFunction, this); zoomImage.unbind('userInteractionEnd', userInteractionEndFunction, this, true); zoomImage.unbind('resize', resizeFunction, this); if(toggleImage) { zoomImage.unbind('click', toggleFunction, this); zoomImage.set('cursor', 'default'); } closeButton.unbind('click', hideFunction, this); self.resumePlayers(playersPaused, audio == null || stopBackgroundAudio); if(audio){ if(stopBackgroundAudio){ self.resumeGlobalAudios(); } self.stopGlobalAudio(audio); } }; var endEffectFunction = function() { zoomImage.set('image', null); hideEffect.unbind('end', endEffectFunction, this); }; var toggleFunction = function() { zoomImage.set('image', isToggleVisible() ? image : toggleImage); }; var isToggleVisible = function() { return zoomImage.get('image') == toggleImage; }; var setCloseButtonPosition = function() { var right = zoomImage.get('actualWidth') - zoomImage.get('imageLeft') - zoomImage.get('imageWidth') + 10; var top = zoomImage.get('imageTop') + 10; if(right < 10) right = 10; if(top < 10) top = 10; closeButton.set('right', right); closeButton.set('top', top); }; var userInteractionStartFunction = function() { if(timeoutUserInteractionID){ clearTimeout(timeoutUserInteractionID); timeoutUserInteractionID = undefined; } else{ closeButton.set('visible', false); } }; var userInteractionEndFunction = function() { if(!closed){ timeoutUserInteractionID = setTimeout(userInteractionTimeoutFunction, 300); } }; var userInteractionTimeoutFunction = function() { timeoutUserInteractionID = undefined; closeButton.set('visible', true); setCloseButtonPosition(); }; this.MainViewer.set('toolTipEnabled', false); var veil = this.veilPopupPanorama; var zoomImage = this.zoomImagePopupPanorama; var closeButton = this.closeButtonPopupPanorama; if(closeButtonProperties){ for(var key in closeButtonProperties){ closeButton.set(key, closeButtonProperties[key]); } } var playersPaused = this.pauseCurrentPlayers(audio == null || !stopBackgroundAudio); if(audio){ if(stopBackgroundAudio){ this.pauseGlobalAudios(); } this.playGlobalAudio(audio); } var timeoutID = undefined; var timeoutUserInteractionID = undefined; zoomImage.bind('loaded', loadedFunction, this); setTimeout(function(){ self.bind('click', playerClickFunction, self, false); }, 0); zoomImage.set('image', image); zoomImage.set('customWidth', customWidth); zoomImage.set('customHeight', customHeight); zoomImage.set('showEffect', showEffect); zoomImage.set('hideEffect', hideEffect); zoomImage.set('visible', true); return zoomImage; },
  "loopAlbum": function(playList, index){  var playListItem = playList.get('items')[index]; var player = playListItem.get('player'); var loopFunction = function(){ player.play(); }; this.executeFunctionWhenChange(playList, index, loopFunction); },
  "setEndToItemIndex": function(playList, fromIndex, toIndex){  var endFunction = function(){ if(playList.get('selectedIndex') == fromIndex) playList.set('selectedIndex', toIndex); }; this.executeFunctionWhenChange(playList, fromIndex, endFunction); },
  "syncPlaylists": function(playLists){  var changeToMedia = function(media, playListDispatched){ for(var i = 0, count = playLists.length; i<count; ++i){ var playList = playLists[i]; if(playList != playListDispatched){ var items = playList.get('items'); for(var j = 0, countJ = items.length; j<countJ; ++j){ if(items[j].get('media') == media){ if(playList.get('selectedIndex') != j){ playList.set('selectedIndex', j); } break; } } } } }; var changeFunction = function(event){ var playListDispatched = event.source; var selectedIndex = playListDispatched.get('selectedIndex'); if(selectedIndex < 0) return; var media = playListDispatched.get('items')[selectedIndex].get('media'); changeToMedia(media, playListDispatched); }; var mapPlayerChangeFunction = function(event){ var panoramaMapLocation = event.source.get('panoramaMapLocation'); if(panoramaMapLocation){ var map = panoramaMapLocation.get('map'); changeToMedia(map); } }; for(var i = 0, count = playLists.length; i<count; ++i){ playLists[i].bind('change', changeFunction, this); } var mapPlayers = this.getByClassName('MapPlayer'); for(var i = 0, count = mapPlayers.length; i<count; ++i){ mapPlayers[i].bind('panoramaMapLocation_change', mapPlayerChangeFunction, this); } },
  "playGlobalAudioWhilePlay": function(playList, index, audio, endCallback){  var changeFunction = function(event){ if(event.data.previousSelectedIndex == index){ this.stopGlobalAudio(audio); if(isPanorama) { var media = playListItem.get('media'); var audios = media.get('audios'); audios.splice(audios.indexOf(audio), 1); media.set('audios', audios); } playList.unbind('change', changeFunction, this); if(endCallback) endCallback(); } }; var audios = window.currentGlobalAudios; if(audios && audio.get('id') in audios){ audio = audios[audio.get('id')]; if(audio.get('state') != 'playing'){ audio.play(); } return audio; } playList.bind('change', changeFunction, this); var playListItem = playList.get('items')[index]; var isPanorama = playListItem.get('class') == 'PanoramaPlayListItem'; if(isPanorama) { var media = playListItem.get('media'); var audios = (media.get('audios') || []).slice(); if(audio.get('class') == 'MediaAudio') { var panoramaAudio = this.rootPlayer.createInstance('PanoramaAudio'); panoramaAudio.set('autoplay', false); panoramaAudio.set('audio', audio.get('audio')); panoramaAudio.set('loop', audio.get('loop')); panoramaAudio.set('id', audio.get('id')); var stateChangeFunctions = audio.getBindings('stateChange'); for(var i = 0; i<stateChangeFunctions.length; ++i){ var f = stateChangeFunctions[i]; if(typeof f == 'string') f = new Function('event', f); panoramaAudio.bind('stateChange', f, this); } audio = panoramaAudio; } audios.push(audio); media.set('audios', audios); } return this.playGlobalAudio(audio, endCallback); },
  "isCardboardViewMode": function(){  var players = this.getByClassName('PanoramaPlayer'); return players.length > 0 && players[0].get('viewMode') == 'cardboard'; },
  "openLink": function(url, name){  if(url == location.href) { return; } var isElectron = (window && window.process && window.process.versions && window.process.versions['electron']) || (navigator && navigator.userAgent && navigator.userAgent.indexOf('Electron') >= 0); if (name == '_blank' && isElectron) { if (url.startsWith('/')) { var r = window.location.href.split('/'); r.pop(); url = r.join('/') + url; } var extension = url.split('.').pop().toLowerCase(); if(extension != 'pdf' || url.startsWith('file://')) { var shell = window.require('electron').shell; shell.openExternal(url); } else { window.open(url, name); } } else if(isElectron && (name == '_top' || name == '_self')) { window.location = url; } else { var newWindow = window.open(url, name); newWindow.focus(); } },
  "registerKey": function(key, value){  window[key] = value; }
 },
 "contentOpaque": false,
 "paddingTop": 0,
 "data": {
  "name": "Player515"
 },
 "shadow": false,
 "downloadEnabled": false,
 "vrPolyfillScale": 0.85,
 "scrollBarOpacity": 0.5
};

    
    function HistoryData(playList) {
        this.playList = playList;
        this.list = [];
        this.pointer = -1;
    }

    HistoryData.prototype.add = function(index){
        if(this.pointer < this.list.length && this.list[this.pointer] == index) {
            return;
        }
        ++this.pointer;
        this.list.splice(this.pointer, this.list.length - this.pointer, index);
    };

    HistoryData.prototype.back = function(){
        if(!this.canBack()) return;
        this.playList.set('selectedIndex', this.list[--this.pointer]);
    };

    HistoryData.prototype.forward = function(){
        if(!this.canForward()) return;
        this.playList.set('selectedIndex', this.list[++this.pointer]);
    };

    HistoryData.prototype.canBack = function(){
        return this.pointer > 0;
    };

    HistoryData.prototype.canForward = function(){
        return this.pointer >= 0 && this.pointer < this.list.length-1;
    };
    //

    if(script.data == undefined)
        script.data = {};
    script.data["history"] = {};    //playListID -> HistoryData

    TDV.PlayerAPI.defineScript(script);
})();
